<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usage extends CI_Controller {
	var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('usage_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		// check user permission
		$this->load->model('permission_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
		
		$data['user_info'] = $this->usage_model->user_info();
		$data['live_usage'] = $this->usage_model->live_usage();
		$this->load->view('account/usage_view', $data);
	}
	
	public function usage_logs(){
		$month = $this->input->post("month");
		$data = $this->usage_model->usage_logs($month);
		echo $data;
	}
	
	public function update_email(){
		if($this->input->post('user_email_hidden') != $this->input->post('user_email')){
			$this->usage_model->update_email();
			$this->session->set_flashdata('updated_usage', 'Email updated successfully.');	
		}
	
		redirect(base_url()."usage");
	}
	
	public function update_mobile(){
		if($this->input->post('user_old_mobile_hidden_update') != $this->input->post('user_new_mobile_hidden_update')){
			$this->usage_model->update_mobile();
			$this->session->set_flashdata('updated_usage', 'Mobile number updated successfully.');
		}
		
		redirect(base_url()."usage");
	}
	
	public function send_otp(){
		$old_number = $this->input->post("old_number");
		$new_number = $this->input->post("new_number");
		if($old_number == $new_number){
			$msg = 0;
		}else{
			$this->usage_model->send_otp();
			$msg = 1;
		}
		echo $msg ;
	}
	
	public function verify_otp(){
		$check = $this->usage_model->verify_otp();
		echo $check;
	}
	
	private  $baseUrl = "http://sendotp.msg91.com/api";
 public function OTP_MSG() {
	$phone = "9650896116";
     /*$data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
     $data_string = json_encode($data);
     $ch = curl_init($this->baseUrl.'/generateOTP');
     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_AUTOREFERER, true);
     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/json',
  'Content-Length: ' . strlen($data_string),
  'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
     ));
     $result = curl_exec($ch);
     curl_close($ch);
     //echo '<pre>'; print_r($result);
     $response = json_decode($result,true);
     if($response["status"] == "error"){
        // return $response["response"]["code"];
  return 0;
     }else{
  return $response["response"]["oneTimePassword"];
     }*/
     echo $phone;
 }
	
	
}
