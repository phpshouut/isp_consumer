<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->helper('cookie');
		$this->load->library('encryption');
		$this->load->model('login_model');
		$this->load->model('home_model');
		$this->load->model('permission_model');
		header("Access-Control-Allow-Origin: *");
	}

	public function index(){
		// check user permission
		$this->permission_model->user_permissions();
		
		if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url().'home');
		}
		else{
			// check cookie set the login
			$cookie_username =  $this->encryption->decrypt(get_cookie('isp_consumer_username'));
			 $cookie_password = $this->encryption->decrypt(get_cookie('isp_consumer_password'));
			if($cookie_username!= '' && $cookie_password != ''){
				
				$result = $this->login_model->check_credentials($cookie_username, $cookie_password);
				if($result){
					
					redirect(base_url().'home');
				}else{
					//unsert cookie and reload again
					delete_cookie('isp_consumer_username');
					delete_cookie('isp_consumer_password'); 
					redirect(base_url().'login');
					
				}
				
			}else{
				$isp_logo = '';
				$isp_name = '';
				$isp_logo_detial = $this->login_model->isp_detail();
				if($isp_logo_detial->num_rows() > 0){
					$isp_detail_row = $isp_logo_detial->row_array();
					if($isp_detail_row['logo_image'] != ''){
						$isp_logo = IMAGEPATHISP."logo/".$isp_detail_row['logo_image'];	
					}
					$isp_name = $isp_detail_row['isp_name'];
					
				}
				
				$data = array();
				if($this->session->userdata['isp_consumer_permission_session']['website_module_permission'] == '1'){
					$data['slider_offer'] = $this->home_model->slider_offer();
					$data['about_us'] = $this->home_model->about_us_data();
				}
				
				//$data['shouut_promotion'] = $this->home_model->shouut_promotion();
				
				$data['isp_logo'] = $isp_logo;
				$data['isp_name'] = $isp_name;
				$this->load->view('login_view',$data);
			}
			
		}
		
		
	}
	public function validate_user(){
		if(empty($_POST)){
			redirect(base_url().'login'); exit;
		}
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim');
		$this->form_validation->set_rules('password', 'Password', 'trim|callback_check_database');

		if ($this->form_validation->run() == FALSE){
			redirect(base_url().'login');
		}else{
			redirect(base_url().'home');
			//echo "success";
		}
	}
	public function check_database($password){
		$username = $this->input->post('username');
		$result = $this->login_model->check_credentials($username, $password);

		if($result){
		
			return TRUE;
		}else{
			
			$this->form_validation->set_message('check_database', 'Username or Password is incorrect.');
			return false;
		}
	}

	public function check_login_ajax(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$check = $this->login_model->check_login_ajax($username, $password);
		echo $check;
	}
	
	public function logout(){
		
		$this->session->unset_userdata('isp_consumer_session');
		delete_cookie('isp_consumer_username');
		delete_cookie('isp_consumer_password'); 
		redirect(base_url());
	}
	
	public function shop_data(){
		$data = $this->home_model->shop_data();
		echo $data;
	}
	
	public function plan_topup_data(){
		$data = $this->home_model->plan_topup_data();
		echo $data;
	}
	public function plan_type_plan(){
		$plan_type = $this->input->post("type");
		
		$data = $this->home_model->plan_type_plan($plan_type);
		echo $data;
	}
	public function  about_us_data(){
		$data = $this->home_model->about_us_data();
		echo $data;
	}
	
	public function services_data(){
		$data = $this->home_model->services_data();
		echo $data;
	}
	
	public function promotion_data(){
		$data = $this->home_model->promotion_data();
		echo $data;
	}
	
	public function contact_us_data(){
		$data = $this->home_model->contact_us_data();
		echo $data;
	}
	
	public function request_plan_topup(){
		$data = $this->home_model->request_plan_topup();
		echo $data;
	}
	
	public function send_otp(){
		$uid = $this->input->post("uid");
		
			$msg = $this->login_model->send_otp($uid);
		
		echo $msg ;
	}
	
	public function forgot_password(){
		$check = $this->login_model->forgot_password();
		echo $check;
	}
	
	public function impersonate_user(){
		if(empty($_GET)){
			redirect(base_url().'login'); exit;
		}
		$getdata = base64_decode($_GET['ud']);
		$userdata = explode('__', $getdata);
		$username = $userdata[0];
		$password = $userdata[1];
		
		$result = $this->impersonate_database($username, $password);

		if ($result == 0){
			redirect(base_url().'login');
		}else{
			$support_number1 = '';
			$support_number2 = '';
			$support_number3 = '';
			$support_email = '';
			$isp_logo = '';
			$isp_name = '';
			$isp_detail =$this->home_model->isp_detail();
			if($isp_detail->num_rows() > 0){
				$isp_detail_row = $isp_detail->row_array();
				$support_number1 = $isp_detail_row['help_number1'];
				$support_number2 = $isp_detail_row['help_number2'];
				$support_number3 = $isp_detail_row['help_number3'];
				$support_email = $isp_detail_row['support_email'];
				if($isp_detail_row['logo_image'] != ''){
					$isp_logo = IMAGEPATHISP."logo/".$isp_detail_row['logo_image'];	
				}
				$isp_name = $isp_detail_row['isp_name'];
				
			}
			$data_session = $this->session->userdata('isp_consumer_session');
			$data_session['support_number1']  = $support_number1;
			$data_session['support_number2']  = $support_number2;
			$data_session['support_number3']  = $support_number3;
			$data_session['support_email']  = $support_email;
			$data_session['isp_logo']  = $isp_logo;
			$data_session['isp_name']  = $isp_name;
			$this->session->set_userdata('isp_consumer_session', $data_session);
			
			redirect(base_url().'usage');
			//echo "success";
		}
	}
	
	public function impersonate_database($username, $password){
		$result = $this->login_model->check_credentials($username, $password);
		if($result){
			return 1;
		}else{	
			return 0;
		}
	}
}
