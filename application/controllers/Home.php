<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('home_model');
		$this->load->model('permission_model');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
	}

	public function index(){
		//set isp detail in session
		$this->permission_model->user_permissions();
		
		$support_number1 = '';
			$support_number2 = '';
			$support_number3 = '';
			$support_email = '';
			$isp_logo = '';
			$isp_name = '';
			$isp_detail =$this->home_model->isp_detail();
			if($isp_detail->num_rows() > 0){
				$isp_detail_row = $isp_detail->row_array();
				$support_number1 = $isp_detail_row['help_number1'];
				$support_number2 = $isp_detail_row['help_number2'];
				$support_number3 = $isp_detail_row['help_number3'];
				$support_email = $isp_detail_row['support_email'];
				if($isp_detail_row['logo_image'] != ''){
					$isp_logo = IMAGEPATHISP."logo/".$isp_detail_row['logo_image'];	
				}
				$isp_name = $isp_detail_row['isp_name'];
				
			}
			$data_session = $this->session->userdata('isp_consumer_session');
			$data_session['support_number1']  = $support_number1;
			$data_session['support_number2']  = $support_number2;
			$data_session['support_number3']  = $support_number3;
			$data_session['support_email']  = $support_email;
			$data_session['isp_logo']  = $isp_logo;
			$data_session['isp_name']  = $isp_name;
			$this->session->set_userdata('isp_consumer_session', $data_session);
			
			$data = array();
			if($this->session->userdata['isp_consumer_permission_session']['website_module_permission'] == '1'){
				$data['slider_offer'] = $this->home_model->slider_offer();
				$data['about_us'] = $this->home_model->about_us_data();
			}	
		
		$this->load->view('home_view',$data);
	}
	public function shop_data(){
		$data = $this->home_model->shop_data();
		echo $data;
	}
	
	public function plan_topup_data(){
		$data = $this->home_model->plan_topup_data();
		echo $data;
	}
	public function plan_type_plan(){
		$plan_type = $this->input->post("type");
		
		$data = $this->home_model->plan_type_plan($plan_type);
		echo $data;
	}
	public function  about_us_data(){
		$data = $this->home_model->about_us_data();
		echo $data;
	}
	
	public function services_data(){
		$data = $this->home_model->services_data();
		echo $data;
	}
	public function promotion_data(){
		$data = $this->home_model->promotion_data();
		echo $data;
	}
	public function contact_us_data(){
		$data = $this->home_model->contact_us_data();
		echo $data;
	}
	
	public function request_plan_topup(){
		$data = $this->home_model->request_plan_topup();
		echo $data;
	}
}
