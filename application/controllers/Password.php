<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('password_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		// check user permission
		$this->load->model('permission_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
	
		$this->load->view('account/password_view');
	}
	
	public function check_current_password(){
		$current_password = $this->input->post('current_password');
		$check = $this->password_model->check_current_password($current_password);
		echo $check;
	}
	
	public function update_password(){
		$this->password_model->update_password();
		$this->session->set_flashdata('update_password_msg', 'Password Successfully updated.');
		redirect(base_url()."password");
	}
	
	
}
