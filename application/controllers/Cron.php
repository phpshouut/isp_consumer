<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('cron_model');
		
		
	}

	public function index(){
		$this->cron_model->radacct_cron();
	}
	
}
