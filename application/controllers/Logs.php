<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('logs_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		include APPPATH . 'libraries/encdec_paytm.php';
		// check user permission
		$this->load->model('permission_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
		$this->load->view('account/logs_view');
	}
	
	public function usuage_logs(){
		$this->logs_model->datausuage_logs();
	}
	public function data_logs(){
		$this->logs_model->data_logs();
	}
	
	
	

}
