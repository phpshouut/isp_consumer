<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('service_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		// check user permission
		$this->load->model('permission_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
		$data['current_request'] = $this->service_model->current_request();
		$data['service_type'] = $this->service_model->service_type();
		$this->load->view('account/service_view',$data);
	}
	
	public function generate_request(){
		$return_msg = $this->service_model->generate_request();
		if($return_msg == '1'){
			$this->session->set_flashdata('generate_request_msg', 'Request generated successfully.');
		}else{
			$this->session->set_flashdata('generate_request_msg', 'Request not generated.');
		}
			
		redirect(base_url().'service');
	}
	
	
}
