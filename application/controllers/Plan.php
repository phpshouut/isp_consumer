<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('plan_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		// check user permission
		$this->load->model('permission_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
		$data['user_plan'] = $this->plan_model->user_plan();
		$data['live_usage'] = $this->plan_model->live_usage();
		$data['recommended_plan'] = $this->plan_model->recommended_plan();
		$this->load->view('account/plan_view', $data);
	}
	
	public function schedule_next_plan(){
		$service_id = $this->input->post("service_id");
		$update = $this->plan_model->schedule_next_plan($service_id);
		echo $update ;
	}
	
	
}
