<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bils extends CI_Controller {
var $isp_uid;
	public function __construct(){
		parent :: __construct();
		$this->isp_uid = ISPID;
		$this->load->model('bils_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
		include APPPATH . 'libraries/encdec_paytm.php';
		// check user permission
		$this->load->model('permission_model');
		$this->load->model('emailer_model');
		$this->permission_model->user_permissions();
		if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '0'){
			redirect(base_url()."login/logout");	
		}
	}

	public function index(){
		$data['bils_model'] = $this->bils_model->bils_model();
		$data['pending_bill'] = $this->bils_model->pending_bill();
		$ispcodet = $this->bils_model->countrydetails();
		$data['cocurrency'] = $ispcodet['currency'];
		$this->load->view('account/bils_view',$data);
	}
	public function addFund_success_view($id){
		$data["transection_detail"] = $this->bils_model->transection_detail($id);
		$this->load->view('account/payment/fund_success', $data);
	}
	public function addFund_fail_view($id){
		$data["transection_detail"] = $this->bils_model->transection_detail($id);
		$this->load->view('account/payment/fund_fail',$data);
	}
	public function pay_bill_citrus($formPostUrl,$secret_key,$vanityUrl,$currency ){
		$amount_current_pending = $this->input->post('total_current_due');
		$sess_array = array();
		$sess_array = array(
					'total_current_due' => $amount_current_pending,
				);
		$this->session->set_userdata('user_bill_payed_session', $sess_array);
		
		$amount = $this->input->post('total_amount_pay');
		
		$merchantTxnId = uniqid();
		$orderAmount = $amount;
		
		$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
		$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
		$notifyUrl = base_url() . 'bils/paymentnotify';
		$returnUrl = base_url() . 'bils/paymentresponse';
		$data['citrusdetail'] = array("merchantTxnId" => $merchantTxnId,"orderAmount" => $orderAmount,
			"currency" => $currency,"returnUrl" => $returnUrl,   "notifyUrl" => $notifyUrl,
			"secSignature" => $securitySignature,'posturl'=>$formPostUrl);
		$this->load->view('account/payment/fund',$data);
	}
	public function paymentnotify() {

	}
	public function paymentresponse() {
		$postdata=$this->input->post();
		$datarr=array();

		if (isset($_POST['TxId'])) {
			// echo "sssssssssss"; die;
			set_include_path('../lib' . PATH_SEPARATOR . get_include_path());
			$secret_key = CITRUSSECRETKEY;
			$data = "";
			$flag = "true";


			$paymentPostResponse = $_POST;
			if (isset($_POST['TxId'])) {
				$txnid = $_POST['TxId'];
				$data .= $txnid;
			}
			if (isset($_POST['TxStatus'])) {
				$txnstatus = $_POST['TxStatus'];
				$data .= $txnstatus;
			}
			if (isset($_POST['amount'])) {
				$amount = $_POST['amount'];
				$data .= $amount;
			}
			if (isset($_POST['pgTxnNo'])) {
				$pgtxnno = $_POST['pgTxnNo'];
				$data .= $pgtxnno;
			}
			if (isset($_POST['issuerRefNo'])) {
				$issuerrefno = $_POST['issuerRefNo'];
				$data .= $issuerrefno;
			}
			if (isset($_POST['authIdCode'])) {
				$authidcode = $_POST['authIdCode'];
				$data .= $authidcode;
			}
			if (isset($_POST['firstName'])) {
				$firstName = $_POST['firstName'];
				$data .= $firstName;
			}
			if (isset($_POST['lastName'])) {
				$lastName = $_POST['lastName'];
				$data .= $lastName;
			}
			if (isset($_POST['pgRespCode'])) {
				$pgrespcode = $_POST['pgRespCode'];
				$data .= $pgrespcode;
			}
			if (isset($_POST['addressZip'])) {
				$pincode = $_POST['addressZip'];
				$data .= $pincode;
			}
			if (isset($_POST['signature'])) {
				$signature = $_POST['signature'];
			}

			$respSignature = hash_hmac('sha1', $data, $secret_key);
			if ($signature != "" && strcmp($signature, $respSignature) != 0) {
				$flag = "false";
			}

			if ($flag == "true") {

				if ($txnstatus == "SUCCESS") {
					$this->bils_model->citrus_success(true);

				} else {
					$this->bils_model->citrus_success();

				}
			} else {

				$this->bils_model->citrus_success();
				// $this->render('payment-failed', array('eventdetail' => $eventDetail, "eventStockList" => $eventStockList, 'paymentPostResponse' => $paymentPostResponse, 'amount' => $amount, 'event_id' => $event_id, 'message' => 'Citrus Response Signature and Our (Merchant) Signature Mis-Mactch'));
			}
		} else {

			$this->redirect(base_url()."bils");
		}
	}
	
	public function pay_bill_payu($MERCHANT_KEY,$SALT,$PAYU_BASE_URL){
		// for payumoney
		$amount_current_pending = $this->input->post('total_current_due');
		$sess_array = array();
		$sess_array = array(
					'total_current_due' => $amount_current_pending,
				);
		$this->session->set_userdata('user_bill_payed_session', $sess_array);
		
		$amount = $this->input->post('total_amount_pay');
		$productinfo = 'bills';
		$fname = '';
		$email = '';
		$phone = '';
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		// get payumoney credential
		$payucredential = $this->bils_model->payumoney_credential();
		/*if($payucredential['mer_key'] != '' && $payucredential['salt']){
			$MERCHANT_KEY = $payucredential['mer_key'];
			$SALT = $payucredential['salt'];
			$PAYU_BASE_URL = "https://secure.payu.in/_payment";
			
		}else{
			//$MERCHANT_KEY = "gtKFFx";
			//$SALT = "eCwWELxi";
			$MERCHANT_KEY = "";
			$SALT = "";
			$PAYU_BASE_URL = "https://test.payu.in";
		}*/
		
		$udf1='';
		$udf2='';
		$udf3='';
		$udf4='';
		$udf5='';
		$hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|'. $fname . '|' . $email .'|'.$udf1.'|' .$udf2.'|' .$udf3.'|'.$udf4.'|'.$udf5.'||||||'. $SALT;
	
        $hash = strtolower(hash('sha512', $hashstring));
        $data['hash'] = $hash;
	$success_url = base_url() . 'bils/payumoney_success';
	$fail_url = base_url() . 'bils/payumoney_fail';
	$cancle_url = base_url() . 'bils/payumoney_cancle';
	$data['payumoneydetail'] = array('posturl'=>$PAYU_BASE_URL, 'mkey' => $MERCHANT_KEY, 'tid' => $txnid, 'amount' => $amount,
					 'name' => $fname, 'email'=>$email, 'phone' => $phone, 'productinfo' => $productinfo,
					 'success_url' => $success_url, 'fail_url' => $fail_url, 'cancle_url'=> $cancle_url);
	
        $this->load->view('account/payment/payumoney/payumoney',$data);
	}
	public function payumoney_success(){
		$this->bils_model->payumoney_success();
	}
	public function payumoney_fail(){
		$this->bils_model->payumoney_cancle();
	}
	public function payumoney_cancle(){
		
		$this->bils_model->payumoney_cancle();
	}
	public function payumoney_cancel_view($id){
		$data["transection_detail"] = $this->bils_model->payumoney_transection_detail($id);
		$this->load->view('account/payment/payumoney/payumoney_cancle',$data);
	}
	public function payumoney_success_view($id){
		$data["transection_detail"] = $this->bils_model->payumoney_transection_detail($id);
		$this->load->view('account/payment/payumoney/payumoney_success',$data);
	}
	
	
	public function pay_bill_paytm($paytm_merchant_key,$paytm_merchant_mid,$paytm_merchant_web,$paytm_environment){
		$data=array();
		$user_uid = '';
		if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
		    $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
		}
		$isp_uid = $this->isp_uid;
		$amount_current_pending = $this->input->post('total_current_due');
		$sess_array = array();
		$sess_array = array(
					'total_current_due' => $amount_current_pending,
				);
		$this->session->set_userdata('user_bill_payed_session', $sess_array);
		
		$amount = $this->input->post('total_amount_pay');
		$data['userdata']['total_amt'] = $amount;
		$data['userdata']['user_id'] = $user_uid;
		$data['userdata']['ORDER_ID'] = "ORDS" . rand(10000,99999999);;
		$data['userdata']['CUST_ID'] = $isp_uid;
		$data['userdata']['INDUSTRY_TYPE_ID'] = 'Retail109';
		$data['userdata']['CHANNEL_ID'] = 'WEB';
		$data['userdata']['TXN_AMOUNT'] = $amount;
		$data['userdata']['MOBILE_NO'] = "";
		
		$data['userdata']['paytm_merchant_key'] = $paytm_merchant_key;
		$data['userdata']['paytm_merchant_mid'] = $paytm_merchant_mid;
		$data['userdata']['paytm_merchant_web'] = $paytm_merchant_web;
		$data['userdata']['paytm_environment'] = $paytm_environment;
		$call_back_url = base_url() . 'bils/paytm_success';
		$data['userdata']['call_back_url'] = $call_back_url;
		//echo "<pre>";print_r($data);die;
		$this->load->view('account/payment/paytm/paytm',$data);
	}
	public function paytm_success(){
		$paytm_merchant_key = '';
		$paytm_merchant_mid = '';
		$paytm_merchant_web = '';
		$paytm_environment = '';
		$credential = $this->bils_model->check_payment_gateway();
		if($credential['resultCode'] != '0'){
			if(count($credential['gatways']) > 0){
				$gateways = $credential['gatways'];
				$gateway_name = $gateways[0]['gatway_name'];
				if($gateway_name == 'paytm'){
					$paytm_merchant_key = $gateways[0]['paytm_merchant_key'];
					$paytm_merchant_mid = $gateways[0]['paytm_merchant_mid'];
					$paytm_merchant_web = $gateways[0]['paytm_merchant_web'];
					$paytm_environment = $gateways[0]['paytm_environment'];
				}
			}
		}
		//print_r($_POST);die;
		$this->bils_model->paytm_success($paytm_merchant_key, $paytm_merchant_mid, $paytm_merchant_web, $paytm_environment);
	}
	public function paytm_success_view($id){
		$data["transection_detail"] = $this->bils_model->transection_detail($id);
		$this->load->view('account/payment/paytm/paytm_success', $data);
	}
	public function pay_bill_ebs($ebs_accountid,$ebs_secretkey,$ebs_account_name){
		$data=array();
		$user_uid = '';
		if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
		    $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
		}
		$isp_uid = $this->isp_uid;
		$amount_current_pending = $this->input->post('total_current_due');
		$sess_array = array();
		$sess_array = array(
					'total_current_due' => $amount_current_pending,
				);
		$this->session->set_userdata('user_bill_payed_session', $sess_array);
		
		$amount = $this->input->post('total_amount_pay');
		$data['userdata']['key'] = $ebs_secretkey;
		
		$ebs_session = array();
		$ebs_session = array(
					'secret_key' => $ebs_secretkey
				);
		$this->session->set_userdata('ebs_session', $ebs_session);
		
		$data['userdata']['account_id'] = $ebs_accountid;
		$data['userdata']['ebsuser_name'] = $ebs_account_name;
		$data['userdata']['ebsuser_address'] = " B-13, Pratap Plaza, Near Haldighati, Sector-5, Pratap Nagar, Jaipur";
		$data['userdata']['ebsuser_zipcode'] = "302022";
		$data['userdata']['ebsuser_city'] = "Jaipur";
		$data['userdata']['ebsuser_state'] = "";
		$data['userdata']['ebsuser_country'] = "Ind";
		$data['userdata']['ebsuser_phone'] = "9261666663";
		$data['userdata']['ebsuser_email'] = "speed4net.world@gmail.com";
		$data['userdata']['ebsuser_id'] = $user_uid;
		$data['userdata']['modelno'] = "1";
		$data['userdata']['finalamount'] = $amount;
		$data['userdata']['order_no'] = "ORDS" . rand(10000,99999999);
		$call_back_url = base_url() . 'bils/ebs_success?DR={DR}';
		$data['userdata']['return_url'] = $call_back_url;
		$data['userdata']['mode'] = "LIVE";
		//echo "<pre>";print_r($data);die;
		$this->load->view('account/payment/ebs/ebs',$data);
	}
	public function ebs_success(){
		$secret_key = "";
		if(isset($this->session->userdata['ebs_session']['secret_key'])){
		    $secret_key = $this->session->userdata['ebs_session']['secret_key'];    
		}
		$response = array();
		if(isset($_GET['DR'])) {  
			include APPPATH . 'libraries/decrypt_ebs.php';
			$DR = preg_replace("/\s/","+",$_GET['DR']);
			$rc4 = new Crypt_RC4($secret_key);
			$QueryString = base64_decode($DR);
			$rc4->decrypt($QueryString);
			$QueryString = explode('&',$QueryString);
			
			foreach($QueryString as $param){
				$param = explode('=',$param);
				$response[$param[0]] = urldecode($param[1]);
			}
		}
		$this->bils_model->ebs_success($response);
	}
	public function ebs_success_view($id){
		$data["transection_detail"] = $this->bils_model->transection_detail($id);
		$this->load->view('account/payment/ebs/ebs_success', $data);
	}
	public function check_payment_gateway(){
		$credential = $this->bils_model->check_payment_gateway();
		if($credential['resultCode'] == '0'){
			$this->session->set_flashdata('msgLogin', 'No payment gateway set');
			redirect(base_url().'bils');
		}else{
			if(count($credential['gatways']) > 0){
				$gateways = $credential['gatways'];
				$gateway_name = $gateways[0]['gatway_name'];
				//echo "<pre>";print_r($credential);die;
				if($gateway_name == 'citrus'){
					$formPostUrl = $gateways[0]['citrus_post_url'];
					$secret_key = $gateways[0]['citrus_secret_key'];
					$vanityUrl = $gateways[0]['citrus_vanity_url'];
					$currency = CITRUSCURRENCY;
					$this->pay_bill_citrus($formPostUrl,$secret_key,$vanityUrl,$currency);
				}elseif($gateway_name == 'payu'){
					$MERCHANT_KEY = $gateways[0]['payu_merchantkey'];
					$SALT = $gateways[0]['payu_merchantsalt'];
					$PAYU_BASE_URL = "https://secure.payu.in/_payment";;
					$this->pay_bill_payu($MERCHANT_KEY,$SALT,$PAYU_BASE_URL);
				}
				elseif($gateway_name == 'paytm'){
					$paytm_merchant_key = $gateways[0]['paytm_merchant_key'];
					$paytm_merchant_mid = $gateways[0]['paytm_merchant_mid'];
					$paytm_merchant_web = $gateways[0]['paytm_merchant_web'];
					$paytm_environment = $gateways[0]['paytm_environment'];
					$this->pay_bill_paytm($paytm_merchant_key,$paytm_merchant_mid,$paytm_merchant_web,$paytm_environment);
				}elseif($gateway_name == 'EBS'){
					$ebs_accountid = $gateways[0]['ebs_accountid'];
					$ebs_secretkey = $gateways[0]['ebs_secretkey'];
					$ebs_account_name = $gateways[0]['ebs_account_name'];
					$this->pay_bill_ebs($ebs_accountid,$ebs_secretkey,$ebs_account_name);
				}
				else{
					$this->session->set_flashdata('msgLogin', 'This payment gateway not integrated');
					redirect(base_url().'bils');
				}
				//echo "<pre>";print_r($credential);die;
				
			}else{
				$this->session->set_flashdata('msgLogin', 'No payment gateway set');
				redirect(base_url().'bils');	
			}
			
		}
	}
	
	public function billcopy_actions(){
		$this->bils_model->billcopy_actions();
	}
	
}
