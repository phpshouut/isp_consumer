<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credits extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('credits_model');
		$this->load->library('form_validation');
		if(!isset($this->session->userdata['isp_consumer_session']['user_id'])){
			redirect(base_url());
		}
	}

	public function index(){
	
		$this->load->view('account/credits_view');
	}
	
	
}
