<?php
class Cron_model extends CI_Model{
   
       public function radacct_cron(){
	//$starting_date = date('Y-m-01', strtotime('-1 MONTH'));
	$starting_date = date('Y-m-01');
        $data = array();
        $start_time = '';
        $end_time = '';
        $current_hr = date('H');
        if($current_hr > 10 && $current_hr < 14){
            //afternoon cron
            $start_time = date('Y-m-d')." 00:00:00";
            $end_time = date('Y-m-d')." 11:59:59";
        }else{
            // night cron
            $start_time = date('Y-m-d')." 12:00:00";
            $end_time = date('Y-m-d')." 23:59:59";
        }
        
        //get user list from radact table
        $get_userid = $this->db->query("select uid from sht_users where inactivate_type != 'terminate' AND account_activated_on != '0000-00-00 00:00:00'");
             foreach($get_userid->result() as $get_userid_row){
                $uid = $get_userid_row->uid;
              
            
                $total_upload = 0;
                $total_download = 0;
                // get user device macid
                     $user_macid = array();
                     $get_macid = $this->db->query("select hotspotMac from sht_user_hotspot_assoc where uid = '$uid'");
                     if($get_macid->num_rows() > 0){
                         foreach($get_macid->result() as $get_macid_row){
                             $user_macid[] = $get_macid_row->hotspotMac;        
                         }
                     }
                     $user_macid = '"'.implode('", "', $user_macid).'"';
                // get user total data
                $query = $this->db->query("select sum(acctinputoctets) as upload, sum(acctoutputoctets) as download from radacct where date(acctstarttime) >= '$starting_date' AND username = '$uid' OR username IN($user_macid)");
               
                if($query->num_rows() > 0){
                    $row = $query->row_array();
                    if(isset($row['upload'])){
                        $total_upload = $row['upload'];
                    }
                    if(isset($row['download'])){
                        $total_download = $row['download'];   
                    }
                    
                }
                // get user previous dat
                $previous_data = $this->db->query("select sum(upload) as upload, sum(download) as download from sht_user_daily_data_usage where uid = '$uid' AND date(start_time) >= '$starting_date'");
                $total_previous_upload = 0;
                $total_previous_download = 0;
                if($previous_data->num_rows() > 0){
                    $row_previous_data = $previous_data->row_array();
                    if(isset($row_previous_data['upload'])){
                        $total_previous_upload = $row_previous_data['upload'];
                    }
                    if(isset($row_previous_data['download'])){
                         $total_previous_download = $row_previous_data['download'];
                    }
                   
                }
                $user_total_upload = $total_upload - $total_previous_upload;
                 if($user_total_upload < '0'){
                  $user_total_upload = 0;
                }
                $user_total_download = $total_download - $total_previous_download;
                if($user_total_download < '0'){
                  $user_total_download = 0;
                }
                //echo $uid."--".$total_upload."--".$total_previous_upload."--".$user_total_upload;
                //echo "<br />";
                $this->db->query("insert into sht_user_daily_data_usage(uid, upload, download, start_time, end_time, created_on) values('$uid', '$user_total_upload', '$user_total_download', '$start_time', '$end_time', now())");
                
            }
            echo "done";
        
        
        
        
        return $data;
    }
}


?>
