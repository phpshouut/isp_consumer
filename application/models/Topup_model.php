<?php
class Topup_model extends CI_Model{
    // isp uid apply
    public function user_current_topup(){
        $data = array();
        $user_uid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $i = 0;
        $get_otp_query = $this->db->query("select su.*,ss.srvname, ss.topuptype from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$user_uid' and su.terminate = '0' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' and su.topuptype != '1'  and su.topup_enddate >= CURDATE()");
        
        foreach($get_otp_query->result() as $row){
            $data[$i]['topupname'] = $row->srvname;
            $topuptype = '';
            if($row->topuptype == '2'){
                $topuptype = "Dataunacnttopup";
                $data[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                $data[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
            }else{
                $topuptype = "SpeedTopup";
                $data[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                $data[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
            }
            
            $data[$i]['topuptype'] = $topuptype;
            
            $i++;
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    
   
   // isp uid apply
    public function recommend_data_topup(){
        $data = array();
        $data_topup = array();
        $speed_topup = array();
        $data_unacc_topup = array();
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.id = '$userid'");
        //echo "<pre>";print_r($query->result());die;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            
            if($user_current_plan_type == 1){
                // user current plan is unlimite plan on suggestion show only speed plan
                $topup_to_show = array('3');
            }else{
                $topup_to_show = array('1', '2', '3');
            }
             $topup_to_show='"'.implode('", "', $topup_to_show).'"';
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.isp_uid = '$this->isp_uid' AND ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.topuptype IN($topup_to_show) and stp.payment_type = 'Paid' AND ss.is_private = '0'");
            //echo "<pre>";print_r($get_topup->result());die;
            if($get_topup->num_rows() > 0){
                $i = 0;
                $j = 0;
                $k = 0;
                foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                    
                        if($topup_type == '1'){//data topup
                            $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                            $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                            $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                            $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                            $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                            $i++;
                        }
                        elseif($topup_type == '2'){//data un.... 
                            $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                            $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                            //pricing
                            $unaccount_topup_price = 0;
                            $unaccount_topup_percent = 0;
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                            foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                $days[] = $get_unaccount_pricing->days;
                            }
                            $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                            $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                            if(count($days) == '7'){
                               $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                            $j++;
                        }
                        elseif($topup_type == '3'){//speed topup
                           $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                           $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                            $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                            
                            }else{
                               $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                            }
                            //pricing
                            $speed_topup_price = 0;
                        
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                            foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                               
                                $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                $days[] = $get_speed_pricing1->days;
                            }
                            $speed_topup[$k]['topup_price'] = $speed_topup_price;
                            if(count($days) == '7'){
                               $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                           $k++;
                        }
                    }
                    else{
                        
                        //$zone_ids = array();
                        $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        
                        if($is_valid_topup == 1){
                        
                           if($topup_type == '1'){//data topup
                                $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                                $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                                $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                                $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                                $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                                $i++;
                            }
                            elseif($topup_type == '2'){//data un.... 
                               $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                               $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                                //pricing
                                $unaccount_topup_price = 0;
                                $unaccount_topup_percent = 0;
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                                foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                    $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                    $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                    $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                    $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                    $days[] = $get_unaccount_pricing->days;
                                }
                                $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                                $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                                if(count($days) == '7'){
                                   $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                                $j++;
                            }
                            elseif($topup_type == '3'){//speed topup
                                $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                                 $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                                $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                    $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                                
                                }else{
                                   $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                                }
                                //pricing
                                $speed_topup_price = 0;
                            
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                    $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                                    $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                    $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                    $days[] = $get_speed_pricing1->days;
                                }
                                $speed_topup[$k]['topup_price'] = $speed_topup_price;
                                if(count($days) == '7'){
                                   $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                            
                                $k++;
                            } 
                        }
                    }
                }
            }
    
        }
        $data['data_topup'] = $data_topup;
        $data['speed_topup'] = $speed_topup;
        $data['data_unacc_topup'] = $data_unacc_topup;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
     
     // isp uid apply
     public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }
    
    
    
    // isp uid apply
    public function citrus_success($addorder = FALSE){
        $postdata=$this->input->post();
        $isp_uid = $this->isp_uid;
        $payment_platform = 2;
        $brand_id = '';
        $user_uid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        //echo "<pre>";print_r($postdata);die;
        $topup_id = '';
        if(isset($this->session->userdata['user_topup_recharge_session']['topup_id'])){
            $topup_id = $this->session->userdata['user_topup_recharge_session']['topup_id'];    
        }
        $topuptype = '';
        if(isset($this->session->userdata['user_topup_recharge_session']['topup_type'])){
            $topuptype = $this->session->userdata['user_topup_recharge_session']['topup_type'];    
        }
        if($addorder)
        {
            if($topuptype == 'datatopup'){
              // apply topup(data topup)
            
                $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                if($get_datacalc->num_rows() > 0){
                    $row = $get_datacalc->row_array();
                    
                    $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                    $row_data = $get_data->row_array();
                    $data_to_update = $row_data['datalimit'];
                    $topuptype = $row_data['topuptype'];
                    if($row['datacalc'] == '1'){
                        $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                    }else{
                        $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                    }
                    $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                }  
            }
            elseif($topuptype == 'speedtopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                
            }
            elseif($topuptype == 'dataunaccounttopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
            }
            
            // apply topup(data topup) end
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>$postdata['txnDateTime']);
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            $transection_id = $postdata['transactionId'];
            //insert into sht_subscriber_billing table
            $userid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
                $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
            }
            $actual_amount = $this->session->userdata['user_topup_recharge_session']['actual_cost'];
            $total_amount = $this->session->userdata['user_topup_recharge_session']['total_cost'];
            
            $bill_number = date('jnyHis');
            $this->db->query("insert into sht_subscriber_billing(isp_uid,subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id, payment_platform)
                             values('$isp_uid','$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id', '$payment_platform')");
            
             //insert into sht_subscriber_wallet
                
                $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$total_cost', now())");
                $wlastid = $this->db->insert_id();
                $wallet_billarr = array(
                      'isp_uid' => $isp_uid,
                      'subscriber_id' => '0',
                      'subscriber_uuid' => $user_uid,
                      'wallet_id' => $wlastid,
                      'bill_added_on' => date('Y-m-d H:i:s'),
                      'receipt_number' => $transection_id,
                      'bill_type' => 'addtowallet',
                      'payment_mode' => 'netbanking',
                      'actual_amount' => $total_amount,
                      'total_amount' => $total_amount,
                      'receipt_received' => '1',
                      'alert_user' => '0',
                      'bill_generate_by' => $isp_uid,
                      'bill_paid_on' => date('Y-m-d H:i:s'),
                      'wallet_amount_received' => '1',
                      "payment_platform" => $payment_platform
                );
                $this->db->insert('sht_subscriber_billing', $wallet_billarr);
                $wlast_billid = $this->db->insert_id();
    
                $receiptArr = array();
                $receiptArr['cheque_dd_paytm_number'] = $transection_id;
                $receiptArr['isp_uid'] = $isp_uid;
                $receiptArr['uid'] = $user_uid;
                $receiptArr['bill_id'] = $wlast_billid;
                $receiptArr['receipt_number'] = $transection_id;
                $receiptArr['receipt_amount'] = $total_amount;
                $receiptArr['added_on'] = date('Y-m-d H:i:s');
                $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
                if($checkrcpthistoryQ->num_rows() > 0){
                      $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
                }else{
                      $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
                }
            
            redirect(base_url()."topup/addFund_success_view/".$id);
        }
        else {
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            redirect(base_url()."topup/addFund_fail_view/".$id);
        }


    }
    
    // isp uid apply
         public function transection_detail($id){
        $data = array();
        $query = $this->db->query("select trns_resp from sht_topup_payment_responce where id = '$id'");
        $email = '';
        $authIdCode = 0;
        $txnDateTime = 0;
        $transactionId = 0;
        $issuerRefNo = 0;
        $TxRefNo = 0;
        $paytm_status = '';
      $paytm_tid = '';
      $RESPMSG = '';
      $ebs_tid = '';
      $ebs_msg = '';
      $ebs_status = '';
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = (isset($def->email))?$def->email:"";
            $authIdCode = (isset($def->authIdCode))?$def->authIdCode: "";
            $txnDateTime = (isset($def->txnDateTime))?$def->txnDateTime:"";
            $transactionId = (isset($def->transactionId))?$def->transactionId:"";
            $issuerRefNo = (isset($def->issuerRefNo))?$def->issuerRefNo:"";
            $TxRefNo = (isset($def->TxRefNo))?$def->TxRefNo:"";
            $paytm_status = (isset($def->STATUS))?$def->STATUS:"";
            $paytm_tid = (isset($def->TXNID))?$def->TXNID:"";
            $RESPMSG = (isset($def->RESPMSG))?$def->RESPMSG:"";
            $ebs_tid = (isset($def->TransactionID))?$def->TransactionID:"";
            $ebs_msg = (isset($def->ResponseMessage))?$def->ResponseMessage:"";
            if(isset($def->ResponseCode) && $def->ResponseCode == '0'){
                $ebs_status = "Success";
            }else{
                $ebs_status = 'Fail';
            }
        }
        $data['email'] = $email;
        $data['authIdCode'] = $authIdCode;
        $data['txnDateTime'] = $txnDateTime;
        $data['transactionId'] = $transactionId;
        $data['issuerRefNo'] = $issuerRefNo;
        $data['TxRefNo'] = $TxRefNo;
        $data['paytm_status'] = $paytm_status;
        $data['paytm_tid'] = $paytm_tid;
        $data['RESPMSG'] = $RESPMSG;
        $data['ebs_tid'] = $ebs_tid;
        $data['ebs_msg'] = $ebs_msg;
        $data['ebs_status'] = $ebs_status;
        return $data;
    }
    

    // isp uid apply
    public function check_already_speed_topup_apply($topupid, $week){
        
        $data = array();
        $user_uid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $today_date = date("Y-m-d");
        $get_current_speed_topup = $this->db->query("select speedtopup_enddate from sht_users where uid = '$user_uid'");
        if($get_current_speed_topup->num_rows() > 0){
            $row = $get_current_speed_topup->row_array();
            $speed_topup_enddate = $row['speedtopup_enddate'];
            if(strtotime($speed_topup_enddate) >= strtotime($today_date)){
                //already speed topup running
                $data['resultCode'] = '0'; 
            }else{
                //get topup detail
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $data['price'] = $total_days*$get_price_row['net_total'];
                $data['resultCode'] = '1';
            }
            
        }else{
            $data['resultCode'] = '0';
        }
        
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
   // isp uid apply
    public function calculate_speed_topup_price($topupid,$week){
        $data = array();
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $price = $total_days*$get_price_row['net_total'];
                
                $data['actual_price'] = $get_price_row['net_total'];
                $data['total_price'] = $price;
                $data['total_days'] = $total_days;
               return $data;
    }
    
   // isp uid apply
   public function check_already_data_unacc_topup_apply($topupid, $week){
        
        $data = array();
        $user_uid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $today_date = date("Y-m-d");
        $get_current_data_unacc_topup = $this->db->query("select dataunacnttopup_enddate from sht_users where uid = '$user_uid'");
        if($get_current_data_unacc_topup->num_rows() > 0){
            $row = $get_current_data_unacc_topup->row_array();
            $data_unacc_topup_enddate = $row['dataunacnttopup_enddate'];
            if(strtotime($data_unacc_topup_enddate) >= strtotime($today_date)){
                //already speed topup running
                $data['resultCode'] = '0'; 
            }else{
                //get topup detail
                $days = array();
                $get_data_unacc_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                foreach($get_data_unacc_pricing->result() as $get_data_unacc_pricing1){
                    $days[] = $get_data_unacc_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $data['price'] = $total_days*$get_price_row['net_total'];
                $data['resultCode'] = '1';
            }
            
        }else{
            $data['resultCode'] = '0';
        }
        
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    // isp uid apply
    public function calculate_data_unacc_topup_price($topupid,$week){
        $data = array();
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $price = $total_days*$get_price_row['net_total'];
                 $data['actual_price'] = $get_price_row['net_total'];
                $data['total_price'] = $price;
                 $data['total_days'] = $total_days;
               return $data;
    }
    
     public function payumoney_credential(){
            $isp_uid = $this->isp_uid;
            $merchent_key = '';
            $merchent_salt = '';
            $query = $this->db->query("select payu_merchantkey, payu_merchantsalt from sht_merchant_account where isp_uid = '$isp_uid'");
            if($query->num_rows() > 0){
                  $row = $query->row_array();
                  if($row['payu_merchantkey'] != '' && $row['payu_merchantsalt'] != ''){
                        $merchent_key = $row['payu_merchantkey'];
                        $merchent_salt = $row['payu_merchantsalt'];
                  }
            }
            $data['mer_key'] = $merchent_key;
            $data['salt'] = $merchent_salt;
            return $data;
      }
      public function payumoney_cancle(){
        $payment_platform = 2;
            $postdata=$this->input->post();
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
           
           $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstname'],"email"=>$postdata['email'],
                "mobile"=>$postdata['phone'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            redirect(base_url()."topup/payumoney_cancel_view/".$id);
      }
      public function payumoney_success(){
        $isp_uid = $this->isp_uid;
        $payment_platform = 2;
            $postdata=$this->input->post();
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
            $topup_id = '';
             if(isset($this->session->userdata['user_topup_recharge_session']['topup_id'])){
                 $topup_id = $this->session->userdata['user_topup_recharge_session']['topup_id'];    
             }
             $topuptype = '';
             if(isset($this->session->userdata['user_topup_recharge_session']['topup_type'])){
                 $topuptype = $this->session->userdata['user_topup_recharge_session']['topup_type'];    
             }
             //echo "here";die;
             if($topuptype == 'datatopup'){
              // apply topup(data topup)
            
                $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                if($get_datacalc->num_rows() > 0){
                    $row = $get_datacalc->row_array();
                    
                    $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                    $row_data = $get_data->row_array();
                    $data_to_update = $row_data['datalimit'];
                    $topuptype = $row_data['topuptype'];
                    if($row['datacalc'] == '1'){
                        $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                    }else{
                        $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                    }
                    $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                }  
            }
            elseif($topuptype == 'speedtopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                
            }
            elseif($topuptype == 'dataunaccounttopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
            }
            
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstname'],"email"=>$postdata['email'],
                "mobile"=>$postdata['phone'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            $transection_id = $postdata['txnid'];
            $actual_amount = $this->session->userdata['user_topup_recharge_session']['actual_cost'];
            $total_amount = $this->session->userdata['user_topup_recharge_session']['total_cost'];
            $bill_number = date('jnyHis');
            $this->db->query("insert into sht_subscriber_billing(isp_uid,subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id,payment_platform)
                             values('$isp_uid','$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id', '$payment_platform')");
            
            
            //insert into sht_subscriber_wallet
            $isp_uid = $this->isp_uid;
            $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$total_cost', now())");
	    $wlastid = $this->db->insert_id();
	    $wallet_billarr = array(
		  'isp_uid' => $isp_uid,
		  'subscriber_id' => '0',
		  'subscriber_uuid' => $user_uid,
		  'wallet_id' => $wlastid,
		  'bill_added_on' => date('Y-m-d H:i:s'),
		  'receipt_number' => $transection_id,
		  'bill_type' => 'addtowallet',
		  'payment_mode' => 'netbanking',
		  'actual_amount' => $total_amount,
		  'total_amount' => $total_amount,
		  'receipt_received' => '1',
		  'alert_user' => '0',
		  'bill_generate_by' => $isp_uid,
		  'bill_paid_on' => date('Y-m-d H:i:s'),
		  'wallet_amount_received' => '1',
                  "payment_platform" => $payment_platform,
	    );
	    $this->db->insert('sht_subscriber_billing', $wallet_billarr);
	    $wlast_billid = $this->db->insert_id();

	    $receiptArr = array();
	    $receiptArr['cheque_dd_paytm_number'] = $transection_id;
	    $receiptArr['isp_uid'] = $isp_uid;
	    $receiptArr['uid'] = $user_uid;
	    $receiptArr['bill_id'] = $wlast_billid;
	    $receiptArr['receipt_number'] = $transection_id;
	    $receiptArr['receipt_amount'] = $total_amount;
	    $receiptArr['added_on'] = date('Y-m-d H:i:s');
	    $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
	    if($checkrcpthistoryQ->num_rows() > 0){
		  $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
	    }else{
		  $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
	    }
            
            
            
            redirect(base_url()."topup/payumoney_success_view/".$id);
      }
      public function payumoney_transection_detail($id){
            $data = array();
        $query = $this->db->query("select trns_resp from sht_topup_payment_responce where id = '$id'");
        $email = '';
        $transactionId = 0;
       
        $transaction_error = '';
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = $def->email;
            $transactionId = $def->txnid;
            $transaction_error = $def->error_Message;
        }
        $data['email'] = $email;
        $data['transactionId'] = $transactionId;
        $data['error_Message'] = $transaction_error;
        return $data;
      }
    public function paytm_success($paytm_merchant_key, $paytm_merchant_mid, $paytm_merchant_web, $paytm_environment){
            $postdata=$this->input->post();
            $isp_uid = $this->isp_uid;
            $payment_platform = 2;
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
            $topup_id = '';
            if(isset($this->session->userdata['user_topup_recharge_session']['topup_id'])){
                $topup_id = $this->session->userdata['user_topup_recharge_session']['topup_id'];    
            }
            $topuptype = '';
            if(isset($this->session->userdata['user_topup_recharge_session']['topup_type'])){
                $topuptype = $this->session->userdata['user_topup_recharge_session']['topup_type'];    
            }
            if($postdata['STATUS'] == "TXN_SUCCESS"){
                if($topuptype == 'datatopup'){
                    // apply topup(data topup)
                  
                      $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                      if($get_datacalc->num_rows() > 0){
                          $row = $get_datacalc->row_array();
                          
                          $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                          $row_data = $get_data->row_array();
                          $data_to_update = $row_data['datalimit'];
                          $topuptype = $row_data['topuptype'];
                          if($row['datacalc'] == '1'){
                              $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                          }else{
                              $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                          }
                          $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                      }  
                  }
                  elseif($topuptype == 'speedtopup'){
                      $week = $this->session->userdata['user_topup_recharge_session']['week'];
                      $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                      $row_data = $get_data->row_array();
                      $topuptype = $row_data['topuptype'];
                      $start_date = date('Y-m-d',strtotime("+1 day"));
                      $end_date = date('Y-m-d',strtotime("+".$week." week"));
                      //update speed topup entery in sht_user table
                      $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                      // insert topup record
                      $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                      $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                      
                  }
                  elseif($topuptype == 'dataunaccounttopup'){
                      $week = $this->session->userdata['user_topup_recharge_session']['week'];
                      $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                      $row_data = $get_data->row_array();
                      $topuptype = $row_data['topuptype'];
                      $start_date = date('Y-m-d',strtotime("+1 day"));
                      $end_date = date('Y-m-d',strtotime("+".$week." week"));
                      //update speed topup entery in sht_user table
                      $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                      // insert topup record
                      $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                      $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                  }
                  
                  $responseParamList = '';
                  if (isset($_POST["ORDERID"]) && $_POST["ORDERID"] != "") {
                        // In Test Page, we are taking parameters from POST request. In actual implementation these can be collected from session or DB. 
                        $ORDER_ID = $_POST["ORDERID"];
                        // Create an array having all required parameters for status query.
                        $requestParamList = array("MID" => $paytm_merchant_mid , "ORDERID" => $ORDER_ID);  
                        $PAYTM_DOMAIN = "pguat.paytm.com";
                        if ($paytm_environment == 'PROD') {
                            $PAYTM_DOMAIN = 'securegw.paytm.in';
                        }
                        //define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
                        define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/merchant-status/getTxnStatus');
                        $StatusCheckSum = getChecksumFromArray($requestParamList,$paytm_merchant_key);
        
                        $requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
                        // Call the PG's getTxnStatusNew() function for verifying the transaction status.
                        $responseParamList = getTxnStatusNew($requestParamList);
                  }
                  
                    $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['TXNAMOUNT'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"1","trns_resp"=>json_encode($postdata),"tes_res"=>json_encode($responseParamList),"created_on"=>(isset($postdata['TXNDATE']))?$postdata['TXNDATE']:date("Y-m-d H:i:s"));
                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  $transection_id = $postdata['TXNID'];
                    $actual_amount = $this->session->userdata['user_topup_recharge_session']['actual_cost'];
                    $total_amount = $this->session->userdata['user_topup_recharge_session']['total_cost'];
                   $bill_number = date('jnyHis');
                    $this->db->query("insert into sht_subscriber_billing(isp_uid,subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id,payment_platform)
                             values('$isp_uid','$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id','$payment_platform')");
                    
                    //insert into sht_subscriber_wallet
            $isp_uid = $this->isp_uid;
            $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$total_cost', now())");
	    $wlastid = $this->db->insert_id();
	    $wallet_billarr = array(
		  'isp_uid' => $isp_uid,
		  'subscriber_id' => '0',
		  'subscriber_uuid' => $user_uid,
		  'wallet_id' => $wlastid,
		  'bill_added_on' => date('Y-m-d H:i:s'),
		  'receipt_number' => $transection_id,
		  'bill_type' => 'addtowallet',
		  'payment_mode' => 'netbanking',
		  'actual_amount' => $total_amount,
		  'total_amount' => $total_amount,
		  'receipt_received' => '1',
		  'alert_user' => '0',
		  'bill_generate_by' => $isp_uid,
		  'bill_paid_on' => date('Y-m-d H:i:s'),
		  'wallet_amount_received' => '1',
                  "payment_platform" => $payment_platform,
	    );
	    $this->db->insert('sht_subscriber_billing', $wallet_billarr);
	    $wlast_billid = $this->db->insert_id();

	    $receiptArr = array();
	    $receiptArr['cheque_dd_paytm_number'] = $transection_id;
	    $receiptArr['isp_uid'] = $isp_uid;
	    $receiptArr['uid'] = $user_uid;
	    $receiptArr['bill_id'] = $wlast_billid;
	    $receiptArr['receipt_number'] = $transection_id;
	    $receiptArr['receipt_amount'] = $total_amount;
	    $receiptArr['added_on'] = date('Y-m-d H:i:s');
	    $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
	    if($checkrcpthistoryQ->num_rows() > 0){
		  $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
	    }else{
		  $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
	    }
                  
                 redirect(base_url()."topup/paytm_success_view/".$id);
            }else{
                  $tabledata=array( "payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['TXNAMOUNT'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['TXNDATE']))?$postdata['TXNDATE']:date("Y-m-d H:i:s"));

                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  redirect(base_url()."topup/paytm_success_view/".$id);
            }
      }

        
        public function ebs_success($response){
            $payment_platform = 2;
            $postdata= $response;
            $isp_uid = $this->isp_uid;
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
            $topup_id = '';
            if(isset($this->session->userdata['user_topup_recharge_session']['topup_id'])){
                $topup_id = $this->session->userdata['user_topup_recharge_session']['topup_id'];    
            }
            $topuptype = '';
            if(isset($this->session->userdata['user_topup_recharge_session']['topup_type'])){
                $topuptype = $this->session->userdata['user_topup_recharge_session']['topup_type'];    
            }
            if($postdata['ResponseCode'] == "0"){
                if($topuptype == 'datatopup'){
                    // apply topup(data topup)
                  
                      $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                      if($get_datacalc->num_rows() > 0){
                          $row = $get_datacalc->row_array();
                          
                          $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                          $row_data = $get_data->row_array();
                          $data_to_update = $row_data['datalimit'];
                          $topuptype = $row_data['topuptype'];
                          if($row['datacalc'] == '1'){
                              $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                          }else{
                              $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                          }
                          $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                      }  
                  }
                  elseif($topuptype == 'speedtopup'){
                      $week = $this->session->userdata['user_topup_recharge_session']['week'];
                      $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                      $row_data = $get_data->row_array();
                      $topuptype = $row_data['topuptype'];
                      $start_date = date('Y-m-d',strtotime("+1 day"));
                      $end_date = date('Y-m-d',strtotime("+".$week." week"));
                      //update speed topup entery in sht_user table
                      $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                      // insert topup record
                      $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                      $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                      
                  }
                  elseif($topuptype == 'dataunaccounttopup'){
                      $week = $this->session->userdata['user_topup_recharge_session']['week'];
                      $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                      $row_data = $get_data->row_array();
                      $topuptype = $row_data['topuptype'];
                      $start_date = date('Y-m-d',strtotime("+1 day"));
                      $end_date = date('Y-m-d',strtotime("+".$week." week"));
                      //update speed topup entery in sht_user table
                      $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                      // insert topup record
                      $total_days = $this->session->userdata['user_topup_recharge_session']['total_days'];
                      $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                  }
                    $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['Amount'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['DateCreated']))?$postdata['DateCreated']:date("Y-m-d H:i:s"));
                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  $transection_id = $postdata['TransactionID'];
                    $actual_amount = $this->session->userdata['user_topup_recharge_session']['actual_cost'];
                    $total_amount = $this->session->userdata['user_topup_recharge_session']['total_cost'];
                    $bill_number = date('jnyHis');
                    $this->db->query("insert into sht_subscriber_billing(isp_uid,subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id,payment_platform)
                             values('$isp_uid','$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id','$payment_platform')");
                 redirect(base_url()."topup/ebs_success_view/".$id);
                 
                 //insert into sht_subscriber_wallet
                $isp_uid = $this->isp_uid;
                $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$total_cost', now())");
                $wlastid = $this->db->insert_id();
                $wallet_billarr = array(
                      'isp_uid' => $isp_uid,
                      'subscriber_id' => '0',
                      'subscriber_uuid' => $user_uid,
                      'wallet_id' => $wlastid,
                      'bill_added_on' => date('Y-m-d H:i:s'),
                      'receipt_number' => $transection_id,
                      'bill_type' => 'addtowallet',
                      'payment_mode' => 'netbanking',
                      'actual_amount' => $total_amount,
                      'total_amount' => $total_amount,
                      'receipt_received' => '1',
                      'alert_user' => '0',
                      'bill_generate_by' => $isp_uid,
                      'bill_paid_on' => date('Y-m-d H:i:s'),
                      'wallet_amount_received' => '1',
                      "payment_platform" => $payment_platform,
                );
                $this->db->insert('sht_subscriber_billing', $wallet_billarr);
                $wlast_billid = $this->db->insert_id();
    
                $receiptArr = array();
                $receiptArr['cheque_dd_paytm_number'] = $transection_id;
                $receiptArr['isp_uid'] = $isp_uid;
                $receiptArr['uid'] = $user_uid;
                $receiptArr['bill_id'] = $wlast_billid;
                $receiptArr['receipt_number'] = $transection_id;
                $receiptArr['receipt_amount'] = $total_amount;
                $receiptArr['added_on'] = date('Y-m-d H:i:s');
                $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
                if($checkrcpthistoryQ->num_rows() > 0){
                      $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
                }else{
                      $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
                }
                
            }else{
                  $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['Amount'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['DateCreated']))?$postdata['DateCreated']:date("Y-m-d H:i:s"));

                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  redirect(base_url()."topup/ebs_success_view/".$id);
            }
      }


        public function check_payment_gateway(){
            $isp_uid = $this->isp_uid;
            $query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
            $i = 0;
            $gatways = array();
            if($query->num_rows() > 0){
                  $data['resultCode'] = '1';
                  $data['resultMsg'] = 'Success';
                  $row = $query->row_array();
                  // for citrus
                  if($row['citrus_secret_key'] != '' || $row['citrus_post_url'] != '' || $row['citrus_vanity_url'] != ''){
                      $gatways[$i]['gatway_name'] = 'citrus';
                      $gatways[$i]['citrus_post_url'] = $row['citrus_post_url'];
                      $gatways[$i]['citrus_secret_key'] = $row['citrus_secret_key'];
                      $gatways[$i]['citrus_vanity_url'] = $row['citrus_vanity_url'];
                      $i++;
                  }
                  // for paytm
                  if($row['paytm_merchant_key'] != '' || $row['paytm_merchant_mid'] != '' || $row['paytm_merchant_web'] != '' || $row['paytm_environment'] != ''){
                      $gatways[$i]['gatway_name'] = 'paytm';
                      $gatways[$i]['paytm_merchant_key'] = $row['paytm_merchant_key'];
                      $gatways[$i]['paytm_merchant_mid'] = $row['paytm_merchant_mid'];
                      $gatways[$i]['paytm_merchant_web'] = $row['paytm_merchant_web'];
                      $gatways[$i]['paytm_environment'] = $row['paytm_environment'];
                      $i++;
                  }
                  // for payu 
                  if($row['payu_merchantid'] != '' || $row['payu_merchantkey'] != '' || $row['payu_merchantsalt'] != '' || $row['payu_authheader'] != ''){
                      $gatways[$i]['gatway_name'] = 'payu';
                      $gatways[$i]['payu_merchantid'] = $row['payu_merchantid'];
                      $gatways[$i]['payu_merchantkey'] = $row['payu_merchantkey'];
                      $gatways[$i]['payu_merchantsalt'] = $row['payu_merchantsalt'];
                      $gatways[$i]['payu_authheader'] = $row['payu_authheader'];
                      $i++;
                  }
                  // for EBS 
                  if($row['ebs_accountid'] != '' || $row['ebs_secretkey'] != '' || $row['ebs_salt'] != ''){
                      $gatways[$i]['gatway_name'] = 'EBS';
                      $gatways[$i]['ebs_accountid'] = $row['ebs_accountid'];
                      $gatways[$i]['ebs_secretkey'] = $row['ebs_secretkey'];
                      $gatways[$i]['ebs_account_name'] = $row['ebs_salt'];
                      $i++;
                  }
                  $data['gatways'] = $gatways;
            }else{
                  $data['resultCode'] = '0';
                  $data['resultMsg'] = 'No record found';
            }
            return $data;
      }

}


?>
