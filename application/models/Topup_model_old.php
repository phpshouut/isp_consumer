<?php
class Topup_model extends CI_Model{
    public function user_current_topup(){
        $data = array();
        $user_uid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $i = 0;
        $get_otp_query = $this->db->query("select su.*,ss.srvname, ss.topuptype from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$user_uid' and su.terminate = '0' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' and su.topuptype != '1'");
        
        foreach($get_otp_query->result() as $row){
            $data[$i]['topupname'] = $row->srvname;
            $topuptype = '';
            if($row->topuptype == '2'){
                $topuptype = "Dataunacnttopup";
                $data[$i]['topupstartdate'] = $row->topup_startdate;
                $data[$i]['topupenddate'] = $row->topup_enddate;
            }else{
                $topuptype = "SpeedTopup";
                $data[$i]['topupstartdate'] = $row->topup_startdate;
                $data[$i]['topupenddate'] = $row->topup_enddate;
            }
            
            $data[$i]['topuptype'] = $topuptype;
            
            $i++;
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function recommend_data_topup(){
        $data = array();
        $data_topup = array();
        $speed_topup = array();
        $data_unacc_topup = array();
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state from sht_users as su where su.id = '$userid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.topuptype IN('1','2','3') and stp.payment_type = 'Paid'");
            if($get_topup->num_rows() > 0){
                $i = 0;
                $j = 0;
                $k = 0;
                foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                    
                        if($topup_type == '1'){//data topup
                            $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                            $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                            $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                            $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                            $i++;
                        }
                        elseif($topup_type == '2'){//data un.... 
                            $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                            //pricing
                            $unaccount_topup_price = 0;
                            $unaccount_topup_percent = 0;
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                            foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                $days[] = $get_unaccount_pricing->days;
                            }
                            $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                            $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                            if(count($days) == '7'){
                               $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Saturday',$days) && in_array("Sunday", $days)){
                                $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Monday',$days) && in_array('Tuesday',$days)&& in_array('Wednesday',$days)&& in_array('Thursday',$days)&& in_array('Friday',$days)){
                                $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                            $j++;
                        }
                        elseif($topup_type == '3'){//speed topup
                           $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                            $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                            
                            }else{
                               $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                            }
                            //pricing
                            $speed_topup_price = 0;
                        
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                            foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                               
                                $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                $days[] = $get_speed_pricing1->days;
                            }
                            $speed_topup[$k]['topup_price'] = $speed_topup_price;
                            if(count($days) == '7'){
                               $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Saturday',$days) && in_array("Sunday", $days)){
                                $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Monday',$days) && in_array('Tuesday',$days)&& in_array('Wednesday',$days)&& in_array('Thursday',$days)&& in_array('Friday',$days)){
                                $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                           $k++;
                        }
                    }
                    else{
                        $zone_ids = array();
                        $get_plan_region = $this->db->query("select zone_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            $zone_ids[] = $get_plan_region1->zone_id;
                        }
                        if(in_array($user_zone, $zone_ids) || in_array('all', $zone_ids)){
                        
                           if($topup_type == '1'){//data topup
                                $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                                $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                                $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                                $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                                $i++;
                            }
                            elseif($topup_type == '2'){//data un.... 
                               $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                                //pricing
                                $unaccount_topup_price = 0;
                                $unaccount_topup_percent = 0;
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                                foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                    $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                    $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                    $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                    $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                    $days[] = $get_unaccount_pricing->days;
                                }
                                $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                                $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                                if(count($days) == '7'){
                                   $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Saturday',$days) && in_array("Sunday", $days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Monday',$days) && in_array('Tuesday',$days)&& in_array('Wednesday',$days)&& in_array('Thursday',$days)&& in_array('Friday',$days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                                $j++;
                            }
                            elseif($topup_type == '3'){//speed topup
                                $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                                $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                    $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                                
                                }else{
                                   $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                                }
                                //pricing
                                $speed_topup_price = 0;
                            
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                    $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                                    $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                    $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                    $days[] = $get_speed_pricing1->days;
                                }
                                $speed_topup[$k]['topup_price'] = $speed_topup_price;
                                if(count($days) == '7'){
                                   $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Saturday',$days) && in_array("Sunday", $days)){
                                    $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Monday',$days) && in_array('Tuesday',$days)&& in_array('Wednesday',$days)&& in_array('Thursday',$days)&& in_array('Friday',$days)){
                                    $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                            
                                $k++;
                            } 
                        }
                    }
                }
            }
    
        }
        $data['data_topup'] = $data_topup;
        $data['speed_topup'] = $speed_topup;
        $data['data_unacc_topup'] = $data_unacc_topup;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
     public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }
    
    
}


?>
