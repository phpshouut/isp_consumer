<?php
class Service_model extends CI_Model{
    // isp uid apply
    public function current_request(){
        $data = array();
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $query = $this->db->query("select * from sht_subscriber_tickets where subscriber_uuid = '$useruid' and status = '0' order by id desc");
        $i = 0;
        foreach($query->result() as $row){
            $data[$i]['reqiested_on'] = date("d.m.Y",strtotime($row->added_on));
            $data[$i]['ticket_type'] = ucfirst(str_replace('_', ' ', $row->ticket_type));
            $data[$i]['ticket_desc'] = $row->ticket_description;
            if($row->ticket_assign_to != '0'){
                $data[$i]['status'] = "In Progress ";
            }else{
              $data[$i]['status'] = "Open";  
            }
            $data[$i]['ticket_id'] = $row->ticket_id;
            
            $i++;
        }
        return $data;
    }
    
    public function service_type(){
        $isp_uid = $this->isp_uid;
        $data = array();
        $i = 0;
        $get = $this->db->query("select * from sht_tickets_type where isp_uid = '$isp_uid'");
        foreach($get->result() as $row){
            $data[$i]['ticket_type_value'] = $row->ticket_type_value;
            $data[$i]['ticket_type'] = $row->ticket_type;
            $i++;
        }
        return $data;
    }
    
    // isp uid apply
    public function generate_request(){
        $ticketid = date("j").date('n').date("y").date('His');
        $isp_uid = $this->isp_uid;
        $subscriber_id = $this->session->userdata['isp_consumer_session']['user_id'];
        $subscriber_uuid = $this->session->userdata['isp_consumer_session']['user_uid'];
        $ticket_type = $this->input->post("request_type");
        $ticket_priority = $this->input->post("request_priority");
        $ticket_description = $this->input->post("request_desc");
        if($ticket_type != '' && $ticket_priority != '' && $ticket_description != ''){
            $insert = $this->db->query("insert into sht_subscriber_tickets(isp_uid, ticket_id, subscriber_id, subscriber_uuid, ticket_type, ticket_priority,ticket_raise_by,ticket_description,status,added_on) values('$isp_uid','$ticketid', '$subscriber_id', '$subscriber_uuid', '$ticket_type', '$ticket_priority', '0', '$ticket_description','0', now())");
            // send notification
            $this->ticket_activation_alert($isp_uid,$subscriber_uuid,$ticket_type,$ticketid);
            return true;
        }
        else{
            return false;
        }
        
    }
    public function ticket_activation_alert($isp_uid,$uuid, $tickettype, $tkt_number){
       
       $username = $this->getcustomer_fullname($uuid);
       $mobileno = $this->getcustomer_mobileno($uuid);
       $ispdetArr = $this->getispdetails($isp_uid);
       $company_name = $ispdetArr['company_name'];
       $isp_name = $ispdetArr['isp_name'];
       $help_number = $ispdetArr['help_number'];
       
       $message = urlencode('Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered.').'%0a'.urlencode('Please allow us sometime to address your request, our team will ensure fastest possible action.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
       
       $sms_tosend = 'Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered. <br/> Please allow us sometime to address your request, our team will ensure fastest possible action. <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
       
       $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
       if($txtsmsQ->num_rows() > 0){
           foreach($txtsmsQ->result() as $txtsmsobj){
               $gupshup_user = $txtsmsobj->gupshup_user;
               $gupshup_pwd = $txtsmsobj->gupshup_pwd;
               $msg91authkey = $txtsmsobj->msg91authkey;
               $bulksmsuser = $txtsmsobj->bulksmsuser;
               $bulksms_pwd = $txtsmsobj->bulksms_pwd;
               $rightsms_user = $txtsmsobj->rightsms_user;
               $rightsms_pwd = $txtsmsobj->rightsms_pwd;
               $rightsms_sender = $txtsmsobj->rightsms_sender;
               $cloudplace_user = $txtsmsobj->cloudplace_user;
               $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
               $cloudplace_sender = $txtsmsobj->scope;
               
               $sender = $txtsmsobj->scope;
               
               if(($gupshup_user != '') && ($gupshup_pwd != '')){
                   $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif($msg91authkey != ''){
                   $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                   $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                   $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                   $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }else{
                   return 1;
               }
           }
       }
    }
    public function getcustomer_fullname($uuid){
        $mobileQ = $this->db->query("SELECT firstname,lastname FROM sht_users WHERE uid='".$uuid."'");
        $rdata = $mobileQ->row();
        return $rdata->firstname .' '. $rdata->lastname;
    }
    public function getcustomer_mobileno($uuid){
        $mobileQ = $this->db->query("SELECT mobile FROM sht_users WHERE uid='".$uuid."'");
        return $mobileQ->row()->mobile;
    }
    public function getispdetails($isp_uid){
        $data = array();
        $ispdetQ = $this->db->query("SELECT company_name,isp_name,help_number1 FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
        $rdata = $ispdetQ->row();
        $data['company_name'] = $rdata->company_name;
        $data['isp_name'] = $rdata->isp_name;
        $data['help_number'] = $rdata->help_number1;
        
        return $data;
    }
    public function gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message){
        $param = array();
        $request =""; //initialise the request variable
        $param['method']= "sendMessage";
        $param['send_to'] = "91"+$user_mobileno;
        $param['msg'] = $user_message;
        $param['userid'] = $gupshup_user;
        $param['password'] = $gupshup_pwd;
        $param['v'] = "1.1";
        $param['msg_type'] = "TEXT"; //Can be "FLASH�/"UNICODE_TEXT"/�BINARY�
        $param['auth_scheme'] = "PLAIN";
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
    }
    public function addmessage_todb($isp_uid, $uuid, $message){
        $this->db->insert("sht_users_notifications", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'notify_type' => 'SMS', 'message' => $message, 'added_on' => date('Y-m-d H:i:s')));
        return 1;
    }
    public function msg91_txtsmsgateway($msg91authkey, $phone, $msg, $sender) {
        $postData = array(
           'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
           'mobiles' => $phone,
           'message' => $msg,
           'sender' => $sender, //'SHOUUT',
           'route' => '4'
        );

        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
           //,CURLOPT_FOLLOWLOCATION => true
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }


    public function bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $phone, $msg){
        $bulksmsurl = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
        $postvalues = "user=".$bulksmsuser."&pwd=".$bulksms_pwd."&senderid=ABC&mobileno=".$phone."&msgtext=".$msg."&smstype=0/4/3";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
    public function rightchoice_txtsmsgateway($rcuser, $rc_pwd, $phone, $msg, $sender){
        $rcsmsurl = "http://www.sms.rightchoicesms.com/sendsms/groupsms.php?";
        $postvalues = "username=".$rcuser."&password=".$rc_pwd."&type=TEXT&mobile=".$phone."&sender=".$sender."&message=".$msg;
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($rcsmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender){
        $bulksmsurl = "http://203.212.70.200/smpp/sendsms?";
        $postvalues = "username=".$cloudplace_user."&password=".$cloudplace_pwd."&to=".$user_mobileno."&from=".$cloudplace_sender."&udh=&text=".$user_message."&dlr-mask=19";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
}


?>
