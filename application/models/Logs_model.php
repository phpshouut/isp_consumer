<?php
class Logs_model extends CI_Model{
   
    public function datausuage_logs(){
		$uuid = $this->input->post('uuid');
		//$uuid = '10000016';
		$date_range = $this->input->post('date_range');
		//$date_range = '26.08.2017 - 26.08.2017';
		$date_filter_from  = '';
		$date_filter_to = '';
		$date_range_explode = explode('-',$date_range);
		$date_filter_from = date('Y-m-d',strtotime($date_range_explode['0']));
		$date_filter_to = date('Y-m-d',strtotime($date_range_explode['1']));
		$total_upload = 0;
		$total_download = 0;
		$data = array();
		$query = $this->db->query("SELECT *, DAY( start_time ) as date, MONTH( start_time ) as month,  TIME( start_time ) as starttime , TIME( end_time ) as endtime  FROM sht_user_daily_data_usage WHERE uid='".$uuid."' AND DATE(start_time) BETWEEN '$date_filter_from' AND '$date_filter_to'");
		//echo $this->db->last_query();die;
		if($query->num_rows() > 0){
		$i = 0;
		foreach($query->result() as $uobj){
			$total_upload = $total_upload + $uobj->upload;
			$total_download = $total_download + $uobj->download;
			$starttime = $uobj->starttime;
			$endtime = $uobj->endtime;
			$date = $uobj->date;
			$dataconsumed = round( ((($uobj->upload) + ($uobj->download)) / (1024*1024)), 2 );
			if(($starttime == '00:00:00') && ($endtime == '11:59:59')){
				$data['firsthalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}elseif(($starttime == '12:00:00') && ($endtime == '23:59:59')){
				$data['secondhalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}
			$i++;
		}
		//$data = array_values($data);
		}
		if($total_upload > 0){
			$total_upload = round($total_upload/(1024*1024*1024),2);
		}else{
			$total_upload = "0";
		}
		$data['total_upload'] = $total_upload. " GB";
		if($total_download > 0){
			$total_download = round($total_download/(1024*1024*1024),2);
		}else{
			$total_download = "0";
		}
		$data['total_download'] = $total_download. " GB";
		$data['total_used'] = ($total_upload+$total_download). " GB";
		echo json_encode($data);
	}
    public function data_logs(){
		$db = array(
		    'dsn'	=> '',
		    'hostname' => '103.20.214.109',
		    'username' => 'decibelremote',
		    'password' => 'remote@giant',
		    'database' => 'decibelgraph',
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($db, TRUE);
		$data = array();
		$uuid = $this->input->post('uuid');
		//$uuid = '10000148';
		$date_filter  = $this->input->post('date_range');
		$date_filter_from = '';
		$date_filter_to = '';
		if($date_filter == 'today'){
		    $date_filter_from = date("Y-m-d");
		    $date_filter_to = date("Y-m-d");
		    $query = $dynamicDB->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'yesterday'){
		    $date_filter_from = date('Y-m-d', strtotime('-1 days'));
		    $date_filter_to = date('Y-m-d', strtotime('-1 days'));
		    $query = $dynamicDB->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_week'){
		    $date_filter_from = date('Y-m-d', strtotime('-6 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 1800");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_month'){
		    $date_filter_from = date('Y-m-d', strtotime('-30 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a %d %b') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 7200");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_year'){
		    $date_filter_from = date('Y-m-d', strtotime('-365 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a %d %b') as dates,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY DATE(date)");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		
		//echo "<pre>";print_r($data)
		echo json_encode($data);
		
	}
	
	public function usage_graph_billing_date($subscriber_uid){
		$query = $this->db->query("select plan_activated_date from sht_users where uid = '$subscriber_uid'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$from = date('d-m-Y', strtotime($row['plan_activated_date']));
			$to = date('d-m-Y');
		}else{
			$from = date('d-m-Y');
			$to = date('d-m-Y');
		}
		return $from.' - '.$to;
	}

}


?>
