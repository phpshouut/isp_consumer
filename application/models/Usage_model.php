<?php
class Usage_model extends CI_Model{
    // isp uid apply
    public function user_info(){
        $data = array();
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        $query = $this->db->query("select su.*, sc.city_name, sz.zone_name from sht_users as su left join sht_cities as sc on (su.city = sc.city_id)
                                  left join sht_zones as sz on (su.zone = sz.id) where su.id = '$userid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['name'] = $row['firstname']." ".$row['middlename'].' '.$row['lastname'];
            $data['uid'] = $row['uid'];
            $data['username'] = $row['username'];
            $data['email'] = $row['email'];
            $data['mobile'] = $row['mobile'];
            $data['address'] = $row['flat_number'].', '. $row['address'];
            $data['city'] = $row['city_name'];
            $data['zone'] = $row['zone_name'];
        }
        
        return $data;
    }
    
    
        public function live_usage_new(){
        $data = array();
        $live_usage =0;
        $percent = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        
         // get topup limit
  $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
  if($get_topup->num_rows() > 0){
   foreach($get_topup->result() as $tobj){
    $topupdata += $tobj->datalimit;
   }
  }
         // get user plan limit
    $get_plan = $this->db->query("select su.downlimit, su.comblimit,ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
    $plan_msg = '';
    $data_allocated = 0;
    $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                
                $data['uses_limit_msg'] = $plan_msg;
            }elseif($get_plan_row['plantype'] == '3'){
                
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }
        }
        
$plan_type = $get_plan_row['plantype'];
	  if($plan_type == '1'){// unlimite plan
	       $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where (username = '$useruid' )");
	       foreach($query->result() as $row){
		   $datacalc = $get_plan_row['datacalc'];
		  if($datacalc == 2){
		   $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
		  }else{
		   $live_usage = $live_usage + $row->acctoutputoctets;
		  }
	       }
	  }else{
	       $datacalc = $get_plan_row['datacalc'];
	       if($datacalc == '2'){
		    $data_avaliable = $get_plan_row['comblimit'];
		    if($data_avaliable < 0){
			 $live_usage = $get_plan_row['datalimit']+$topupdata;
		    }else{
			 $live_usage = ($get_plan_row['datalimit']+$topupdata) - $data_avaliable;
		    }
	       }else{
		    $data_avaliable = $get_plan_row['downlimit'];
		    if($data_avaliable < 0){
			 $live_usage = $get_plan_row['datalimit']+$topupdata;
		    }else{
			 $live_usage = ($get_plan_row['datalimit']+$topupdata) - $data_avaliable;
		    }
	       }
	  }
        $live_usage_value = round($live_usage/(1024*1024*1024),2);
    
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }
        $data['live_usage'] = $live_usage_value;
        
        if($data_allocated > 0){
            $percent = ($live_usage*100)/$data_allocated;
        }else{
            $percent = $live_usage/(1024*1024*1024);
        }
            if($percent > 100){
                $percent = 100;
            }
        $data['uses_percent'] = $percent;
        //die;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    

    // isp uid apply
    public function live_usage(){
        $data = array();
        $live_usage =0;
        $percent = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        
         // get topup limit
  $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
  if($get_topup->num_rows() > 0){
   foreach($get_topup->result() as $tobj){
    $topupdata += $tobj->datalimit;
   }
  }
         // get user plan limit
    $get_plan = $this->db->query("select ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
    $plan_msg = '';
    $data_allocated = 0;
    $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                
                $data['uses_limit_msg'] = $plan_msg;
            }elseif($get_plan_row['plantype'] == '3'){
                
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }
        }
        
        
        
        // get billing date
        /*$get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$this->isp_uid'");
        $start_date = ''; $end_date = '';
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            if(date("j") > $row_billing_date['billing_cycle']){
                //current month to next month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                //$start_date =  $date->format('Y-m-d');
                $start_date =  date('Y-m-d', strtotime('+1 day', strtotime($date->format('Y-m-d'))));
                $end_date = date('Y-m-d', strtotime('+1 month -1 day', strtotime($start_date)));
            }else{
                //previous month to current month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                //$end_date =  date('Y-m-d', strtotime('-1 day', strtotime($date->format('Y-m-d'))));
                $end_date =  $date->format('Y-m-d');
                $start_date = date('Y-m-d', strtotime('-1 month +1 day', strtotime($end_date)));
            }
        }*/
        $get_billing_date = $this->db->query("select plan_activated_date, next_bill_date from sht_users where uid = '$useruid'");
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            $start_date = date('Y-m-d', strtotime($row_billing_date['plan_activated_date']));
            $end = $row_billing_date['next_bill_date'];
            $end_date = date('Y-m-d', strtotime($row_billing_date['next_bill_date']));
        }
        // get user device macid
        $user_macid = array();
        $get_macid = $this->db->query("select hotspotMac from sht_user_hotspot_assoc where uid = '$useruid'");
        if($get_macid->num_rows() > 0){
            foreach($get_macid->result() as $get_macid_row){
                $user_macid[] = $get_macid_row->hotspotMac;        
            }
        }
        $user_macid = '"'.implode('", "', $user_macid).'"';
        $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where (username = '$useruid' OR username IN ($user_macid)) AND DATE(acctstarttime) between '$start_date' and '$end_date'");
       
        foreach($query->result() as $row){
            $datacalc = $get_plan_row['datacalc'];
           if($datacalc == 2){
            $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
           }else{
            $live_usage = $live_usage + $row->acctoutputoctets;
           }
        }
        $live_usage_value = round($live_usage/(1024*1024*1024),2);
    
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }
        $data['live_usage'] = $live_usage_value;
        
        if($data_allocated > 0){
            $percent = ($live_usage*100)/$data_allocated;
        }else{
            $percent = $live_usage/(1024*1024*1024);
        }
            if($percent > 100){
                $percent = 100;
            }
        $data['uses_percent'] = $percent;
        //die;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    
    // isp uid apply
    public function usage_logs($month = ''){
    
        $content = '';
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        $month = explode(" ", $month);
        //$current_month = date('m');
        $current_month = $month[0] ;
        //$current_year = date("Y");
        $current_year = $month[1];
        $query = $this->db->query("select * from sht_user_daily_data_usage where uid = '$useruid' AND MONTH(start_time) = '$current_month' AND YEAR(start_time) = '$current_year' order by id desc");
         
        //echo "<pre>";print_r($query->result());die;
        foreach($query->result() as $row){
            $content .= '<tr>
                        <td>'.date("d.m.Y H:i:s", strtotime($row->start_time)).'</td>
                        <td>'.date("d.m.Y H:i:s", strtotime($row->end_time)).'</td>
                        <td>'.round($row->download/(1024*1024),2).'</td>
                        <td>'.round($row->upload/(1024*1024),2).'</td>
                        
                    </tr>';
        }
        
        return $content;
    }
    
    
    // isp uid apply
    public function update_email(){
        $email_id = $this->input->post("user_email");
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        $this->db->query("update sht_users set email = '$email_id' where id = '$userid'");
    }
    
    
    // isp uid apply
    public function update_mobile(){
        $mobile = $this->input->post("user_new_mobile_hidden_update");
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        $this->db->query("update sht_users set mobile = '$mobile' where id = '$userid'");
    }
    private  $baseUrl = "http://sendotp.msg91.com/api";
    // isp uid apply
    public function send_otp(){
        $phone = $this->input->post("old_number");
       
        $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
        $data_string = json_encode($data);
        $ch = curl_init($this->baseUrl.'/generateOTP');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'Content-Length: ' . strlen($data_string),
     'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $otp_gets = '';
        $response = json_decode($result,true);
        if($response["status"] == "error"){
           
        }else{
            $otp_gets =  $response["response"]["oneTimePassword"];
        }
        
	if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
	    $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
	}
        $this->db->query("insert into sht_otp_verification (uid, otp, is_used) values('$useruid', '$otp_gets', '0')");
    }
    
    // isp uid apply
    public function verify_otp(){
        $msg = '';
        $useruid = '';
	if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
	    $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
	}
        $otp = $this->input->post("otp");
        $otp_veri = $this->db->query("select * from sht_otp_verification where uid = '$useruid' and otp = '$otp' and is_used = '0' order by id desc limit 1");
        if($otp_veri->num_rows() > 0){
            $row = $otp_veri->row_array();
            $id = $row['id'];
            $this->db->query("update sht_otp_verification set is_used = '1' where id = '$id'");
            $msg = 1;
        }else{
            $msg = 0;
        }
       return $msg;
    }
    
    
    
    // isp uid apply
    public function user_previous_data($useruid){
        $data = array();
        $upload = 0;
        $download = 0;
        $query = $this->db->query("select previous_upload, previous_download from sht_user_daily_data_usage where uid = '$useruid' order by id desc limit 1");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $upload = $row['previous_upload'];
            $download = $row['previous_download'];
        }
        $data['previous_upload'] = $upload;
        $data['previous_download'] = $download;
        return $data;
    }
    
   
}


?>
