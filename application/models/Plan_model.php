<?php
class Plan_model extends CI_Model{
    // isp uid apply
    public function user_plan(){
        $data = array();
         $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        $query = $this->db->query("select ss.plantype,su.uid,ss.srvname,ss.plantype,ss.downrate, ss.fupdownrate,ss.datalimit, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.id = '$userid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['planname'] = $row['srvname'];
            $data['rental'] = $row['net_total'];
            $download_speed = $this->convertTodata($row['downrate']."MB");
            if($download_speed < 1){
                $download_speed = $this->convertTodata($row['downrate']."KB");
                $data['download_speed'] = round($download_speed,0)." kbps"; 
            
            }else{
               $data['download_speed'] = round($download_speed,0)." mbps"; 
            }
            if($row['plantype'] == 3){
               $unlimited_speed = $this->convertTodata($row['fupdownrate']."MB");
                if($unlimited_speed < 1){
                    $unlimited_speed = $this->convertTodata($row['fupdownrate']."KB");
                    $data['unlimited_speed'] = round($unlimited_speed,0)." kbps"; 
                
                }else{
                   $data['unlimited_speed'] = round($unlimited_speed,0)." mbps"; 
                } 
            }
            if($row['plantype'] == 3){
                $fup_limit = $this->convertTodata($row['datalimit']."GB");
                $data['fup_limit'] = round($fup_limit,2)." GB". " / month";
            }
            else{
               $data['fup_limit'] = "None";  
            }
            //get num of topup assign to user
            $uid = $row['uid'];
           $get_otp_query = $this->db->query("select * from sht_usertopupassoc where uid = '$uid' and terminate = '0' and topuptype != '1' and topup_enddate >= CURDATE()");
           //echo $this->db->last_query();die;
            $data['topup'] = $get_otp_query->num_rows();
            
            //get user next scheduled plan
            $useruid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
            $get_scheduled_plan = $this->db->query("select snu.baseplanid, ss.srvname,ss.plantype from sht_nextcycle_userplanassoc as snu inner join  sht_services as ss on (snu.baseplanid = ss.srvid)  where snu.uid = '$useruid' and snu.status = '0'");
            if($get_scheduled_plan->num_rows() > 0){
                $row_scheduled = $get_scheduled_plan->row_array();
                $data['next_scheduled_plan'] = $row_scheduled['srvname'];
                $user_current_plan_type=  $row['plantype'];
                $get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$this->isp_uid' ");
                $row_billing_date = $get_billing_date->row_array();
                if(date("j") > $row_billing_date['billing_cycle']){
                    // date will be next month
                    $date = date('Y-m-d', strtotime('+1 month'));
                    $date = new DateTime($date);
                    $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                    $data['next_scheduled_plan_date'] =  $date->format('d-m-Y'); 
                    
                }else{
                    //date will be same month
                    $date = date('Y-m-d');
                    $date = new DateTime($date);
                    $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                     $data['next_scheduled_plan_date'] =  $date->format('d-m-Y');
                   
                }
                
            }
        }
        return $data;
    }
     
     // isp uid apply
     public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }
    
    
    // isp uid apply
    public function live_usage_new(){
        $data = array();
        $live_usage =0;
        $percent = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        
         // get topup limit
  $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
  if($get_topup->num_rows() > 0){
   foreach($get_topup->result() as $tobj){
    $topupdata += $tobj->datalimit;
   }
  }
         // get user plan limit
    $get_plan = $this->db->query("select su.downlimit, su.comblimit,ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
    $plan_msg = '';
    $data_allocated = 0;
    $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                
                $data['uses_limit_msg'] = $plan_msg;
            }elseif($get_plan_row['plantype'] == '3'){
                
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }
        }
        
        
        
        
$plan_type = $get_plan_row['plantype'];
	  if($plan_type == '1'){// unlimite plan
	       $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where (username = '$useruid' )");
	       foreach($query->result() as $row){
		   $datacalc = $get_plan_row['datacalc'];
		  if($datacalc == 2){
		   $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
		  }else{
		   $live_usage = $live_usage + $row->acctoutputoctets;
		  }
	       }
	  }else{
	       $datacalc = $get_plan_row['datacalc'];
	       if($datacalc == '2'){
		    $data_avaliable = $get_plan_row['comblimit'];
		    if($data_avaliable < 0){
			 $live_usage = $get_plan_row['datalimit']+$topupdata;
		    }else{
			 $live_usage = ($get_plan_row['datalimit']+$topupdata) - $data_avaliable;
		    }
	       }else{
		    $data_avaliable = $get_plan_row['downlimit'];
		    if($data_avaliable < 0){
			 $live_usage = $get_plan_row['datalimit']+$topupdata;
		    }else{
			 $live_usage = ($get_plan_row['datalimit']+$topupdata) - $data_avaliable;
		    }
	       }
	  }
        $live_usage_value = round($live_usage/(1024*1024*1024),2);
    
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }
        $data['live_usage'] = $live_usage_value;
        
        if($data_allocated > 0){
            $percent = ($live_usage*100)/$data_allocated;
        }else{
            $percent = $live_usage/(1024*1024*1024);
        }
            if($percent > 100){
                $percent = 100;
            }
        $data['uses_percent'] = $percent;
        //die;
        return $data;
    }
    
    
    
        public function live_usage(){
        $data = array();
        $live_usage =0;
        $percent = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        
         // get topup limit
  $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
  if($get_topup->num_rows() > 0){
   foreach($get_topup->result() as $tobj){
    $topupdata += $tobj->datalimit;
   }
  }
         // get user plan limit
    $get_plan = $this->db->query("select ss.plantype, ss.datalimit, ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
    $plan_msg = '';
    $data_allocated = 0;
    $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                
                $data['uses_limit_msg'] = $plan_msg;
            }elseif($get_plan_row['plantype'] == '3'){
                
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                $plan_msg = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB". "<br/> Monthly Data: ".$monthly_data."<br/> Topup Data: ".$topupdata;
                $data['uses_limit_msg'] =$plan_msg;
            }
        }
        
        
        
        // get billing date
        /*$get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$this->isp_uid'");
        $start_date = ''; $end_date = '';
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            if(date("j") > $row_billing_date['billing_cycle']){
                //current month to next month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                //$start_date =  $date->format('Y-m-d');
                $start_date =  date('Y-m-d', strtotime('+1 day', strtotime($date->format('Y-m-d'))));
                $end_date = date('Y-m-d', strtotime('+1 month -1 day', strtotime($start_date)));
            }else{
                //previous month to current month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                //$end_date =  date('Y-m-d', strtotime('-1 day', strtotime($date->format('Y-m-d'))));
                $end_date =  $date->format('Y-m-d');
                $start_date = date('Y-m-d', strtotime('-1 month +1 day', strtotime($end_date)));
            }
        }*/
        $get_billing_date = $this->db->query("select plan_activated_date, next_bill_date from sht_users where uid = '$useruid'");
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            $start_date = date('Y-m-d', strtotime($row_billing_date['plan_activated_date']));
            $end = $row_billing_date['next_bill_date'];
            $end_date = date('Y-m-d', strtotime($row_billing_date['next_bill_date']));
        }
        // get user device macid
        $user_macid = array();
        $get_macid = $this->db->query("select hotspotMac from sht_user_hotspot_assoc where uid = '$useruid'");
        if($get_macid->num_rows() > 0){
            foreach($get_macid->result() as $get_macid_row){
                $user_macid[] = $get_macid_row->hotspotMac;        
            }
        }
        $user_macid = '"'.implode('", "', $user_macid).'"';
        $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where (username = '$useruid' OR username IN ($user_macid)) AND DATE(acctstarttime) between '$start_date' and '$end_date'");
       
        foreach($query->result() as $row){
            $datacalc = $get_plan_row['datacalc'];
           if($datacalc == 2){
            $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
           }else{
            $live_usage = $live_usage + $row->acctoutputoctets;
           }
        }
        $live_usage_value = round($live_usage/(1024*1024*1024),2);
    
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }
        $data['live_usage'] = $live_usage_value;
        
        if($data_allocated > 0){
            $percent = ($live_usage*100)/$data_allocated;
        }else{
            $percent = $live_usage/(1024*1024*1024);
        }
            if($percent > 100){
                $percent = 100;
            }
        $data['uses_percent'] = $percent;
        //die;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    

    // isp uid apply
    public function recommended_plan(){
        $data = array();
        //get current plan rate
        $userid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_id'])){
            $userid = $this->session->userdata['isp_consumer_session']['user_id'];    
        }
        $query = $this->db->query("select su.zone, su.city, su.state,su.baseplanid, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.id = '$userid'");
        //echo "<pre>";print_r($query->result());die;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_active = $row['baseplanid'];
            $user_current_plan_price = $row['net_total'];
            //get all live plan
            $get_plan = $this->db->query("select ss.*, spp.region_type, spp.net_total from sht_services as ss  left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where ss.isp_uid = '$this->isp_uid' AND ss.plantype != '0' AND ss.is_private = '0' and ss.plantype != '2' and ss.srvid != '$user_current_plan_active' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0'");
        //echo "<pre>";print_r($get_plan->result());die;
            $i = 0;
            if($get_plan->num_rows() > 0){
                
                foreach($get_plan->result() as $get_plan1){
                    
                    // first check plan price is equal or grater
                    if($get_plan1->net_total >= $user_current_plan_price){
                       // if region_type  = allindia in sht_plan_pricing then show this plan dont check in plan region table
                       if($get_plan1->region_type == 'allindia'){
                            $data[$i]['service_id'] = $get_plan1->srvid;
                            $data[$i]['plan_name'] = $get_plan1->srvname;
                            $plan_type = '';
                            if($get_plan1->plantype == 1){
                                $plan_type = "Unlimited Plan";
                                $data[$i]['total_data'] = "&nbsp;";
                            }elseif($get_plan1->plantype == 2){
                                $plan_type = 'Time Plan';
                                $data[$i]['total_data'] = " ";
                            }
                            elseif($get_plan1->plantype == 3){
                                $plan_type = 'FUP Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $data[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                            }
                            elseif($get_plan1->plantype == 4){
                                $plan_type = 'Data Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $data[$i]['total_data'] = round($data_limit,2)." GB";
                            }
                            $data[$i]['plan_type'] = $plan_type;
                            $data[$i]['plan_cost'] = ceil($get_plan1->net_total);
                            $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                $data[$i]['speed'] = ceil($download_speed)." kbps";
                            }else{
                                $data[$i]['speed'] = ceil($download_speed)." mbps"; 
                            }
                            
                            
                            $i++;
                       }
                       else{
                        // get region is same as user from sht_plan_region table
                            $service_id = $get_plan1->srvid;
                            //$zone_ids = array();
                            $is_valid_offer = 0;
                            $get_plan_region = $this->db->query("select zone_id, city_id,state_id from sht_plan_region where plan_id = '$service_id'");
                            foreach($get_plan_region->result() as $get_plan_region1){
                                //$zone_ids[] = $get_plan_region1->zone_id;
                                //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_offer = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_offer = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_offer = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //echo "<br />".$is_valid_offer;
                            //echo "<pre>";print_r($zone_ids);die;
                            $is_fup_plan = '';
                            // now check region is same as user region or all
                            //if(in_array($user_zone, $zone_ids) || in_array('all', $zone_ids)){
                            if($is_valid_offer == 1){
                                $data[$i]['service_id'] = $get_plan1->srvid;
                                $data[$i]['plan_name'] = $get_plan1->srvname;
                                $plan_type = '';
                                if($get_plan1->plantype == 1){
                                    $plan_type = "Unlimited Plan";
                                    $data[$i]['total_data'] = "&nbsp;";
                                }elseif($get_plan1->plantype == 2){
                                    $plan_type = 'Time Plan';
                                    
                                    $data[$i]['total_data'] = "&nbsp;";
                                }
                                elseif($get_plan1->plantype == 3){
                                    $plan_type = 'FUP Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                    $data[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                                }
                                elseif($get_plan1->plantype == 4){
                                    $plan_type = 'Data Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                   $data[$i]['total_data'] = round($data_limit,2)." GB";
                                }
                                $data[$i]['plan_type'] = $plan_type;
                                $data[$i]['plan_cost'] = ceil($get_plan1->net_total);
                                $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                    $data[$i]['speed'] = ceil($download_speed)." <small>kbps</small>";
                                }else{
                                    $data[$i]['speed'] = ceil($download_speed)." <small>mbps</small>"; 
                                }
                           
                                
                                    $i++;
                    
                            }
                            
                       }//all india else end
                    }
                }
            }
        }
        //echo "<pre>";print_r($data);die;
        
               return $data;
    }
    
    
    // isp uid apply
    
        public function schedule_next_plan($service_id){
        $msg = '';
        //check already schedule plan if schedule then check same plan schedule which user request
        $useruid = '';
        if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
        // check user can change plan or not
        $is_plan_can_change = 1;
        $check = $this->db->query("select planautorenewal from sht_users where uid = '$useruid'");
        if($check->num_rows() > 0){
            $check_row = $check->row_array();
            $is_plan_can_change = $check_row['planautorenewal'];
        }
        if($is_plan_can_change == '1'){
            $query = $this->db->query("select * from sht_nextcycle_userplanassoc where uid = '$useruid' and status = '0'");
            if($query->num_rows() > 0){
                //already schedule , reschedule new plan
                $row = $query->row_array();
                $scheduled_service_id = $row['baseplanid'];
                if($scheduled_service_id == $service_id){
                    $msg = "This Plan already Scheduled";
                }else{
                    $id = $row['id'];
                    $update = $this->db->query("update sht_nextcycle_userplanassoc set baseplanid = '$service_id', updated_on = now() where id = '$id'");
                    $msg = "Successfully Scheduled requested  plan";
                }
            }
            else{
                //first time schedule plan
                $insert = $this->db->query("insert into sht_nextcycle_userplanassoc(uid, baseplanid, created_on) values('$useruid', '$service_id', now())");
                $msg = "Successfully Scheduled requested  plan";
            }
        }else{
           $msg = "Oops! You can't assign plan for your next cycle. Please contact to your ISP"; 
        }
        
        return $msg;
    }

}


?>
