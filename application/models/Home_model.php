<?php
class Home_model extends CI_Model{
    
    
     // isp id apply
    public function isp_detail(){
        $query = $this->db->query("select * from sht_isp_detail where isp_uid = '$this->isp_uid' limit 1");
        return $query;
    }
    // isp id apply
    public function slider_offer(){
        $data = array();
        $query = $this->db->query("select schsp.*, ss.srvname,ss.descr from sht_consumer_home_slider_promotion as schsp inner join sht_services as ss on (schsp.plan_id = ss.srvid) where schsp.isp_uid = '$this->isp_uid'");
        $i = 0;
        foreach($query->result() as $row){
            $data[$i]['plan_name'] = ucwords($row->srvname);
            $data[$i]['plan_desc'] = ucwords($row->descr);
            
            $data[$i]['banner_desc'] = ucwords($row->description);
            $data[$i]['image_path'] = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/promotion_image/slider_promotion/".$row->image_path;
            $i++;
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
   
    
    
    public function shop_data(){
        $content = "";
        for($i = 0; $i< 2; $i++){
            $content .= '<div class="col-sm-6 col-md-6 promo_col-6">
                                    <div class="promo_thumbnail">
                                       <div class="promo-img-container">
                                          <img class="promo-card-img-top img-responsive" src="'.base_url().'assets/images/zyme.png" alt="Card image cap">
                                          <div class="promo-img-footer">
                                             <h5>Zyme Bluetooth Smart Car Adapter</h5>
                                             <p>Trip history and trip log along with fuel costs and driving analysis Engine diagnosis for upto 6000 error codes.</p>
                                          </div>
                                       </div>
                                       <div class="caption">
                                          <p style="margin-bottom:0px">
                                             <a href="#" class="btn btn-primary promo-btn-left-footer">@ ? 2,499.00</a>
                                             <a href="#" class="btn btn-raised btn-sm promo-btn-footer pull-right" role="button">BUY NOW</a>
                                          </p>
                                       </div>
                                    </div>
                                 </div>';
        }
        return $content;
    }
    
   
    // isp id apply
    public function plan_topup_data(){
        $content = '';
        //get plan
        $plan_query = $this->db->query("select ss.srvid,ss.srvname,ss.plantype, ss.datalimit,ss.downrate, ss.fupdownrate,spp.net_total from sht_services as ss inner join sht_plan_pricing as spp on(ss.srvid = spp.srvid) where ss.plantype != '0' AND ss.plantype != '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' ANd ss.isp_uid = '$this->isp_uid' AND ss.is_private='0'");
        if($plan_query->num_rows() > 0){
            $unlimite_plan_count = 0;$fup_plan_count = 0; $data_plan_count = 0;
            $unlimitedclick = 'style="cursor : not-allowed"';
            $fupclick = 'style="cursor : not-allowed"';
            $dataclick = 'style="cursor : not-allowed"';
            foreach($plan_query->result() as $plan_query_count){
                if($plan_query_count->plantype == '1'){
                    $unlimitedclick = 'onclick="plan_type_plan(\'Unlimited\')"';
                    $unlimite_plan_count = $unlimite_plan_count+1;
                }
                elseif($plan_query_count->plantype == '3'){
                    $fupclick = 'onclick="plan_type_plan(\'Fup\')"';
                    $fup_plan_count = $fup_plan_count+1;
                }
                elseif($plan_query_count->plantype == '4'){
                    $dataclick = 'onclick="plan_type_plan(\'Data\')"';
                    $data_plan_count = $data_plan_count+1;
                }
            }
            $content .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <ul class="breadcrumb" style="margin-bottom: 5px;">
                                                <li>
                                                    <a href="javascript:void(0)" onclick="plan_type_plan(\'All\')">
                                                        <h4 class = "plan_type_plan_color_all" style = "color: #f00f64">
                                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                                            ALL PLANS
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li class="disabled">
                                                    <a href="javascript:void(0)"  '.$unlimitedclick.'>
                                                        <h4 class = "plan_type_plan_color_unlimited"> &nbsp;&nbsp;&nbsp;| UNLIMITED PLAN</h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)" '.$fupclick.'>
                                                        <h4 class = "plan_type_plan_color_fup"> &nbsp;&nbsp;&nbsp;| FUP Plan</h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)" '.$dataclick.'>
                                                        <h4 class = "plan_type_plan_color_data">&nbsp;&nbsp;&nbsp; | DATA PLAN</h4>
                                                    </a>
                                                </li>
                                            </ul>
                                       </div>
                                    </div>
                                    <!--div class="col-lg-6 col-md-6 col-sm-6">
                                       <div class="row">
                                                <h5><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                   Plans applicable for New Delhi Region Only
                                                </h5>
                                       </div>
                                    </div-->
                                 </div>
                              </div>
                           </div>';
            $content .= ' <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:30px; padding: 0px;">
                                 
                                 
                                 
                                <div class="row" >
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 pull-right">
                                            <nav class="slidernav">
                                          <div id="navbtns" class="col-lg-12 col-md-12 col-sm-12">
                                         <span href="#" class="previous pull-left">
                                         <i class="fa fa-angle-left" aria-hidden="true"></i>
                                         </span>
                                         <span href="#" class="next pull-right">
                                           <i class="fa fa-angle-right" aria-hidden="true"></i>
                                         </span>
                                          </div>
                                         </nav>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 0px;">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding-right:0px ;margin-top:5px;">
                                      
                                            <table class="table table-condensed" style="">
                                                <tr><td class="table_crsl">Plan Name</td></tr>
                                                <tr><td class="table_crsl">Plan Type</td></tr>
                                                <tr><td class="table_crsl">Monthly Rental</td></tr>
                                               <tr><td class="table_crsl">Speed</td></tr>
                                               <tr><td class="table_crsl">Data</td></tr>
                                               
                                            </table>
                                          </div> 
                                        
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="margin-top:0px;">
                                            <div class="row">
                                                <div id="w">
                                                    
                                                    <div class="crsl-items" data-navigation="navbtns">
                                                        <div class="crsl-wrap" id="plan_slider_content">';
                                                       foreach($plan_query->result() as $plan_query_row){
                                                             $srvid = $plan_query_row->srvid;
                                                            $speed = '';$fupspeed = '';
                                                            $download_speed = $this->convertTodata($plan_query_row->downrate."MB");
                                                            if($download_speed < 1){
                                                                    $download_speed = $this->convertTodata($plan_query_row->downrate."KB");
                                                                    $speed = round($download_speed,0)." kbps"; 
                                                                }else{
                                                                   $speed = round($download_speed,0)." mbps"; 
                                                                }
                                                                $palntype = '';$total_data = '';
                                                                if($plan_query_row->plantype == '1'){
                                                                    $palntype = 'Unlimited Plan';
                                                                    $total_data = "Unlimited";
                                                                }
                                                                elseif($plan_query_row->plantype == '3'){
                                                                    $palntype = 'FUP Plan';
                                                                    $data_limit = $this->convertTodata($plan_query_row->datalimit."GB");
                                                                    $total_data = round($data_limit,2)." GB (FUP)";
                                                                    $unlimited_speed = $this->convertTodata($plan_query_row->fupdownrate."MB");
                                                                    if($unlimited_speed < 1){
                                                                        $unlimited_speed = $this->convertTodata($plan_query_row->fupdownrate."KB");
                                                                        $fupspeed = ' / '.round($unlimited_speed,0)." kbps"; 
                                                                    
                                                                    }else{
                                                                       $fupspeed = ' / '.round($unlimited_speed,0)." mbps"; 
                                                                    } 
                                                                }
                                                                elseif($plan_query_row->plantype == '4'){
                                                                    $palntype = 'Data Plan';
                                                                    $data_limit = $this->convertTodata($plan_query_row->datalimit."GB");
                                                                    $total_data = round($data_limit,2)." GB";
                                                                }
                                                                
                                                                $content .= '<a class="crsl-item">
                                                             <table class="table table-condensed">
                                                               <tr><td class="table_crsl_item "><b>'.$plan_query_row->srvname.'</b></td></tr>
                                                                <tr><td class="table_crsl_item">'.$palntype.'</td></tr>
                                                               <tr><td class="table_crsl_item">&#8377; '.$plan_query_row->net_total.'</td></tr>
                                                               <tr><td class="table_crsl_item">'.$speed.$fupspeed.'</td></tr>
                                                               <tr><td class="table_crsl_item">'.$total_data.'</td></tr>
                                                              
                                                               <tr><td class="table_crsl_item_btns"><span class="table_btn" style="margin: 15px 0px" onclick = "plan_topup_modal(\''.$srvid.'\')">GET THIS</span></td></tr>
                                                              </table>
                                                            </a>';
                                                         }
                                                           
                                                            
                                        $content .=    '</div>
                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 
                             </div>
                           </div>';
                           
                                 
                           
            // topup code here start
            $content .= '<div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:15px">
                                 <div class="row">
                                    <h4><i class="fa fa-bolt" aria-hidden="true"></i>
                                             RECOMMENDED TOP-UPS
                                    </h4>
                                 </div>
                              </div>
                           </div>';
                           
            $content .= '<div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="row">';
                              
            // speed topup start
            $speed_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.downrate,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '3' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$this->isp_uid' AND ss.is_private='0'");
            if($speed_topup_query->num_rows() > 0){
                $content .= '<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:20px">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                      <h3>SPEED TOP-UP</h3>
                                             </div>
                                             <!--div class="col-lg-6 col-md-6 col-sm-6">
                                                      <a href="#" class="pull-right" style="font-weight:300">See All Speed Top-Ups</a>
                                             </div-->
                                          </div>
                                          <p>Get on-demand speed burst to watch that movie online or to attend that super important web conference meeting</p>
                                       </div>
                                    </div>
                                    <div class="row">';
                foreach($speed_topup_query->result() as $speed_topup_query_row){
                    $speed = '';$topupid = $speed_topup_query_row->srvid;
                    $topup_days = '';
                    $speed_topup_price = 0;
                    $start_time = '';$end_time = '';
                            $days = array();
                    $download_speed = $this->convertTodata($speed_topup_query_row->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($speed_topup_query_row->downrate."KB");
                                $speed = round($download_speed,2)." kbps"; 
                            
                            }else{
                               $speed = round($download_speed,2)." mbps"; 
                            }
                            $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                            foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                $speed_topup_price = $speed_topup_price + $speed_topup_query_row->net_total;
                                $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                $days[] = $get_speed_pricing1->days;
                            }
                            if(count($days) == '7'){
                               $topup_days = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                               $topup_days = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $topup_days = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $topup_days = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                    $content .= '
                                          <div class="plan_blue_box">
                                             <div class="caption" style="margin-bottom:15px">
                                                      <h6 style = "margin: 5px 0px">'.$speed_topup_query_row->srvname.'</h6>
                                                      <h6 style = "margin: 5px 0px">'.$topup_days.'</h6>
                                                      
                                                      <h4>'.$speed.'</h4>
                                                      <h4>&#8377; '.$speed_topup_price.' / <small>Week</small></h4>
                                             </div>
                                             <!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                         <select class="form-control form-control_select">
                                                            <option>Top-Up for 1hr</option>
      
                                                         </select>
                                                </div>
                                             </div-->
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick = "plan_topup_modal(\''.$topupid.'\')">GET THIS</a>
                                                   </div>
                                                   <!--div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right">View Details</a>
                                                   </div-->
                                                </div>
                                             </div>
                                          </div>
                                       ';
                                    
                }
                $content .= '</div>
                                 </div>';
                              
            }
            //speed topup end
            
            // data topup start
            $data_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.datalimit,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '1' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$this->isp_uid' AND ss.is_private='0'");
            if($data_topup_query->num_rows() > 0){
                $content .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                      <h3>DATA TOP-UP</h3>
                                             </div>
                                             <!--div class="col-lg-6 col-md-6 col-sm-6">
                                                      <a href="#" class="pull-right" style="font-weight:300">See All Data Top-Ups</a>
                                             </div-->
                                          </div>
                                          <p>Ran out of data / FUP limit? Just add a top-up and get back your speed!</p>
                                       </div>
                                    </div>
                                    <div class="row">';
                foreach($data_topup_query->result() as $data_topup_query_row){
                    $topupid = $data_topup_query_row->srvid;
                    $data_limit = $this->convertTodata($data_topup_query_row->datalimit."GB");
                    $data_limit = round($data_limit,2)." GB ";
                    $toupu_price =  $data_topup_query_row->net_total;
                    $content .= ' 
                                          <div class="plan_red_box">
                                             <div class="caption">
                                                      <h6 style = "margin: 5px 0px">'.$data_topup_query_row->srvname.'</h6>
                                                      <h4>'.$data_limit.'</h4>
                                                      <h4>&#8377; '.$toupu_price.'</h4>
                                             </div>
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick = "plan_topup_modal(\''.$topupid.'\')">GET THIS</a>
                                                   </div>
                                                   <!--div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right">View Details</a>
                                                   </div-->
                                                </div>
                                             </div>
                                          </div>
                                       ';
                }
                $content .= ' </div>
                                 </div>';
            }
            //data topup end
            
            
            //data unaccount topup start
            $dataunaccount_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.downrate,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$this->isp_uid' AND ss.is_private='0'");
            if($dataunaccount_topup_query->num_rows() > 0){
                $content .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>
FREE DATA TOP-UPS</h3>
                                             </div>
                                             <!--div class="col-lg-6 col-md-6 col-sm-6">
                                                      <a href="#" class="pull-right" style="font-weight:300">See All Monthly Top-Ups</a>
                                             </div-->
                                          </div>
                                          <p>Ran out of data / FUP limit? Just add a top-up and get back your speed!</p>
                                       </div>
                                    </div>
                                    <div class="row">';
                foreach($dataunaccount_topup_query->result() as $dataunaccount_topup_query_row){
                    $topupid = $dataunaccount_topup_query_row->srvid;
                    $unaccount_topup_price = 0;$unaccount_topup_percent = 0;
                    $topup_days = '';
                    $start_time = '';
                            $end_time = '';
                            $days = array();
                    $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                    foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                $unaccount_topup_price = $unaccount_topup_price + $dataunaccount_topup_query_row->net_total;
                                $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                 $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                $days[] = $get_unaccount_pricing->days;
                            }
                            if(count($days) == '7'){
                               $topup_days = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                $topup_days = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $topup_days = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $topup_days = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                    $content .= ' 
                                          <div class="plan_pink_box">
                                             <div class="caption">
                                                      <h6 style = "margin: 5px 0px">'.$dataunaccount_topup_query_row->srvname.'</h6>
                                                      <h6 style = "margin: 5px 0px">'.$topup_days.'</h6>
                                                      <br />
                                                      <h4>'.$unaccount_topup_percent.'% free data</h4>
                                                      <h4>&#8377; '.$unaccount_topup_price.' / Week</h4>
                                             </div>
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick = "plan_topup_modal(\''.$topupid.'\')">GET THIS</a>
                                                   </div>
                                                   <!--div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right">View Details</a>
                                                   </div-->
                                                </div>
                                             </div>
                                          </div>
                                       
                                    ';
                }
                $content .= '</div>
                                 </div>';
            }
            // data unaccount topup end
            $content .= '</div>
                           </div>';
                           
        // topup end
        }
        return $content;
    }
    
   
   
   // isp id apply
    public function plan_type_plan($plan_type){
        $plantypewhere = '';
        if($plan_type == 'Unlimited'){
            $plantypewhere = 'AND ss.plantype = "1"';
        }
        elseif($plan_type == 'Fup'){
            $plantypewhere = 'AND ss.plantype = "3"';
        }
        elseif($plan_type == 'Data'){
            $plantypewhere = 'AND ss.plantype = "4"';
        }
        $content = '';
        $plan_query = $this->db->query("select ss.srvid,ss.srvname,ss.plantype, ss.datalimit,ss.downrate, ss.fupdownrate,spp.net_total from sht_services as ss inner join sht_plan_pricing as spp on(ss.srvid = spp.srvid) where ss.plantype != '0' AND ss.plantype != '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$this->isp_uid' AND ss.is_private='0' $plantypewhere");
         foreach($plan_query->result() as $plan_query_row){
                                                             $srvid = $plan_query_row->srvid;
                                                            $speed = '';$fupspeed = '';
                                                            $download_speed = $this->convertTodata($plan_query_row->downrate."MB");
                                                            if($download_speed < 1){
                                                                    $download_speed = $this->convertTodata($plan_query_row->downrate."KB");
                                                                    $speed = round($download_speed,0)." kbps"; 
                                                                }else{
                                                                   $speed = round($download_speed,0)." mbps"; 
                                                                }
                                                                $palntype = '';$total_data = '';
                                                                if($plan_query_row->plantype == '1'){
                                                                    $palntype = 'Unlimited Plan';
                                                                    $total_data = "Unlimited";
                                                                }
                                                                elseif($plan_query_row->plantype == '3'){
                                                                    $palntype = 'FUP Plan';
                                                                    $data_limit = $this->convertTodata($plan_query_row->datalimit."GB");
                                                                    $total_data = round($data_limit,2)." GB (FUP)";
                                                                    $unlimited_speed = $this->convertTodata($plan_query_row->fupdownrate."MB");
                                                                    if($unlimited_speed < 1){
                                                                        $unlimited_speed = $this->convertTodata($plan_query_row->fupdownrate."KB");
                                                                        $fupspeed = ' / '.round($unlimited_speed,0)." kbps"; 
                                                                    
                                                                    }else{
                                                                       $fupspeed = ' / '.round($unlimited_speed,0)." mbps"; 
                                                                    } 
                                                                }
                                                                elseif($plan_query_row->plantype == '4'){
                                                                    $palntype = 'Data Plan';
                                                                    $data_limit = $this->convertTodata($plan_query_row->datalimit."GB");
                                                                    $total_data = round($data_limit,2)." GB";
                                                                }
                                                                
                                                                $content .= '<a class="crsl-item">
                                                             <table class="table table-condensed">
                                                               <tr><td class="table_crsl_item "><b>'.$plan_query_row->srvname.'</b></td></tr>
                                                                <tr><td class="table_crsl_item">'.$palntype.'</td></tr>
                                                               <tr><td class="table_crsl_item">&#8377; '.$plan_query_row->net_total.'</td></tr>
                                                               <tr><td class="table_crsl_item">'.$speed.$fupspeed.'</td></tr>
                                                               <tr><td class="table_crsl_item">'.$total_data.'</td></tr>
                                                              
                                                               <tr><td class="table_crsl_item_btns"><span class="table_btn" style="margin: 15px 0px" onclick = "plan_topup_modal(\''.$srvid.'\')">GET THIS</span></td></tr>
                                                              </table>
                                                            </a>';
                                                         }
        return $content;
    }
    
    // isp id apply
    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }
    
   
   // isp id apply
    public function about_us_data(){
        $content = '';
        $query = $this->db->query("select about_us from sht_isp_detail where isp_uid = '$this->isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $content .= $row['about_us'];
        }
        return $content;
    }
    
    // isp id apply
    public function services_data(){
        $content = '';
        $query = $this->db->query("select services from sht_isp_detail where isp_uid = '$this->isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $content .= $row['services'];
        }
        return $content;
    }
    
    // isp id apply
    public function contact_us_data(){
        $content = '';
        $content .= '<div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                       <h4><i class="fa fa-phone" aria-hidden="true"></i>
                                                CONTACT US
                                       </h4>
                                    </div>';
        $query = $this->db->query("select * from sht_isp_detail where isp_uid = '$this->isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            
            $content .= '<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:25px">
                                             <h3>Head Office</h3>
                                             <h5><strong>'.$row['company_name'].'</strong></h5>
                                             <h6>'.$row['address1'].'</h6>
                                            
                                             <h6>'.$row['city'].' - '.$row['pincode'].'</h6>
                                          </div>';
                                          
            $content .= '<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:25px">
                                             <h3>Service Request Centre</h3>
                                             <h6>'.$row['help_number1'].'</h6>
                                             <h6>'.$row['help_number2'].'</h6>
                                             <h6 style="margin-bottom:20px">'.$row['help_number3'].'</h6>
                                             <h6><h6>'.$row['support_email'].'</h6></h6>
                                          </div>';
        }
        $content .= '<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:25px">
                                       <h3>Reach Us</h3>
                                       <div class="row">
                                          <div class="col-lg-6 col-md-6 col-sm-6">
                                            
                                                <div class="form-group" style="padding-bottom:2px">
                                                   <input type="text" class="form-control" id="contact_us_name" name = "contact_us_name" placeholder="Name">
                                                </div>
                                                <div class="form-group" style="padding-bottom:2px">
                                                         <input type="email" class="form-control" id="contact_us_email" name = "contact_us_email" placeholder="Email">
                                                </div>
                                                <div class="form-group" style="padding-bottom:2px">
                                                         <input type="text" class="form-control" id="contact_us_mobile" name="contact_us_mobile" placeholder="Mobile No.">
                                                </div>
                                                <div class="form-group" style="padding-bottom:2px">
                                                         <textarea class="form-control" rows="3" placeholder="Message" name = "contact_us_msg" id="contact_us_msg"></textarea>
                                                </div>
                                                    <p style = "color:red" id="contact_us_error"></p>
                                                      <button type="button" class="btn btn-raised btn-danger" onclick="request_contact_us()">Submit</button>
                                             
                                          </div>
                                       </div>
                                    </div>';
        $content .= '</div>';
        $lat = '28.5403141';
                                 $long = '77.2263366';
                                 if(isset($row['lat']) && $row['lat'] != ''){
                                    $lat = $row['lat'];
                                 }
                                 if(isset($row['long']) && $row['long'] != ''){
                                    $long = $row['long'];
                                 }
        $content .='<div class="col-lg-5 col-md-5 col-sm-5">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 nopadding-left">
                                          <div class="map-col">
                                                   <h3>Get There</h3>
                                             <div class="row-fluid">
                                                <div class="span8" style="margin-bottom: 15px; border:2px solid #6D6E71; height: 350px;" id="map1">
                                                         <!--iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.9802255181703!2d77.22633661467786!3d28.540314082454202!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce2258eee9bc1%3A0x4fcbf84c137046ff!2sPrachin+Shiv+Mandir!5e0!3m2!1sen!2sin!4v1495540937094" width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen></iframe-->
                                                </div>
                                                <span><i class="fa fa-caret-right" aria-hidden="true" style="color:#231F20; font-size:20px"></i>
                                                      <a href="https://www.google.co.in/maps/place/'.$lat.','.$long.'" target="_blank" style="font-weight:300">View on Google Maps</a></span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>';
                                 
                                 echo "<script>
                                        var myLocation = new google.maps.LatLng(".$lat.", ".$long.");
                                            var mapOptions = {
                                                center: myLocation,
                                                zoom: 10
                                            };
                                            var marker = new google.maps.Marker({
                                                position: myLocation,
                                                title: 'Property Location'
                                            });
                                            var map = new google.maps.Map(document.getElementById('map1'),mapOptions);
                                            marker.setMap(map);
                                 echo </script>";
        return $content;
    }
    
     
    // isp id apply
    public function promotion_data(){
        $content = '';
        $query = $this->db->query("select * from sht_consumer_marketing_promotion where start_date <= now() AND end_date >= now() and is_deleted = '0' and status = '1' AND isp_uid = '$this->isp_uid'");
       
        foreach($query->result() as $row){
            $content .= '<div class="col-sm-6 col-md-6  promo_col-6">
                                    <div class="promo_thumbnail">
                                       <div class="promo-img-container">
                                          <img class="promo-card-img-top img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/promotion_image/marketing_promotion/'.$row->image_path.'" alt="Card image cap">
                                          <div class="promo-img-footer">
                                             <h5>'.$row->title.'</h5>
                                             <p>'. $row->description.'</p>
                                          </div>
                                       </div>
                                       <div class="caption">
                                          <p style="margin-bottom:0px">
                                             <a href="#" class="btn btn-primary promo-btn-left-footer">'.$row->promotion_offer.'</a>
                                             <a href="'.$row->redirect_link.'" target="_blank" class="btn btn-raised btn-sm promo-btn-footer pull-right" role="button">SUBSCRIBE</a>
                                          </p>
                                       </div>
                                    </div>
                                 </div>';
          
        }
        //echo "<pre>";print_r($data);die;
        return $content;
    }
    
    
    // isp id apply
    public function request_plan_topup(){
        $data = '';
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $mobile = $this->input->post("mobile");
        $srvid = '';
        if($this->input->post('srvid') != ''){
            $srvid = $this->input->post('srvid');
        }
        $msg = $this->input->post("msg");
        $this->db->query("insert into sht_comsumer_request(name,email,mobile,msg,requested_on,srvid, isp_uid) values('$name', '$email', '$mobile', '$msg', now(), '$srvid', '$this->isp_uid')");
        return $data;
    }
}


?>
