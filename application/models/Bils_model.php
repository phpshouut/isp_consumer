<?php
class Bils_model extends CI_Model{
    // isp uid apply
      public function bils_model(){
	    $data = array();
	    $useruid = '';
	    if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
		$useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
	    }
	    //$query = $this->db->query("select * from sht_subscriber_billing where subscriber_uuid = '$useruid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security'  order by id desc");
	    $year_filter = date('Y');
	    $query = $this->db->query("select * from sht_subscriber_billing where subscriber_uuid = '$useruid' and (bill_type = 'midchange_plancost' OR bill_type = 'montly_pay' OR bill_type = 'topup') AND YEAR(bill_added_on) = '$year_filter'  order by id desc");
	    $i = 0;
	    foreach($query->result() as $row){
		  // echo strtotime($row->bill_paid_on)."<br />";
		  $topup_name = '';
		  $data[$i]['id'] = $row->id;
		  $data[$i]['bill_added_on'] = $row->bill_added_on;
		  $data[$i]['generated_on'] = date("d.m.Y",strtotime($row->bill_added_on));
		  $data[$i]['paid_on'] = ($row->bill_paid_on != '0000-00-00 00:00:00')?date("d.m.Y",strtotime($row->bill_paid_on)):"-";
		  /*if(ucwords($row->bill_type) == 'Topup'){
			$plan_id = $row->plan_id;
			$get_toup_name = $this->db->query("select srvname from sht_services where srvid = '$plan_id'");
			if($get_toup_name->num_rows() > 0){
			    $row_sevname = $get_toup_name->row_array();
			    $topup_name = " (".$row_sevname['srvname'].")";
			}
		  }*/
		  $data[$i]['service_name'] = $this->planname($row->plan_id);
		  $data[$i]['payment_type'] = $row->bill_type;
		  $data[$i]['bill_number'] = ($row->bill_number != '')?$row->bill_number:"-";
		  $data[$i]['receipt_number'] = ($row->receipt_number != '')?$row->receipt_number:"-";
		  $data[$i]['transaction_id'] = ($row->transection_id!=0)?$row->transection_id:"-";
		  $data[$i]['payment_amount'] = $row->total_amount;
		  $data[$i]['payment_mode'] = $row->payment_mode;
		  $data[$i]['bill_status'] = ($row->receipt_received == '1')?'Paid':"Pending";
		  $data[$i]['bill_payed'] = ($row->receipt_received == '1')?'1':"0";
		  $i++;
	    }
	    
	    $custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE uid='".$useruid."' AND is_deleted='0'  AND YEAR(added_on) = '$year_filter'  ORDER BY id DESC");
	    if($custbillQ->num_rows() > 0){
		  foreach($custbillQ->result() as $row){
			$data[$i]['id'] = $row->id;
			$data[$i]['bill_added_on'] = $row->added_on;
			$data[$i]['generated_on'] = date("d.m.Y",strtotime($row->added_on));
			$data[$i]['paid_on'] = ($row->bill_paid_on != '0000-00-00 00:00:00')?date("d.m.Y",strtotime($row->bill_paid_on)):"-";
			$data[$i]['service_name'] = $row->bill_details;
			$data[$i]['payment_type'] = 'custom_invoice';
			$data[$i]['bill_number'] = ($row->bill_number != '')?$row->bill_number:"-";
			$data[$i]['receipt_number'] = ($row->receipt_number != '')?$row->receipt_number:"-";
			$data[$i]['transaction_id'] = "-";
			$data[$i]['payment_amount'] = $row->total_amount;
			$data[$i]['payment_mode'] = $row->payment_mode;
			$data[$i]['bill_status'] = ($row->receipt_received == '1')?'Paid':"Pending";
			$data[$i]['bill_payed'] = ($row->receipt_received == '1')?'1':"0";
			$i++;
		  }
	    }
	    usort($data, function($a, $b) {
		  return strtotime($b['bill_added_on']) - strtotime($a['bill_added_on']);
	    });
	    return $data;
      }
      
      public function countrydetails(){
	    $data = array();
	    $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".ISPID."'");
	    if($ispcountryQ->num_rows() > 0){
		  $crowdata = $ispcountryQ->row();
		  $countryid = $crowdata->country_id;
		  
		  $data['countryid'] = $countryid;
		  $countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
		  if($countryQ->num_rows() > 0){
			$rowdata = $countryQ->row();
			$currid = $rowdata->currency_id;
			
			$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
			if($currencyQ->num_rows() > 0){
			    $currobj = $currencyQ->row();
			    $currsymbol = $currobj->currency_symbol;
			    $data['currency'] = $currsymbol;
			}
			$data['demo_cost'] = $rowdata->demo_cost;
			$data['cost_per_user'] = $rowdata->cost_per_user;
			$data['cost_per_location'] = $rowdata->cost_per_location;
		  }
	    }
	    return $data;
      }
    
      public function planname($srvid){
	    $planname = '-';
	    $query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
	    if($query->num_rows() > 0){
		  $planname = $query->row()->srvname;
	    }
	    return $planname;
      }
    // isp uid apply
      public function pending_bill(){
            $data = array();
            $useruid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
            $uuid = $useruid;
            $wallet_amt = 0;
            $wallet_amt = 0;
	    $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
	    if($walletQ->num_rows() > 0){
		  $wallet_amt = $walletQ->row()->wallet_amt;
	    }
	    $passbook_amt = 0;
	    $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
	    if($passbookQ->num_rows() > 0){
		  $passbook_amt = $passbookQ->row()->plan_cost;
	    }
	    $balanceamt = ($wallet_amt - $passbook_amt);
            /*if($balanceamt < 0){
                  $data['pending_amount'] = abs($balanceamt);
            }*/
	    $payableamount = 0;
	    $totalbillamt = 0; $totalrcptamt = 0; $totalbalamt = 0;
	    $custbillQ = $this->db->query("SELECT COALESCE(SUM(total_amount),0) as total_amount FROM sht_subscriber_custom_billing WHERE uid='".$uuid."' AND is_deleted='0'");
	    if($custbillQ->num_rows() > 0){
		  $totalbillamt = $custbillQ->row()->total_amount;
	    }
	    $recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE uid='".$uuid."' ORDER BY id DESC");
	    if($recptQ->num_rows() > 0){
		  $totalrcptamt = $recptQ->row()->receipt_amount;
	    }
	    $totalbalamt = ($totalrcptamt - $totalbillamt);
	    
	    
	    if($totalbalamt < 0){
		  $payableamount += $totalbalamt;
	    }
	    if($balanceamt < 0){
		  $payableamount += $balanceamt;
	    }
            $data['pending_amount'] = abs($payableamount);
            return $data;
      }
      public function pending_bill_old(){
	  $data = array();
	  $useruid = '';
	  if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
	      $useruid = $this->session->userdata['isp_consumer_session']['user_uid'];    
	  }
	  $query = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$useruid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' order by id desc");
	  $i = 0;
	  
	  if($query->num_rows() > 0){
	      foreach($query->result() as $row){
		  $data[$i]['id'] = $row->id;
		  $topup_name = '';
		  if($row->bill_type == 'montly_pay'){
		      $bill_generated_on = $row->bill_added_on;
		      $month  = date('M', strtotime("-1 Month",strtotime($bill_generated_on)));
		      $data[$i]['bill_type'] = "Monthly Pay (".$month.")"; 
  
		  }else{
		      
		      if(ucwords($row->bill_type) == 'Topup'){
			  $plan_id = $row->plan_id;
			  $get_toup_name = $this->db->query("select srvname from sht_services where srvid = '$plan_id'");
			  if($get_toup_name->num_rows() > 0){
			      $row_sevname = $get_toup_name->row_array();
			      $topup_name = " (".$row_sevname['srvname'].")";
			  }
		      }
		     $data[$i]['bill_type'] = ucwords($row->bill_type).$topup_name; 
		  }
		  
		  $data[$i]['bill_amount'] = $row->total_amount;
		  $i++;
	      }
	      
	  }
	 //echo "<pre>";print_r($data);die;
	  return $data;
      }
      
      public function userbalance_amount($uuid){
          $wallet_amt = 0;
          $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
          if($walletQ->num_rows() > 0){
                  $wallet_amt = $walletQ->row()->wallet_amt;
          }
          
          $passbook_amt = 0;
          $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
          if($passbookQ->num_rows() > 0){
                  $passbook_amt = $passbookQ->row()->plan_cost;
          }
          $balanceamt = $wallet_amt - $passbook_amt;
          return abs($balanceamt);
      }
   
      public function userassoc_details($uuid){
            $data = array();
            $userassocQ = $this->db->query("SELECT tb1.user_credit_limit,tb1.paidtill_date, tb1.baseplanid,tb3.net_total FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$uuid."'");
            if($userassocQ->num_rows() > 0){
                  $rowdata = $userassocQ->row();
                  $data['user_credit_limit'] = $rowdata->user_credit_limit;
                  $data['plan_cost_perday'] = round(($rowdata->net_total/30) ,2);
                  $data['paidtill_date'] = $rowdata->paidtill_date;
            }
            return $data;
      }
   
   // isp uid apply
      public function citrus_success($addorder = FALSE){
	  $postdata=$this->input->post();
	   $isp_uid = $this->isp_uid;
	  $user_uid = '';
	  if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
	      $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
	  }
	
	 $payment_platform = 2;
	  if($addorder)
	  {
	     
	      
	      // apply topup(data topup) end
	      $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
		  "mobile"=>$postdata['mobileNo'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>$postdata['txnDateTime']);
	      $this->db->insert('sht_topup_payment_responce',$tabledata);
	      $id = $this->db->insert_id();
	      $transection_id = $postdata['transactionId'];
	      // update new date
	      $bill_paid_till_date = '';
	      $query_date = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$user_uid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' ");
	      if($query_date->num_rows() > 0){
		    foreach($query_date->result() as $query_date_row){
			  if($query_date_row->bill_type == 'montly_pay'){
				$bill_paid_till_date = date('Y-m-d', strtotime($query_date_row->bill_added_on));
			  }
			  
		    }
	      }
	      $total_amount = '';
	      if(isset($this->session->userdata['user_bill_payed_session']['total_current_due'])){
		  $total_amount = $this->session->userdata['user_bill_payed_session']['total_current_due'];    
	      }else{
		   $total_amount =  $postdata['amount'];
	      }
	      
	    $amount_payed = $this->userbalance_amount($user_uid);
	    $custom_amount = $total_amount - $custom_amount;
	    if($amount_payed > 0){
		  //insert into sht_subscriber_wallet
	      $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$amount_payed', now())");
	      $wlastid = $this->db->insert_id();
	      $wallet_billarr = array(
		    'isp_uid' => $isp_uid,
		    'subscriber_id' => '0',
		    'subscriber_uuid' => $user_uid,
		    'wallet_id' => $wlastid,
		    'bill_added_on' => date('Y-m-d H:i:s'),
		    'receipt_number' => $transection_id,
		    'bill_type' => 'addtowallet',
		    'payment_mode' => 'netbanking',
		    'actual_amount' => $amount_payed,
		    'total_amount' => $amount_payed,
		    'receipt_received' => '1',
		    'alert_user' => '0',
		    'bill_generate_by' => $isp_uid,
		    'bill_paid_on' => date('Y-m-d H:i:s'),
		    'wallet_amount_received' => '1',
		    "payment_platform" => $payment_platform,
	      );
	      $this->db->insert('sht_subscriber_billing', $wallet_billarr);
	      $wlast_billid = $this->db->insert_id();
  
	      $receiptArr = array();
	      $receiptArr['cheque_dd_paytm_number'] = $transection_id;
	      $receiptArr['isp_uid'] = $isp_uid;
	      $receiptArr['uid'] = $user_uid;
	      $receiptArr['bill_id'] = $wlast_billid;
	      $receiptArr['receipt_number'] = $transection_id;
	      $receiptArr['receipt_amount'] = $amount_payed;
	      $receiptArr['added_on'] = date('Y-m-d H:i:s');
	      $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
	      if($checkrcpthistoryQ->num_rows() > 0){
		    $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
	      }else{
		    $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
	      }
	      // update bill
	     
	      $this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now(),payment_platform = '$payment_platform' where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
	      
	    }
	    if($custom_amount > 0){
			$this->addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id);
		  }
	      
	      //TO UPDATE USER EXPIRATION DATE
	      
	      $userassoc_details = $this->userassoc_details($user_uid);
	      $user_credit_limit = $userassoc_details['user_credit_limit'];
	      $user_wallet_balance = $this->userbalance_amount($user_uid);
	      $plan_cost_perday = $userassoc_details['plan_cost_perday'];
	      $paidtill_date = $userassoc_details['paidtill_date'];
	      if($bill_paid_till_date != ''){
		   $paidtill_date = $bill_paid_till_date;
	      }
	      $expiration_acctdays = ceil(($user_credit_limit + $user_wallet_balance) / $plan_cost_perday);
	      $expiration_date = date('Y-m-d', strtotime($paidtill_date . " +".$expiration_acctdays." days"));
	      $expiration_date = $expiration_date.' 00:00:00';
	      $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $user_uid));
	      if($bill_paid_till_date != ''){
		    $this->db->update('sht_users', array('paidtill_date' => $paidtill_date), array('uid' => $user_uid));
	      }
	      
	      redirect(base_url()."bils/addFund_success_view/".$id);
	  }
	  else {
	      $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
		  "mobile"=>$postdata['mobileNo'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));
  
	      $this->db->insert('sht_topup_payment_responce',$tabledata);
	      $id = $this->db->insert_id();
	      redirect(base_url()."bils/addFund_fail_view/".$id);
	  }
  
  
      }
      
      
    // isp uid apply
      public function transection_detail($id){
        $data = array();
        $query = $this->db->query("select trns_resp from sht_topup_payment_responce where id = '$id'");
        $email = '';
        $authIdCode = 0;
        $txnDateTime = 0;
        $transactionId = 0;
        $issuerRefNo = 0;
        $TxRefNo = 0;
        $paytm_status = '';
      $paytm_tid = '';
      $RESPMSG = '';
      $ebs_tid = '';
      $ebs_msg = '';
      $ebs_status = '';
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = (isset($def->email))?$def->email:"";
            $authIdCode = (isset($def->authIdCode))?$def->authIdCode: "";
            $txnDateTime = (isset($def->txnDateTime))?$def->txnDateTime:"";
            $transactionId = (isset($def->transactionId))?$def->transactionId:"";
            $issuerRefNo = (isset($def->issuerRefNo))?$def->issuerRefNo:"";
            $TxRefNo = (isset($def->TxRefNo))?$def->TxRefNo:"";
            $paytm_status = (isset($def->STATUS))?$def->STATUS:"";
            $paytm_tid = (isset($def->TXNID))?$def->TXNID:"";
            $RESPMSG = (isset($def->RESPMSG))?$def->RESPMSG:"";
            $ebs_tid = (isset($def->TransactionID))?$def->TransactionID:"";
            $ebs_msg = (isset($def->ResponseMessage))?$def->ResponseMessage:"";
            if(isset($def->ResponseCode) && $def->ResponseCode == '0'){
                $ebs_status = "Success";
            }else{
                $ebs_status = 'Fail';
            }
        }
        $data['email'] = $email;
        $data['authIdCode'] = $authIdCode;
        $data['txnDateTime'] = $txnDateTime;
        $data['transactionId'] = $transactionId;
        $data['issuerRefNo'] = $issuerRefNo;
        $data['TxRefNo'] = $TxRefNo;
        $data['paytm_status'] = $paytm_status;
        $data['paytm_tid'] = $paytm_tid;
        $data['RESPMSG'] = $RESPMSG;
        $data['ebs_tid'] = $ebs_tid;
        $data['ebs_msg'] = $ebs_msg;
        $data['ebs_status'] = $ebs_status;
        return $data;
    }
    


       public function payumoney_credential(){
            $isp_uid = $this->isp_uid;
            $merchent_key = '';
            $merchent_salt = '';
            $query = $this->db->query("select payu_merchantkey, payu_merchantsalt from sht_merchant_account where isp_uid = '$isp_uid'");
            if($query->num_rows() > 0){
                  $row = $query->row_array();
                  if($row['payu_merchantkey'] != '' && $row['payu_merchantsalt'] != ''){
                        $merchent_key = $row['payu_merchantkey'];
                        $merchent_salt = $row['payu_merchantsalt'];
                  }
            }
            $data['mer_key'] = $merchent_key;
            $data['salt'] = $merchent_salt;
            return $data;
      }
      public function payumoney_cancle(){
	    $payment_platform = 2;
            $postdata=$this->input->post();
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
           
           $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstname'],"email"=>$postdata['email'],
                "mobile"=>$postdata['phone'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            redirect(base_url()."bils/payumoney_cancel_view/".$id);
      }
      public function payumoney_success(){
            $postdata=$this->input->post();
            $isp_uid = $this->isp_uid;
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
	    $payment_platform = 2;
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstname'],"email"=>$postdata['email'],"mobile"=>$postdata['phone'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            $transection_id = $postdata['txnid'];
            
            // update new date
            $bill_paid_till_date = '';
            $query_date = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$user_uid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' ");
            if($query_date->num_rows() > 0){
                  foreach($query_date->result() as $query_date_row){
                        if($query_date_row->bill_type == 'montly_pay'){
                              $bill_paid_till_date = date('Y-m-d', strtotime($query_date_row->bill_added_on));
                        }
                        
                  }
            }
            $total_amount = '';
            if(isset($this->session->userdata['user_bill_payed_session']['total_current_due'])){
                $total_amount = $this->session->userdata['user_bill_payed_session']['total_current_due'];    
            }else{
                 $total_amount =  $postdata['amount'];
            }
	    $amount_payed = $this->userbalance_amount($user_uid);
		  $custom_amount = $total_amount - $custom_amount;
		  
		
            if($amount_payed > 0){
		  //insert into sht_subscriber_wallet
		  $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$amount_payed', now())");
		  $wlastid = $this->db->insert_id();
		  $wallet_billarr = array(
			'isp_uid' => $isp_uid,
			'subscriber_id' => '0',
			'subscriber_uuid' => $user_uid,
			'wallet_id' => $wlastid,
			'bill_added_on' => date('Y-m-d H:i:s'),
			'receipt_number' => $transection_id,
			'bill_type' => 'addtowallet',
			'payment_mode' => 'netbanking',
			'actual_amount' => $amount_payed,
			'total_amount' => $amount_payed,
			'receipt_received' => '1',
			'alert_user' => '0',
			'bill_generate_by' => $isp_uid,
			'bill_paid_on' => date('Y-m-d H:i:s'),
			'wallet_amount_received' => '1',
			"payment_platform" => $payment_platform,
		  );
		  $this->db->insert('sht_subscriber_billing', $wallet_billarr);
		  $wlast_billid = $this->db->insert_id();
      
		  $receiptArr = array();
		  $receiptArr['cheque_dd_paytm_number'] = $transection_id;
		  $receiptArr['isp_uid'] = $isp_uid;
		  $receiptArr['uid'] = $user_uid;
		  $receiptArr['bill_id'] = $wlast_billid;
		  $receiptArr['receipt_number'] = $transection_id;
		  $receiptArr['receipt_amount'] = $amount_payed;
		  $receiptArr['added_on'] = date('Y-m-d H:i:s');
		  $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
		  if($checkrcpthistoryQ->num_rows() > 0){
			$this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
		  }else{
			$this->db->insert('sht_subscriber_receipt_history', $receiptArr);
		  }
		  
		  // update bill
		  $this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now(),payment_platform = '$payment_platform' where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
		  
	    }
	    
	    if($custom_amount > 0){
			$this->addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id);
		  }
            
            
            //TO UPDATE USER EXPIRATION DATE
            
            $userassoc_details = $this->userassoc_details($user_uid);
            $user_credit_limit = $userassoc_details['user_credit_limit'];
	    $user_wallet_balance = $this->userbalance_amount($user_uid);
	    $plan_cost_perday = $userassoc_details['plan_cost_perday'];
            $paidtill_date = $userassoc_details['paidtill_date'];
            if($bill_paid_till_date != ''){
                 $paidtill_date = $bill_paid_till_date;
            }
            $expiration_acctdays = ceil(($user_credit_limit + $user_wallet_balance) / $plan_cost_perday);
            $expiration_date = date('Y-m-d', strtotime($paidtill_date . " +".$expiration_acctdays." days"));
            $expiration_date = $expiration_date.' 00:00:00';
            $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $user_uid));
            if($bill_paid_till_date != ''){
                  $this->db->update('sht_users', array('paidtill_date' => $paidtill_date), array('uid' => $user_uid));
            }
            redirect(base_url()."bils/payumoney_success_view/".$id);
      }
      public function payumoney_transection_detail($id){
            $data = array();
        $query = $this->db->query("select trns_resp from sht_topup_payment_responce where id = '$id'");
        $email = '';
        $transactionId = 0;
       
        $transaction_error = '';
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = $def->email;
            $transactionId = $def->txnid;
            $transaction_error = $def->error_Message;
        }
        $data['email'] = $email;
        $data['transactionId'] = $transactionId;
        $data['error_Message'] = $transaction_error;
        return $data;
      }
     
      public function paytm_success($paytm_merchant_key, $paytm_merchant_mid, $paytm_merchant_web, $paytm_environment){
            $postdata=$this->input->post();
            $isp_uid = $this->isp_uid;
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
	    $payment_platform = 2;
            if($postdata['STATUS'] == "TXN_SUCCESS"){
                  $responseParamList = '';
                  if (isset($_POST["ORDERID"]) && $_POST["ORDERID"] != "") {
                        // In Test Page, we are taking parameters from POST request. In actual implementation these can be collected from session or DB. 
                        $ORDER_ID = $_POST["ORDERID"];
                        // Create an array having all required parameters for status query.
                        $requestParamList = array("MID" => $paytm_merchant_mid , "ORDERID" => $ORDER_ID);  
                        $PAYTM_DOMAIN = "pguat.paytm.com";
                        if ($paytm_environment == 'PROD') {
                            $PAYTM_DOMAIN = 'securegw.paytm.in';
			    //$PAYTM_DOMAIN = 'secure.paytm.in';
                        }
                        //define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
			define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/merchant-status/getTxnStatus');
                        $StatusCheckSum = getChecksumFromArray($requestParamList,$paytm_merchant_key);
        
                        $requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
                        // Call the PG's getTxnStatusNew() function for verifying the transaction status.
                        $responseParamList = getTxnStatusNew($requestParamList);
                  }
                    $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['TXNAMOUNT'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"1","trns_resp"=>json_encode($postdata),"tes_res"=>json_encode($responseParamList),"created_on"=>(isset($postdata['TXNDATE']))?$postdata['TXNDATE']:date("Y-m-d H:i:s"));
                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  $transection_id = $postdata['TXNID'];
                  // update new date
                  $bill_paid_till_date = '';
                  $query_date = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$user_uid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' ");
                  if($query_date->num_rows() > 0){
                        foreach($query_date->result() as $query_date_row){
                              if($query_date_row->bill_type == 'montly_pay'){
                                    $bill_paid_till_date = date('Y-m-d', strtotime($query_date_row->bill_added_on));
                              }
                              
                        }
                  }
                  $total_amount = '0';
                  if(isset($this->session->userdata['user_bill_payed_session']['total_current_due'])){
                      $total_amount = $this->session->userdata['user_bill_payed_session']['total_current_due'];    
                  }else{
                       $total_amount =  $postdata['TXNAMOUNT'];
                  }
		  $amount_payed = $this->userbalance_amount($user_uid);
		  $custom_amount = $total_amount - $custom_amount;
		  if($amount_payed > 0){
			 $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$amount_payed', now())");
			$wlastid = $this->db->insert_id();
			$wallet_billarr = array(
			      'isp_uid' => $isp_uid,
			      'subscriber_id' => '0',
			      'subscriber_uuid' => $user_uid,
			      'wallet_id' => $wlastid,
			      'bill_added_on' => date('Y-m-d H:i:s'),
			      'receipt_number' => $transection_id,
			      'bill_type' => 'addtowallet',
			      'payment_mode' => 'netbanking',
			      'actual_amount' => $amount_payed,
			      'total_amount' => $amount_payed,
			      'receipt_received' => '1',
			      'alert_user' => '0',
			      'bill_generate_by' => $isp_uid,
			      'bill_paid_on' => date('Y-m-d H:i:s'),
			      'wallet_amount_received' => '1',
			      "payment_platform" => $payment_platform,
			);
			$this->db->insert('sht_subscriber_billing', $wallet_billarr);
			$wlast_billid = $this->db->insert_id();
	    
			$receiptArr = array();
			$receiptArr['cheque_dd_paytm_number'] = $transection_id;
			$receiptArr['isp_uid'] = $isp_uid;
			$receiptArr['uid'] = $user_uid;
			$receiptArr['bill_id'] = $wlast_billid;
			$receiptArr['receipt_number'] = $transection_id;
			$receiptArr['receipt_amount'] = $amount_payed;
			$receiptArr['added_on'] = date('Y-m-d H:i:s');
			$checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
			if($checkrcpthistoryQ->num_rows() > 0){
			      $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
			}else{
			      $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
			}
			     // update bill
			    
			     $this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now(),payment_platform = '$payment_platform' where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
			
            
		  }
		  if($custom_amount > 0){
			$this->addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id);
		  }
                 //insert into sht_subscriber_wallet
           
                  //TO UPDATE USER EXPIRATION DATE
                 
                 $userassoc_details = $this->userassoc_details($user_uid);
                 $user_credit_limit = $userassoc_details['user_credit_limit'];
                 $user_wallet_balance = $this->userbalance_amount($user_uid);
                 $plan_cost_perday = $userassoc_details['plan_cost_perday'];
                 $paidtill_date = $userassoc_details['paidtill_date'];
                 if($bill_paid_till_date != ''){
                      $paidtill_date = $bill_paid_till_date;
                 }
                 $expiration_acctdays = ceil(($user_credit_limit + $user_wallet_balance) / $plan_cost_perday);
                 $expiration_date = date('Y-m-d', strtotime($paidtill_date . " +".$expiration_acctdays." days"));
                 $expiration_date = $expiration_date.' 00:00:00';
                 $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $user_uid));
                 if($bill_paid_till_date != ''){
                       $this->db->update('sht_users', array('paidtill_date' => $paidtill_date), array('uid' => $user_uid));
                 }
                 redirect(base_url()."bils/paytm_success_view/".$id);
            }else{
                  $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['TXNAMOUNT'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['TXNDATE']))?$postdata['TXNDATE']:date("Y-m-d H:i:s"));

                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  redirect(base_url()."bils/paytm_success_view/".$id);
            }
      }
      public function ebs_success($response){
            $postdata=$response;
            $isp_uid = $this->isp_uid;
            $user_uid = '';
            if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
                $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
            }
	    $payment_platform = 2;
            if($postdata['ResponseCode'] == "0"){
                    $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['Amount'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['DateCreated']))?$postdata['DateCreated']:date("Y-m-d H:i:s"));
                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  $transection_id = $postdata['TransactionID'];
                  // update new date
                  $bill_paid_till_date = '';
                  $query_date = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$user_uid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' ");
                  if($query_date->num_rows() > 0){
                        foreach($query_date->result() as $query_date_row){
                              if($query_date_row->bill_type == 'montly_pay'){
                                    $bill_paid_till_date = date('Y-m-d', strtotime($query_date_row->bill_added_on));
                              }
                              
                        }
                  }
                  $total_amount = '';
                  if(isset($this->session->userdata['user_bill_payed_session']['total_current_due'])){
                      $total_amount = $this->session->userdata['user_bill_payed_session']['total_current_due'];    
                  }else{
                       $total_amount =  $postdata['Amount'];
                  }
            $amount_payed = $this->userbalance_amount($user_uid);
		  $custom_amount = $total_amount - $custom_amount;
	    if($amount_payed > 0){
		       //insert into sht_subscriber_wallet
		  $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$amount_payed', now())");
		  $wlastid = $this->db->insert_id();
		  $wallet_billarr = array(
			'isp_uid' => $isp_uid,
			'subscriber_id' => '0',
			'subscriber_uuid' => $user_uid,
			'wallet_id' => $wlastid,
			'bill_added_on' => date('Y-m-d H:i:s'),
			'receipt_number' => $transection_id,
			'bill_type' => 'addtowallet',
			'payment_mode' => 'netbanking',
			'actual_amount' => $amount_payed,
			'total_amount' => $amount_payed,
			'receipt_received' => '1',
			'alert_user' => '0',
			'bill_generate_by' => $isp_uid,
			'bill_paid_on' => date('Y-m-d H:i:s'),
			'wallet_amount_received' => '1',
			"payment_platform" => $payment_platform,
		  );
		  $this->db->insert('sht_subscriber_billing', $wallet_billarr);
		  $wlast_billid = $this->db->insert_id();
      
		  $receiptArr = array();
		  $receiptArr['cheque_dd_paytm_number'] = $transection_id;
		  $receiptArr['isp_uid'] = $isp_uid;
		  $receiptArr['uid'] = $user_uid;
		  $receiptArr['bill_id'] = $wlast_billid;
		  $receiptArr['receipt_number'] = $transection_id;
		  $receiptArr['receipt_amount'] = $amount_payed;
		  $receiptArr['added_on'] = date('Y-m-d H:i:s');
		  $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
		  if($checkrcpthistoryQ->num_rows() > 0){
			$this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
		  }else{
			$this->db->insert('sht_subscriber_receipt_history', $receiptArr);
		  }
		       // update bill
		      
		       $this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now(),payment_platform = '$payment_platform' where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
		  
	    }
	    
	    if($custom_amount > 0){
			$this->addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id);
		  }
            
            
                  //TO UPDATE USER EXPIRATION DATE
                 
                 $userassoc_details = $this->userassoc_details($user_uid);
                 $user_credit_limit = $userassoc_details['user_credit_limit'];
                 $user_wallet_balance = $this->userbalance_amount($user_uid);
                 $plan_cost_perday = $userassoc_details['plan_cost_perday'];
                 $paidtill_date = $userassoc_details['paidtill_date'];
                 if($bill_paid_till_date != ''){
                      $paidtill_date = $bill_paid_till_date;
                 }
                 $expiration_acctdays = ceil(($user_credit_limit + $user_wallet_balance) / $plan_cost_perday);
                 $expiration_date = date('Y-m-d', strtotime($paidtill_date . " +".$expiration_acctdays." days"));
                 $expiration_date = $expiration_date.' 00:00:00';
                 $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $user_uid));
                 if($bill_paid_till_date != ''){
                       $this->db->update('sht_users', array('paidtill_date' => $paidtill_date), array('uid' => $user_uid));
                 }
                 redirect(base_url()."bils/ebs_success_view/".$id);
            }else{
                  $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$postdata['Amount'],"name"=>"","email"=>"",
                "mobile"=>"","payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['DateCreated']))?$postdata['DateCreated']:date("Y-m-d H:i:s"));

                  $this->db->insert('sht_topup_payment_responce',$tabledata);
                  $id = $this->db->insert_id();
                  redirect(base_url()."bils/ebs_success_view/".$id);
            }
      }

      public function check_payment_gateway(){
            $isp_uid = $this->isp_uid;
            $query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
            $i = 0;
            $gatways = array();
            if($query->num_rows() > 0){
                  $data['resultCode'] = '1';
                  $data['resultMsg'] = 'Success';
                  $row = $query->row_array();
                  // for citrus
                  if($row['citrus_secret_key'] != '' || $row['citrus_post_url'] != '' || $row['citrus_vanity_url'] != ''){
                      $gatways[$i]['gatway_name'] = 'citrus';
                      $gatways[$i]['citrus_post_url'] = $row['citrus_post_url'];
                      $gatways[$i]['citrus_secret_key'] = $row['citrus_secret_key'];
                      $gatways[$i]['citrus_vanity_url'] = $row['citrus_vanity_url'];
                      $i++;
                  }
                  // for paytm
                  if($row['paytm_merchant_key'] != '' || $row['paytm_merchant_mid'] != '' || $row['paytm_merchant_web'] != '' || $row['paytm_environment'] != ''){
                      $gatways[$i]['gatway_name'] = 'paytm';
                      $gatways[$i]['paytm_merchant_key'] = $row['paytm_merchant_key'];
                      $gatways[$i]['paytm_merchant_mid'] = $row['paytm_merchant_mid'];
                      $gatways[$i]['paytm_merchant_web'] = $row['paytm_merchant_web'];
                      $gatways[$i]['paytm_environment'] = $row['paytm_environment'];
                      $i++;
                  }
                  // for payu 
                  if($row['payu_merchantid'] != '' || $row['payu_merchantkey'] != '' || $row['payu_merchantsalt'] != '' || $row['payu_authheader'] != ''){
                      $gatways[$i]['gatway_name'] = 'payu';
                      $gatways[$i]['payu_merchantid'] = $row['payu_merchantid'];
                      $gatways[$i]['payu_merchantkey'] = $row['payu_merchantkey'];
                      $gatways[$i]['payu_merchantsalt'] = $row['payu_merchantsalt'];
                      $gatways[$i]['payu_authheader'] = $row['payu_authheader'];
                      $i++;
                  }
                  // for EBS 
                  if($row['ebs_accountid'] != '' || $row['ebs_secretkey'] != '' || $row['ebs_salt'] != ''){
                      $gatways[$i]['gatway_name'] = 'EBS';
                      $gatways[$i]['ebs_accountid'] = $row['ebs_accountid'];
                      $gatways[$i]['ebs_secretkey'] = $row['ebs_secretkey'];
                      $gatways[$i]['ebs_account_name'] = $row['ebs_salt'];
                      $i++;
                  }
                  $data['gatways'] = $gatways;
            }else{
                  $data['resultCode'] = '0';
                  $data['resultMsg'] = 'No record found';
            }
            return $data;
      }
      
      public function billcopy_actions(){
		$billid = $this->input->post('billid');
		$billtype = $this->input->post('billtype');
		$uid = '';
		  if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
		      $uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
		  }
		$action = $this->input->post('action');
		
		if($billtype == 'installation'){
			$this->installation_billing($uid, $billid, $action);
		}elseif($billtype == 'security'){
			$this->emailer_model->security_billing($uid, $billid, $action);
		}elseif($billtype == 'montly_pay'){
			$this->emailer_model->monthly_billing($uid, $billid, $action);
		}elseif($billtype == 'midchange_plancost'){
			$this->emailer_model->monthly_billing($uid, $billid, $action);
		}elseif($billtype == 'topup'){
			$this->emailer_model->topups_billing($uid, $billid, $action);
		}elseif($billtype == 'addtowallet'){
			$this->emailer_model->addtowallet_billing($uid, $billid, $action);
		}elseif($billtype == 'custom_invoice'){
			$this->emailer_model->custominvoice_billing($uid, $billid, $action);
		}elseif($billtype == 'advprepay'){
			$this->emailer_model->advmonthly_billing($uid, $billid, $action);
		}else{
			echo 'No Bill Available.';
		}
		
      }
      
      
      public function addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id){
	// get unpaid custom bills
	$unpaid = $this->db->query("select * from sht_subscriber_custom_billing where is_deleted = '0' and receipt_received != '1' and uid = '$user_uid'");
	if($unpaid->num_rows() > 0){
	    foreach($unpaid->result() as $row){
		$billid = $row->id;
		$billno = $row->bill_number;
		$uuid = $user_uid;
		$rcpt_addedon = date('Y-m-d H:i:s');
		$rcpt_number = $transection_id;
		$receiptArr = array();
		$update_payment_mode = "netbanking";
		$receiptArr['cheque_dd_paytm_number'] = $transection_id;
		
		$receiptArr['payment_mode'] = $update_payment_mode;
		$receiptArr['bill_id'] = $billid;
		$receiptArr['bill_number'] = $billno;
		$receiptArr['receipt_number'] = $rcpt_number;
		$receiptArr['added_on'] = $rcpt_addedon;
		$receiptArr['receipt_amount'] = $row->total_amount;
		$receiptArr['uid'] = $uuid;
		
		$this->db->insert('sht_custom_receipt_list', $receiptArr);
		// paid custom bills
		
		$update = $this->db->query("update sht_subscriber_custom_billing set receipt_received = '1', bill_paid_on = now() where id = '$billid'");
	    }
	}
    }


	
	
	
	
	
}


?>
