<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
      <?php
      if(isset($this->session->userdata['isp_consumer_session']['isp_name'])){
	 echo $this->session->userdata['isp_consumer_session']['isp_name'];
      }
      ?>
   </title>
   <!-- Font Awesome -->
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
   <!-- Bootstrap core CSS -->
   <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
   <!-- Material Design Bootstrap -->
   <link href="<?php echo base_url()?>assets/css/material-design.min.css" rel="stylesheet">
   <!-- Ripples Design Bootstrap -->
   <link href="<?php echo base_url()?>assets/css/ripples.min.css" rel="stylesheet">
   <!-- Your custom styles (optional) -->
   <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/slider_styles.css" rel="stylesheet">
</head>
<body>
   <div id="loading" style="display: none"><img src="<?php echo base_url()?>assets/images/loader.svg"/></div>
<!-- Start your project here-->
<div class="home_wapper">
   
   <div class="container">
      <div class="sub_wapper">
         <header>
            <div class="navbar navbar-default navbar-fixed-top">
               <div class="container">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav_top">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                     </div>
                     <div class="navbar-collapse">
                        <ul class="nav navbar-nav">
                           <li>
                              <a href="#"  style="padding-left:0px; padding-right:0px">
                                  <?php
				    if(isset($this->session->userdata['isp_consumer_session']['isp_logo']) && $this->session->userdata['isp_consumer_session']['isp_logo'] != ''){
				       $logo_path = $this->session->userdata['isp_consumer_session']['isp_logo'];
				       echo '<img src="'.$logo_path.'" class="img-responsive" width="70%"/>';
                                       
				    }
				    else{
				       ?>
				       <img src="<?php echo base_url()?>assets/images/isp_logo.png" class="img-responsive" width="70%"/>
				       <?php
				    }
				    ?>
                                 
                              </a>
                           </li>
                           <!--<li><a href="#">Link</a></li>-->
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <!--li>
                              <a href="#">
                                 <div class="provider_logo">
                                    <img src="<?php echo base_url()?>assets/images/shouut_logo.svg"/>
                                 </div>
                              </a>
                           </li-->
                            <?php
                            $style = 'style="padding-right: 20px; padding-top:20px"';
                            $button = '';
                              if($this->session->userdata['isp_consumer_permission_session']['customer_model_permission'] == '1'){
                                 $style = '';
                                 $button = ' <a href="'.base_url().'usage" class="nav_btn_login">MY ACCOUNT</a>';
                              }
                           ?>
                           <li <?php echo $style?>>
                              <!--/a href="<?php echo base_url()?>usage" class="nav_btn_login">MY ACCOUNT</a-->
                             <?php echo $button;?>
                              <center><span class="top_header_span">UID : <?php echo $this->session->userdata['isp_consumer_session']['user_uid']  ?></span></center>
                              </li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <section id="wrapper_right">
            <div class="container-fluid">
                <?php
               if($this->session->userdata['isp_consumer_permission_session']['website_module_permission'] == '1'){
               ?>
               <div class="row">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <div class="carousel-inner">
                        <?php
                         $i = 0;
                        foreach($slider_offer as $slider_offer1){
                           $active_class = '';
                           if($i == 0){
                              $active_class = 'active';
                           }
                          ?>
                        <div class="item <?php echo $active_class?>">
                           <img src="<?php echo $slider_offer1['image_path']?>"  class="img-responsive"/>
                           <!--div class="carousel-caption">
                              <h3><?php echo $slider_offer1['plan_name']?></h3>
                              <p><?php echo $slider_offer1['banner_desc']?></p>
                             
                           </div-->
                        </div>
                        <?php 
                           
                           $i++;
                        }
                        ?>
                     </div>
                     <ul class="nav nav-pills nav-justified deepak_nav">
                        <?php
                         $i = 0;
                        foreach($slider_offer as $slider_offer1){
                          $active_class = '';
                           if($i == 0){
                              $active_class = 'active';
                           }
                           echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="'.$active_class.'"><a href="#" style="border-left:none;">'.$slider_offer1['plan_name'].'<center><small >'.$slider_offer1['plan_desc'].'</small></center></a></li>';
                           
                           $i++;
                        }
                        ?>
                        
                       </ul>
                  </div>

               </div>
                <?php } ?>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h2 class="heading"><i class="fa fa-caret-right" aria-hidden="true"></i>
                     <i class="fa fa-caret-right" aria-hidden="true"></i>
                     Don't just browse. Now get a <span>LOT MORE</span> from your broadband!!
                  </h2>
               </div>
              
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="row">
                     <ul class="nav nav-tabs" role="tablist">
                         <?php
                           if($this->session->userdata['isp_consumer_permission_session']['website_module_permission'] == '1'){
                        ?>
                        <li role="presentation" class="active">
                           <a href="#about" aria-controls="about" role="tab" data-toggle="tab" onclick="about_us_data()">ABOUT US</a>
                        </li>
                        <li role="presentation">
                           <a href="#services" aria-controls="services" role="tab" data-toggle="tab" onclick="services_data()">SERVICES</a>
                        </li>
                        <li role="presentation">
                           <a href="#plans" aria-controls="plans" role="tab" data-toggle="tab" onclick="plan_topup_data()">PLANS & TOP UPS</a>
                        </li>
                        <li role="presentation" >
                           <a href="#promotions" aria-controls="promotions" role="tab" data-toggle="tab" style="font-weight: 500" onclick="promotion_data()">PROMOTIONS</a>
                        </li>
                         <?php
                           if($this->session->userdata['isp_consumer_permission_session']['engage_module_permission'] == '1'){
                           ?>
                        <li role="presentation" >
                           <a href="#engage" aria-controls="engage" role="tab" data-toggle="tab" style="font-weight: 500">ENGAGE</a>
                        </li>
                         <?php }
                       if($this->session->userdata['isp_consumer_permission_session']['shop_module_permission'] == '1'){
                       ?>
                        <li role="presentation">
                           <a href="#shop" aria-controls="shop" role="tab" data-toggle="tab" style="font-weight: 500" onclick="shop_data()">SHOP</a>
                        </li>
                        
                        <?php } ?>
                        
                        <li role="presentation">
                           <a href="#contact" aria-controls="contact" role="tab" data-toggle="tab" onclick="contact_us_data()">CONTACT US</a>
                        </li>
                        <?php }?>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                     <div class="tab-content">
                        
                        <!-- about us tab start -->
                        <div role="tabpanel" class="tab-pane active" id="about" >
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="about_us_data">
                                 <?php
                                if($this->session->userdata['isp_consumer_permission_session']['website_module_permission'] == '1'){
                                  echo $about_us;
                                }
                                
                                ?>     
                           </div>
                        </div>
                        <!-- about us tab end -->
                        
                        
                        <!--services tag start -->
                        <div role="tabpanel" class="tab-pane" id="services">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="services_data">
                                    
                           </div>
                        </div>
                        <!-- services tag end -->
                        
                          <!-- plan and topup tab start -->
                        
                        <div role="tabpanel" class="tab-pane tab_plans active" id="plans">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
                              <div class="row">
                                 <p class="para">Check out our Internet Packs and Top UP Plans for home users</p>
                              </div>
                           </div>
                           
                           <div id="plan_topup_data">
                              
                           </div>
                        </div>
                        <!-- plan and topup tab end -->
                        
                         <!-- promotion tab start -->
                        <div role="tabpanel" class="tab-pane " id="promotions">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:25px">
                              <div class="row">
                                 <p class="para">Exclusive deals and offers for  users. Grab them before they expire!</p>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row" id= "promotion_data">
                                
                              </div>
                           </div>
                        </div>
                           <!-- promotion tab End -->
                        
                        
                        <!-- Engaje tab Start -->
                        <div role="tabpanel" class="tab-pane " id="engage">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <p class="para">Engage <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    Earn <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    Redeem against your bill. Credits you earn here by viewing these messages from our<br/>
                                    sponsors can be redeemed to against your monthly broadband.</p>
                                 <center><a href="#" class="btn btn-raised btn_yellow btn-lg">YOUR BALANCE : 500 POINTS</a>  <a href="#" style="margin-left:15px ">Redeem Now</a></center>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <h4>WATCH & EARN</h4>
                                 <div class="well">
                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-img-icon"><img src="<?php echo base_url()?>assets/images/watch_play.svg"/></div>
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam. </p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-img-icon"><img src="<?php echo base_url()?>assets/images/watch_play.svg"/></div>
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                    <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-img-icon"><img src="<?php echo base_url()?>assets/images/watch_play.svg"/></div>
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                     <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-img-icon"><img src="<?php echo base_url()?>assets/images/watch_play.svg"/></div>
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                    <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-img-icon"><img src="<?php echo base_url()?>assets/images/watch_play.svg"/></div>
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <h4>POLLS</h4>
                                 <div class="well">
                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam. </p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <h4>OFFERS</h4>
                                 <div class="well">
                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam. </p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <h4>BRAND CAPTCHA</h4>
                                 <div class="well">
                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam. </p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_2.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>

                                    <div class="card" style="width: 20.6rem;">
                                       <img class="card-img-top img-responsive" src="<?php echo base_url()?>assets/images/add_3.jpg" alt="Card image cap">
                                       <div class="card-block">
                                          <div class="card-img">
                                             <img src="<?php echo base_url()?>assets/images/cocke_logo.jpg"
                                                  class="img-responsive img-circle"/>
                                          </div>
                                          <h4 class="card-title">Coke Studio  </h4>
                                          <p class="card-text">Paiyada - Ram Sampath,
                                             Padma Shri Aruna Sairam.</p>
                                          <h5>Watch & Earn 500 Points</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Engaje tab End -->
                        
                       
                        <!-- shop tab start -->   
                        <div role="tabpanel" class="tab-pane" id="shop">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:25px">
                              <div class="row">
                                 <p class="para">Get smart with all Shouut. Exclusive deals and offers for Shouut users.</p>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row" id="shop_data">
                                 
                              </div>
                           </div>
                        </div>
                        <!-- shop tab Endt -->
                        
                      
                        
                        
                        
                        <!-- contact page start -->
                        <div role="tabpanel" class="tab-pane contact" id="contact">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row" id="contact_us_data">
                                 
                                    
                                 
                                 
                              </div>
                           </div>
                        </div>
                        <!-- contact page end -->
                        
                        
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!-- /Start your project here-->



<!---/ Buy plan or topup modal open --->
<div class="modal  fade" id="plan_topup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-container" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <center>
                <?php
				    if(isset($this->session->userdata['isp_consumer_session']['isp_logo']) && $this->session->userdata['isp_consumer_session']['isp_logo'] != ''){
				       $logo_path = $this->session->userdata['isp_consumer_session']['isp_logo'];
				       echo '<img src="'.$logo_path.'" class="img-responsive" width="50%"/>';
                                       
				    }
				    else{
				       ?>
				       <img src="<?php echo base_url()?>assets/images/isp_logo.png" class="img-responsive" width="50%"/>
				       <?php
				    }
				    ?>
            </center>
            <div class="modal-title">
               <h4>GET IN TOUCH</h4>
            </div>
         </div>
         <div class="modal-body" style="margin-top:5px">
         
            
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="hidden" id="request_srvid" name = "request_srvid">
                     <input type="text" class="form-control form-modal"  placeholder="Name" onblur="this
                     .placeholder='Name'" onfocus="this.placeholder=''" id="request_name" name="request_name" >
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="email" class="form-control form-modal"  placeholder="Email" onblur="this
                     .placeholder='Email'" onfocus="this.placeholder=''" id="request_email" name="request_email">
                  </div>
               </div>
                <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="text" class="form-control form-modal"  placeholder="Mobile No." onblur="this
                     .placeholder='Mobile No.'" onfocus="this.placeholder=''" id="request_mobile_number" name="request_mobile_number">
                  </div>
               </div>
                <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <textarea class="form-control" rows="3" placeholder="Message" id="request_msg" name="request_msg"></textarea>
                        
                  </div>
               </div>
               <div class="form-group">
                  <p ><center style="color:red" id="request_error"> </center></p>
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="button" name="submitlogin" value="SUBMIT" class="btn btn-raised btn-danger btn-lg btn-block" onclick="request_plan_topup()">
                  </div>
               </div>
           
         </div>
      </div>
   </div>
</div>

<!---/ Buy plan or topup modal End --->


 <!-- Buy plan or topup modal success open -->
       <div class="modal fade" id="request_success" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title" style="font-weight:400"></h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
          
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <input type="hidden" name = "data_topup_id_hidden" id="data_topup_id_hidden">
		    <h4 style="display: inline-block; font-weight:300">
                     Thank you. Your request submitted successfully.
                    </h4>
		   
                  </div>
               </div>

         </div>
               
              
            </div>
         </div>
       </div>
 <!-- Buy plan or topup modal success open -->
<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/scripts.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/material.min.js"></script>
<!-- Ripples core JavaScript -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/ripples.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js.download"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/responsiveCarousel.min.js"></script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0wqDpyD8TYvX-6KVfGpLxPNXqpVbxJFc&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(
       function() {
          //$("#modal").click(function () {
          //alert('Hello')
          $("#myModal").addClass("in");
          //$("#myModal").show("slow");
       
   });
</script>

<script type="text/javascript" language="javascript">
   $(function(){
      var windowHeight = $(window).innerHeight();
      $('#wrapper_right').css({'height':(windowHeight)+'px'});
      /*
      $('.wrapper').css({'height':($(document).height())+'px'});
      $(window).resize(function(){
         $('.wrapper').css({'height':($(document).height())+'px'});
      });
      $('#wrapper_right').css({'height':($(document).height())+'px'});
      $(window).resize(function(){
         $('#wrapper_right').css({'height':($(document).height())+'px'});
      });*/
   });
</script>

<script type="text/javascript" language="javascript">
   $(document).ready( function() {
      $('#myCarousel').carousel({
         interval:   4000
      });

      var clickEvent = false;
      $('#myCarousel').on('click', '.deepak_nav a', function() {
         clickEvent = true;
         $('.deepak_nav li').removeClass('active');
         $(this).parent().addClass('active');
      }).on('slid.bs.carousel', function(e) {
         if(!clickEvent) {
            var count = $('.deepak_nav').children().length -1;
            var current = $('.deepak_nav li.active');
            current.removeClass('active').next().addClass('active');
            //current.removeClass('active').next().find('a:first').css('color', 'white');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
               $('.deepak_nav li').first().addClass('active');
            }
         }
         clickEvent = false;
      });
   });
   // function to get shop data
   function shop_data(){
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>home/shop_data",
         async:false,
         cache: false,
         data: "type="+"shop",
         success: function(data){
            $("#shop_data").html(data);
            $("#loading").hide();
         }
      });
   }
   
   // function to get plan to topup
   function plan_topup_data(){
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>home/plan_topup_data",
         async:false,
         cache: false,
         data: "type="+"plan_topup",
         success: function(data){
            $("#plan_topup_data").html(data);
            $("#loading").hide();
            // for plan slider
                      $('.crsl-items').carousels({
                        visible: 4,
                        itemMinWidth: 150,
                        itemEqualHeight: 250,
                        itemMargin: 0,
                        infinite: false,
                        itemClassActive:'crsl-active',
                       
                     });
         }
      });
   }
    // function to call when particular plan click
      function plan_type_plan(plantype) {
        $(".plan_type_plan_color_all, .plan_type_plan_color_unlimited, .plan_type_plan_color_fup, .plan_type_plan_color_data").css("color", "#414042");
        if (plantype == 'All') {
            $(".plan_type_plan_color_all").css("color", "#f00f64");
        }else if (plantype == 'Unlimited') {
            $(".plan_type_plan_color_unlimited").css("color", "#f00f64");
        }
        else if (plantype == 'Fup') {
            $(".plan_type_plan_color_fup").css("color", "#f00f64");
        }
        else if (plantype == 'Data') {
            $(".plan_type_plan_color_data").css("color", "#f00f64");
        }
        $("#loading").show();
         $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>login/plan_type_plan",
            async:false,
            cache: false,
            data: "type="+plantype,
            success: function(data){
               $("#plan_slider_content").html(data);
               
                $("#loading").hide();   
                // for plan slider
                         $('.crsl-items').carousels({
                           visible: 4,
                           itemMinWidth: 150,
                           itemEqualHeight: 250,
                           itemMargin: 0,
                           infinite: false,
                        });
         
                   
                        
            }
         });
   }
   // function to get about us detail
   function about_us_data() {
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>home/about_us_data",
         async:false,
         cache: false,
         data: "type="+"about_us",
         success: function(data){
            $("#about_us_data").html(data);
            $("#loading").hide();
         }
      });
   }
   
   // function to get services detail
   function services_data() {
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>home/services_data",
         async:false,
         cache: false,
         data: "type="+"about_us",
         success: function(data){
            $("#services_data").html(data);
            $("#loading").hide();
         }
      });
   }
   //function to get contact us detail
   function contact_us_data() {
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>home/contact_us_data",
         async:false,
         cache: false,
         data: "type="+"about_us",
         success: function(data){
            $("#contact_us_data").html(data);
            $("#loading").hide();
         }
      });
   }
   
     // function to get promotion data
   function promotion_data() {
      $("#loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>login/promotion_data",
         async:false,
         cache: false,
         data: "type="+"promotion_data",
         success: function(data){
            $("#promotion_data").html(data);
            $("#loading").hide();
         }
      });
   }
   //on plan or tupup click open request modal and set service id to hidden field
   function plan_topup_modal(srvid){
      
      $("#request_srvid").val(srvid);
      $("#plan_topup").modal("show");
   }
   // function to request plan or topup
   function request_plan_topup() {
      var srvid = $("#request_srvid").val();
      var name = $("#request_name").val();
      var email = $("#request_email").val();
      var mobile = $("#request_mobile_number").val();
      var msg = $("#request_msg").val();
      if (name == '' || email == '' || mobile == '' || msg == '') {
         $("#request_error").html("please fill all the fields");
      }else{
         if (!ValidateEmail(email)) {
             $("#request_error").html("please Enter valid email");
         }else{
            if (!validateMobile(mobile)) {
               $("#request_error").html("Please Enter 10 digit valid mobile number");
            }else{
               $("#loading").show();
               $("#request_error").html("");
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url()?>home/request_plan_topup",
                  async:false,
                  cache: false,
                  data: "name="+name+"&email="+email+"&mobile="+mobile+"&msg="+msg+"&srvid="+srvid,
                  success: function(data){
                     $("#request_name").val('');
                     $("#request_email").val('');
                     $("#request_mobile_number").val('');
                     $("#request_msg").val('');
                     $("#loading").hide();
                     $("#plan_topup").modal("hide");
                     $("#request_success").modal("show");
                  }
               });
               
            }
            
         }
      }
   }
    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
    function validateMobile(mobile){
      var expr = /^\d{10}$/;
        return expr.test(mobile);
    }
    
    
    // function to request contact us
   function request_contact_us() {
      var name = $("#contact_us_name").val();
      var email = $("#contact_us_email").val();
      var mobile = $("#contact_us_mobile").val();
      var msg = $("#contact_us_msg").val();
      if (name == '' || email == '' || mobile == '' || msg == '') {
         $("#contact_us_error").html("please fill all the fields");
      }else{
         if (!ValidateEmail(email)) {
             $("#contact_us_error").html("please Enter valid email");
         }else{
            if (!validateMobile(mobile)) {
               $("#contact_us_error").html("Please Enter 10 digit valid mobile number");
            }else{
               $("#loading").show();
               $("#contact_us_error").html("");
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url()?>home/request_plan_topup",
                  async:false,
                  cache: false,
                  data: "name="+name+"&email="+email+"&mobile="+mobile+"&msg="+msg,
                  success: function(data){
                     $("#contact_us_name").val('');
                     $("#contact_us_email").val('');
                     $("#contact_us_mobile").val('');
                     $("#contact_us_msg").val('');
                     $("#loading").hide();
                     $("#request_success").modal("show");
                  }
               });
               
            }
            
         }
      }
   }
</script>

</body>
</html>
