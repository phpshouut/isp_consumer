<?php $this->load->view("account/include/header");?>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<section>
   <div class="container-fluid">
      <div class="row">
         <?php $this->load->view("account/include/left_nav");?>
         <div class="container">
            <div class="right_side" id="wrapper_right">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                     <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <table class="table" style="border-top:none">
                           <tr>
                              <td><strong class="strong">Plan Name:</strong></td>
                              <td colspan="2"><?php if(isset($user_plan['planname']))echo $user_plan['planname']?></td>
                           </tr>
                           <tr>
                              <td><strong class="strong">Rental:</strong></td>
                              <td colspan="2">&#8377; <?php if(isset($user_plan['rental']))echo $user_plan['rental']?> / month + Applicable taxes</td>
                           </tr>
                           <tr>
                              <td><strong class="strong">Speed:</strong></td>
                              <td colspan="2"><?php if(isset($user_plan['download_speed']))echo $user_plan['download_speed']?> 
                                                                                            <?php if(isset($user_plan['unlimited_speed']))echo '/ '.$user_plan['unlimited_speed']." (Unlimited)"?> </td>
			   </tr>
                           <tr>
                              <td><strong class="strong">FUP:</strong></td>
                              <td colspan="2"><?php if(isset($user_plan['fup_limit']))echo $user_plan['fup_limit']?>
											    <a href="<?php echo base_url()?>topup" class="btn btn-raised btn-xs change_btn">TOP-UP</a> 
			   </td>
                           </tr>
                           <tr>
                              <td><strong class="strong">Top-Up Plan:</strong></td>
                              <td colspan="2">
                                 <?php
                                    if(isset($user_plan['topup']) && $user_plan['topup'] > 0){
                                       echo '<a href="'.base_url().'topup">'.$user_plan['topup']. " Scheduled</a>";
                                    }
                                    else{
                                       echo "None";
                                       echo '<a href="'.base_url().'topup" class="btn btn-raised btn-xs outline_btn">BUY</a>';
                                    }
                                 ?>
                              </td>
                           </tr>
                           <?php
                           if(isset($user_plan['next_scheduled_plan'])){
                              echo "<tr>";
                              echo "<td><strong class='strong'>Scheduled plan:</strong></td>";
                              echo '<td colspan="2">'.$user_plan["next_scheduled_plan"].'</td>';
                              echo "</tr>";
                              echo "<tr>";
                              echo "<td><strong class='strong'>Scheduled plan Start On:</strong></td>";
                              echo '<td colspan="2">'.$user_plan["next_scheduled_plan_date"].'</td>';
                              echo "</tr>";
                           }
                           ?>
                        </table>
                     </div>
                  </div>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4>LIVE USAGE</h4>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                           <div class="bs-component">
                              <div class="progress">
                                 <div class="progress-bar" style="width: <?php echo $live_usage['uses_percent']?>%">
                                 <?php
                                               echo $live_usage['live_usage'];
                                               ?></div>
                                 
                              </div>
                           </div>
                           <label class="progress_label pull-right"><?php echo $live_usage['uses_limit_msg']?></label>
                        </div>
                     </div>
                     <?php
                        $total_recmd_plan =  count($recommended_plan);
                        if($total_recmd_plan > 0){
                           ?>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                           <h4>RECOMMENDED PLANS</h4>
                           <p style="color:#6D6E71">Updated plans will be applied on a pro-rated basis and balance data will be added to monthly quota</p>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                           <div class="row">
                           <?php
                           foreach($recommended_plan as $recommended_plan1){
                              ?>
                              <div class="col-md-3 col-sm-3 col-xs-4">
                                 <div class="thumbnail">
                                    <div class="caption">
                                       <h6><?php echo $recommended_plan1['plan_name']?></h6>
                                       <h6><?php echo $recommended_plan1['plan_type']?></h6>
                                       <h4>&#8377; <?php echo $recommended_plan1['plan_cost']?> / <small>month</small></h4>
                                       <h4><?php echo $recommended_plan1['speed']?> </h4>
                                       <h4><?php echo $recommended_plan1['total_data']?></h4>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                             <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" style="margin: 2px 1px" onclick="change_plan('<?php echo $recommended_plan1['service_id']?>')">CHANGE</a>
					  </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <?php
                              
                           }
                          
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                     
                           
                           
                        }
                     ?>
                     
		    
                     
                  </div>
							   </div>
							</div>
						</div>
					  </div>
					</div>
				   </section>
			   </div>
		   </div>
	   </div>
      <!-- /Start your project here-->
      
      
      
      
      
      
      <!-- change plan modal -->
    
      <div class="modal fade" id="change_plan" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <!--h4 class="modal-title">Enter New Mobile Number</h4-->
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <center>
                        <input type="hidden" id="change_service_id">
                        <h4 id="plan_change_msg"></h4>
                     </center>
                  </div>
               </div>
               <center>
                  <p id="update_mobile_error" style="color: red"></p>      
               </center>
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="button" id="plan_change_button" value="Ok" class="btn btn-raised btn-danger btn-lg btn-block" onclick="update_plan()">
                     
                  </div>
               </div>
            
         </div>
               
              
            </div>
         </div>
       </div>
      
      
      <div class="modal fade" id="change_plan_success" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" onclick="refresh_page()">&times;</button>
                  <center>
                     <!--h4 class="modal-title">Enter New Mobile Number</h4-->
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <center>
                        <h4 id="plan_success_change_msg"></h4>
                     </center>
                  </div>
               </div>
               <center>
                  <p id="update_mobile_error" style="color: red"></p>      
               </center>
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="button" value="Done" class="btn btn-raised btn-danger btn-lg btn-block" onclick="refresh_page()">
                     
                  </div>
               </div>
            
         </div>
               
              
            </div>
         </div>
       </div>
      
      <script type="text/javascript" language="javascript">
		  $(function(){
				$('.wrapper').css({'height':($(document).height())+'px'});
				$(window).resize(function(){
					$('.wrapper').css({'height':($(document).height())+'px'});
				});
			  $('#wrapper_right').css({'height':($(document).height())+'px'});
				$(window).resize(function(){
					$('#wrapper_right').css({'height':($(document).height())+'px'});
				});
		 });
                  
                  function change_plan(service_id) {
                     //alert(service_id);
                     $("#plan_change_msg").html("Are You sure you want to schedule this plan");
                     $("#change_service_id").val(service_id);
                     $("#change_plan").modal("show");
                  }
                  function update_plan(){
                     $("#plan_change_msg").html("Please wait");
                     $('#plan_change_button').attr('disabled','disabled');
                     var service_id = $("#change_service_id").val();
                     $.ajax({
                        type: "POST",
                        url: "<?php echo base_url()?>plan/schedule_next_plan",
                        async:false,
                        cache: false,
                        data: "service_id="+service_id,
                        success: function(data){
                           $('#plan_change_button').removeAttr('disabled');
                           $("#change_plan").modal("hide");
                           $("#change_plan_success").modal("show");
                           $("#plan_success_change_msg").html(data);
                        }
                     });
                    
                     
                  }
                  
                  function refresh_page(){
                     location.reload();
                  }
	   </script>
   </body>
</html>