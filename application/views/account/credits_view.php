<?php $this->load->view("account/include/header");?>
               <section>
                  <div class="container-fluid">
                     <div class="row">
                        <?php $this->load->view("account/include/left_nav");?>
                        <div class="container">
                           <div class="right_side" id="wrapper_right">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                       <h5>Credit Balance:</h5>
                                       <div class="row" style="margin-bottom: 15px;">
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                             <h3>500 Credits</h3>
                                          </div>
                                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                             <a href="#" class="btn btn-raised btn-xs change_btn" style="padding:4px 25px!important">BUY</a>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <p style='color:#6D6E71'>50 credit points = ? 1.00  Add credit balance to your account and use them to pay you monthly bills.
                                                You can also earn credits by view sponsored content and engaging with us in many ways. <a href="#">Click here</a> to find out more
                                             </p>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <a href="#" class="btn btn-raised btn-danger btn-lg">EARN CREDITS</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <h4>ADD CREDITS</h4>
                                          <p style='color:#6D6E71'>Add credits to your account and never worry about missing a bill payment deadline and get much more bang for the buck!</p>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <div class="row">
                                             <div class="col-md-3 col-sm-3 col-xs-4">
                                                <div class="thumbnail_yellow">
                                                   <div class="caption_yellow">
                                                      <h6>6 + 1 Month Benefit</h6>
                                                      <h4>? 6000</h4>
                                                      <h4>300000 <small>Credits</small></h4>
                                                      <h5>6 + 1 month free!!</h5>
                                                   </div>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left_yellow" style=" margin: 5px 1px">BUY</a>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right_yellow" style=" margin: 5px 1px;">View Details</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-4">
                                                <div class="thumbnail_yellow">
                                                   <div class="caption_yellow">
                                                      <h6>1.5X Benefit</h6>
                                                      <h4>? 200</h4>
                                                      <h4>150000 <small>Credits</small></h4>
                                                      <h5>Value worth ? 300</h5>
                                                   </div>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left_yellow" style=" margin: 5px 1px">BUY</a>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right_yellow" style=" margin: 5px 1px">View Details</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-4">
                                                <div class="thumbnail_yellow">
                                                   <div class="caption_yellow">
                                                      <h6>2X Benefit</h6>
                                                      <h4>? 500</h4>
                                                      <h4>50000 <small>Credits</small></h4>
                                                      <h5>Value worth ? 1000</h5>
                                                   </div>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left_yellow" style=" margin: 5px 1px">BUY</a>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right_yellow" style=" margin: 5px 1px">View Details</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-4">
                                                <div class="thumbnail_yellow">
                                                   <div class="caption_yellow">
                                                      <h6>1.5X Benefit</h6>
                                                      <h4>? 1000</h4>
                                                      <h4>100000 <small>Credits</small></h4>
                                                      <h5>Value worth ? 2000</h5>
                                                   </div>
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
                                                            <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left_yellow" style=" margin: 5px 1px">BUY</a>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
                                                            <a href="#" class="btn btn-xs thumbnail_btn_right_yellow" style=" margin: 5px 1px">View Details</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
     
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
      </script>
   </body>
</html>