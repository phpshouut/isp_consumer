<?php $this->load->view("account/include/header");?>
 <link rel="stylesheet" href="<?php echo base_url()?>assets/css/pagination/dataTables.bootstrap.min.css">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination/dataTables.bootstrap.min.js"></script>
               <section>
                  <div class="container-fluid">
                     <div class="row">
                        <?php $this->load->view("account/include/left_nav");?>
                        <div class="container">
                           <div class="right_side" id="wrapper_right">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
					<?php echo $this->session->flashdata('generate_request_msg')?>
                                       <h4>NEW SERVICE REQUEST</h4>
                                       <form method="post" action="<?php echo base_url()?>service/generate_request" id='generate_request'>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
						   <div class="form-group">
						      <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">Request Type:</label>
						      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							 <select name = 'request_type' id = 'request_type' class="form-control" required> 
							    <!--option value="">Select Ticket Type</option>
							    <option value="change_address">Change Address</option>
							    <option value="change_plan">Change Plan</option>
							    <option value="terminate_service">Terminate Service</option>
							    <option value="suspend_service">Suspend Service</option>
							    <option value="service_down">Service Down</option>
							    <option value="router_not_working">Router Not Working</option>
							    <option value="other_request">Other Request</option-->
							    <?php foreach($service_type as $service_type1){
								  echo "<option value=".$service_type1['ticket_type_value'].">".$service_type1['ticket_type']."</option>";
							       }?>
							 </select>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
						   <div class="form-group">
						      <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">Priority:</label>
						      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							 <select name = 'request_priority' id = 'request_priority' class="form-control" required> 
							    <option value="">Select Ticket Priority</option>
							    <option value="low">Low</option>
							    <option value="medium">Medium</option>
							    <option value="high">High</option>
							 </select>
						      </div>
						   </div>
						</div>
					     </div>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
						   <div class="form-group">
						      <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3">Query:</label>
						      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							 <textarea class="form-control" id= 'request_desc' name = 'request_desc' rows="3"  placeholder="Type here..."></textarea>
						      </div>
						   </div>
						</div>
					     </div>
					     <p id="generate_request_error" style="color: red"> </p>
					     <div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						   <button type="submit" class="btn btn-raised btn-danger btn-lg" style="padding: 10px 40px">SUBMIT</button>
						</div>
					     </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <h4>CURRENT REQUESTS</h4>
                                          <p style='color:#6D6E71'>Only open or in-progress requests will be visible here</p>
                                          <div class="table-responsive">
                                             <table class="table table-striped" id="service_table">
						  <thead>
						       <tr class="table_active">
							  <th class="table_th">REQUEST DATE</th>
							  <th class="table_th">TICKET ID</th>
							  <th class="table_th">TYPE</th>
							  <th class="table_th">ADDITIONAL DETAILS</th>
							  <th class="table_th">STATUS</th>
						       </tr>
						</thead>
                                                <tbody>
						  <?php
						  if(count($current_request)> 0){
						       foreach($current_request as $current_request1){
							  echo "<tr>";
							  echo "<td>".$current_request1['reqiested_on']."</td>";
							  echo "<td>".$current_request1['ticket_id']."</td>";
							  echo "<td>".$current_request1['ticket_type']."</td>";
							  echo "<td>".$current_request1['ticket_desc']."</td>";
							  echo "<td>".$current_request1['status']."</td>";
							  echo "</tr>";
						       }
						  }else{
						       echo '<tr>
                                                      <td>No Pending Request</td></tr>';
						  }
						  ?>
						 
                                                   
                                                   
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                       <h4>REACH US</h4>
                                       <table class="table" style="border-top:none">
                                          <tr>
                                             <td><strong class="strong">Customer Support:</strong></td>
                                             <td><?php echo $this->session->userdata['isp_consumer_session']['support_number1']  ?></td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">Email:</strong></td>
                                             <td colspan="2"><?php echo $this->session->userdata['isp_consumer_session']['support_email']  ?></td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
    
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	  //on generat request validate form
         $('#generate_request').submit(function(){
            if ($("#request_type").val() == '' || $("#request_priority").val() == '' || $("#request_desc").val() == '') {
              $("#generate_request_error").html("Please fill all the fields");
               return false;
            }
            else{
                  return true;
            }
         });
	 
	 
	  $(document).ready(function() {
	       $('#service_table').DataTable({
		    
		    "bLengthChange": false,//for num of record in one page
		    "bFilter": false,// for search box
		    "bInfo": false,// for num of record show in page hide
		    /*"aoColumnDefs": [{  // column shorting by column number
			 'bSortable': false,
			 'aTargets': [ 1,2,3,4,5,6,7,8]
		    }],*/
		    "bSort": false ,// column shorting
		    drawCallback: function(settings) {
			 var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
			 pagination.toggle(this.api().page.info().pages > 1);
		    }
	       });
	  } );
      </script>
   </body>
</html>