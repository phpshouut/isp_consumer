<?php $this->load->view("account/include/header");?>
               <section>
                  <div class="container-fluid">
                     <div class="row">
                        <?php $this->load->view("account/include/left_nav");?>
                        <div class="container">
                           <div class="right_side" id="wrapper_right">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
					<?php echo $this->session->flashdata('update_password_msg')?>
                                          <h4>CHANGE ACCOUNT PASSWORD</h4>
                                        
                                       </div>
                                       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                       <div class="row">
					<form action="<?php echo base_url()?>password/update_password" method="post" id="update_password">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4">Current Password:</label>
                                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                      <input class="form-control"  placeholder="Type Current Password" type="password" name="current_password" id="current_password" required>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4">New Password:</label>
                                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                      <input class="form-control"  placeholder="Type New Password" type="password" name="new_password" id="new_password" required>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4">Re-Enter New Password:</label>
                                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                      <input class="form-control"  placeholder="Confirm Password" type="password" name="confirm_password" id="confirm_password" required>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          
                                          
                                       
                                          <p id="update_password_error" style="color:red"></p>
                                          <div class="form-group">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" class="btn btn-raised btn-danger btn-lg" style="padding: 10px 40px">CHANGE PASSWORD</button>
                                             </div>
                                          </div>
                                       </form>
										   </div>
										</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                       <h4>REACH US</h4>
                                       <table class="table" style="border-top:none">
                                          <tr>
                                             <td><strong class="strong">Customer Support:</strong></td>
                                             <td><?php echo $this->session->userdata['isp_consumer_session']['support_number1']  ?></td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">Email:</strong></td>
                                             <td colspan="2"><?php echo $this->session->userdata['isp_consumer_session']['support_email']  ?></td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
     
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	  $('#update_password').submit(function(){
	       $("#update_password_error").html("");
	       var current_password = $("#current_password").val();
	       var new_password = $("#new_password").val();
	       var confirm_password_val = $("#confirm_password").val();
	       if (current_password == '' || new_password == '' || confirm_password_val == '') {
		   $("#update_password_error").html("Please fill all fields.");
		   return false;
	       }
	       var ajax_response = 0;
	       $.ajax({
		    type: "POST",
		    url: "<?php echo base_url()?>password/check_current_password",
		    async:false,
		    cache: false,
		    data: "current_password="+current_password,
		    success: function(data){
			 
			 if(data == '0'){
			    $("#update_password_error").html("Wrong Current password.");
			     ajax_response = 0;
			 }
			 else{
			     ajax_response = 1;
			 }
		    }
	       });
	       
	       if(ajax_response == '0'){
		    return false;
	       }
	       else{
		    if (new_password != confirm_password_val) {
			 $("#update_password_error").html("Confirm password not matched.");
			 return false;
		    }
		    else{
			return true; 
		    }
	       }
	  });
      </script>
   </body>
</html>