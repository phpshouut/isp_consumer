<?php $this->load->view("account/include/header");?>
 <link rel="stylesheet" href="<?php echo base_url()?>assets/css/pagination/dataTables.bootstrap.min.css">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/pagination/dataTables.bootstrap.min.js"></script>
<div id="loading" style="display: none"><img src="<?php echo base_url()?>assets/images/loader.svg"/></div>
               <section>
                  <div class="container-fluid">
                     <div class="row">
                      <?php $this->load->view("account/include/left_nav");?>
                        <div class="container">
                           <div class="right_side" id="wrapper_right">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                       <h4>PENDING PAYMENTS</h4>
                                       <table class="table" style="border-top:none">
					<form method="post" action="<?php echo base_url()?>bils/check_payment_gateway">
					<?php
					if(count($pending_bill) > 0){
					     $total_current_due = 0;
					     $total_past_due = 0;
					     $total_current_due = $pending_bill['pending_amount'];
					     /*foreach($pending_bill as $pending_bill1){
						  if($pending_bill1['bill_type'] != 'Installation' && $pending_bill1['bill_type'] != 'Security'){
						       $total_current_due = $total_current_due+$pending_bill1['bill_amount'];
						  echo '<tr>
							    <td width = "200px"><strong class="strong">'.$pending_bill1['bill_type'].'</strong></td>
							    <td colspan="2">'.$pending_bill1['bill_amount'].'</td>
						        </tr>';
						  }
						  
					     }
					      echo '<tr >
						       <td style = "padding-bottom: 30px;" width = "200px"><strong class="strong">Current Due</strong></td>
						       <td colspan="2"><strong class="strong">'.$total_current_due.'</strong></td>
						  </tr>';
						  $total_past_due = 0;
					     foreach($pending_bill as $pending_bill1){
						  if($pending_bill1['bill_type'] == 'Installation' || $pending_bill1['bill_type'] == 'Security'){
						       $total_past_due = $total_past_due+$pending_bill1['bill_amount'];
						  echo '<tr>
							    <td><strong class="strong" width = "200px">'.$pending_bill1['bill_type'].'</strong></td>
							    <td colspan="2">'.$pending_bill1['bill_amount'].'</td>
						        </tr>';
						  }
						  
					     }
					     if($total_past_due > 0){
						  echo '<tr>
							    <td style = "padding-bottom: 30px;" width = "200px"><strong class="strong">Past Due</strong></td>
							    <td colspan="2"><strong class="strong">'.$total_past_due.'</strong></td>
						       </tr>';
					     }*/
					     $total_amount_pay = $total_current_due+$total_past_due;
					     echo '<input type="hidden" name = "total_current_due" value="'.$total_current_due.'">';
					     echo '<input type="hidden" name = "total_amount_pay" value="'.$total_amount_pay.'">';
					     echo '<tr>
							    <td style="font-size:16px"><strong class="strong">TOTAL PAYMENT DUE</strong> : <strong class="strong">'.$cocurrency." ".number_format($total_amount_pay, 2).'</strong></td>
							    <td colspan="2"></td>
						       </tr>';
					      if($total_amount_pay > 0){
						echo '<tr>
						       <td>
							  <button type="submit" class="btn btn-raised btn-danger btn-lg" style="margin:0px;">PAY NOW</button>
							  <div style="color:red">
							  '.$this->session->flashdata("msgLogin").'
							 </div>
							  
						       </td>
						</tr>'; 
					      }
					    
					}else{
					     echo '<tr>
                                             <td><strong class="strong">No Bill Pending</strong></td>
                                             
                                          </tr>';
					}
					?>
                                          
                                          
                                          </form>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                             <h4>DOWNLOAD BILLS</h4>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <strong class="strong">Select Month:</strong>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                   <select class="form-control">
                                                      <option>April (Current Month)</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                   </select>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                   <a href="#" class="btn btn-raised btn-xs change_btn">
                                                   <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                   </a> 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div-->
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <h4>PAST TRANSACTIONS</h4>
                                          <div class="table-responsive">
                                             <table class="table table-striped " id="bill_table">
						  <thead>
						       <tr class="table_active">
							    <th class="table_th">GENERATED ON</th>
							     <!--th class="table_th">PAID ON</th-->
							     <th class="table_th">BILL TYPE</th>
							     <th class="table_th">SERVICES</th>
							     <th class="table_th">BILL NUMBER</th>
							     <th class="table_th">RECEIPT NUMBER</th>
							     <th class="table_th">TRANSACTION ID</th>
							     <th class="table_th">AMOUNT</th>
							     <!--th class="table_th">MODE</th>
							     <th class="table_th">STATUS</th-->
							    <!--th class="table_th">RECEIPT</th-->
							    <!--th>Action</th-->
						       </tr>
						  </thead>
						  
                                                <tbody>
						  <?php
						  foreach($bils_model as $bils_model1){
						       if($bils_model1['bill_payed'] == '1'){
							 $background_color = ' style="background-color:#4DDC54;text-align:center"';   
						       }else{
							    $background_color = ' style="background-color:#F4C205;text-align:center"';
						       }
						       $bill_id = $bils_model1['id'];
						       $payment_type = $bils_model1['payment_type'];
						       
							  echo "<tr>";
							  echo "<td>".$bils_model1['generated_on']."</td>";
							  //echo "<td>".$bils_model1['paid_on']."</td>";
							  echo "<td>".$bils_model1['payment_type']."</td>";
							  echo "<td>".$bils_model1['service_name']."</td>";
							  echo "<td>".$bils_model1['bill_number']."</td>";
							  echo "<td>".$bils_model1['receipt_number']."</td>";
							  echo "<td>".$bils_model1['transaction_id']."</td>";
							  echo "<td>".$cocurrency." ".$bils_model1['payment_amount']."</td>";
							  //echo "<td>".$bils_model1['payment_mode']."</td>";
							  //echo "<td".$background_color.">".$bils_model1['bill_status']."</td>";
							  //<td><i class="fa fa-cloud-download" aria-hidden="true"></i></td>
							  //echo '<td><a title="View Receipt" style="text-decoration: none; color:#29abe2; " onclick="billcopy_actions(\''.$bill_id.'\',\''.$payment_type.'\', \'viewbill\')" href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>';
							  echo "</tr>";
							  
						       }
						 ?>
                                                 
                                             
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
   
   
   
   <div class="modal fade" id="viewbillModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	       <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
	       </div>
	       <div class="modal-body" style="padding-bottom:5px" id="billhtmldiv">
	       </div>
	       <div class="modal-footer" style="text-align: right">
	       </div>
	    </div>
         </div>
      </div>
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	 
	 function billcopy_actions(billid,billtype,action) {
	    $('#loading').show();
	    $.ajax({
		url: '<?php echo base_url()?>'+'bils/billcopy_actions',
		type: 'POST',
		data: 'billid='+billid+'&billtype='+billtype+'&action='+action,
		async: false,
		success: function(data){
		     if ((billtype != 'downloadbill') || (billtype != 'sendbill')) {
			$('#billhtmldiv').html(data);
			$('#viewbillModal').modal('show');
		     }
		    $('#loading').hide();
		}
	    });
	 }


	 $(document).ready(function() {
	       $('#bill_table').DataTable({
		    "bLengthChange": false,//for num of record in one page
		    "bFilter": false,// for search box
		    "bInfo": false,// for num of record show in page hide
		    /*"aoColumnDefs": [{  // column shorting by column number
			 'bSortable': false,
			 'aTargets': [ 1,2,3,4,5,6,7,8]
		    }],*/
		  "bSort": false, // column shorting
		    drawCallback: function(settings) {
			 var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
			 pagination.toggle(this.api().page.info().pages > 1);
		    }
	       });
	  } );
      </script>
   </body>
</html>