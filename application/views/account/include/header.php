<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
       <title>
      <?php
      if(isset($this->session->userdata['isp_consumer_session']['isp_name'])){
	 echo $this->session->userdata['isp_consumer_session']['isp_name'];
      }
      ?>
   </title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
      <!-- Material Design Bootstrap -->
      <link href="<?php echo base_url()?>assets/css/material-design.min.css" rel="stylesheet">
      <!-- Ripples Design Bootstrap -->
      <link href="<?php echo base_url()?>assets/css/ripples.min.css" rel="stylesheet">
      <!-- Your custom styles (optional) -->
      <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
      <!--link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"-->
      
      
        <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/material.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
      <!-- Ripples core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/ripples.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
   </head>
     <body>
      <!-- Start your project here-->
      <div class="wapper">
         <div class="container">
            <div class="sub_wapper">
               <header>
                  <div class="navbar navbar-default navbar-fixed-top">
                     <div class="container">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav_top">
                        <div class="navbar-header">
                           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           </button>
                           <a class="navbar-brand" href="<?php echo base_url()?>home">
                           <i class="fa fa-home" aria-hidden="true"></i>
                           </a>
                        </div>
                        <div class="navbar-collapse">
                           <ul class="nav navbar-nav" >
                              <li>
                                 <a href="#">
				    <?php
				    if(isset($this->session->userdata['isp_consumer_session']['isp_logo']) && $this->session->userdata['isp_consumer_session']['isp_logo'] != ''){
				       $logo_path = $this->session->userdata['isp_consumer_session']['isp_logo'];
				       echo '<img src="'.$logo_path.'" class="img-responsive" width="70%"/>';
				    }
				    else{
				       ?>
				       <img src="<?php echo base_url()?>assets/images/isp_logo.png" class="img-responsive" width="70%"/>
				       <?php
				    }
				    ?>
                                 
                                 </a>
                              </li>
                              <!--<li><a href="#">Link</a></li>-->
                           </ul>
                           <ul class="nav navbar-nav navbar-right">
                              <li>
                                 <a href="#">
                                    <div class="provider_logo">
                                       <img src="<?php echo base_url()?>assets/images/shouut_logo.svg"/>
                                    </div>
                                 </a>
                              </li>
                              <li>
                                 <a href="<?php echo base_url()?>home" class="nav_btn">BACK TO HOME</a>
			      <center><span class="top_header_span">UID : <?php echo $this->session->userdata['isp_consumer_session']['user_uid']  ?></span></center>
                              </li>
                              <li style="border-left:1px solid #E6E7E8">
								  <a href="#" style="cursor: default">
									<span class="top_header_right_span">
									 <h5>REACH US</h5>
										<h6><?php echo $this->session->userdata['isp_consumer_session']['support_number1']  ?></h6>
										<h6><?php echo $this->session->userdata['isp_consumer_session']['support_number2']  ?></h6>
										
										<h6><?php echo $this->session->userdata['isp_consumer_session']['support_email']  ?> </h6>
									</span>  
								   </a>
                              </li>
                           </ul>
                        </div>
						 </div>
                     </div>
                  </div>
               </header>