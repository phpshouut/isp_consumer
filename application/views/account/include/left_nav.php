<?php
$url = $_SERVER['REQUEST_URI'];
$method_name = $this->router->fetch_method();
$controller_name = $this->router->fetch_class();
$usage_style = '';
$plan_style = '';
$topup_style = '';
$credits_style = '';
$bils_style = '';
$service_style = '';
$password_style = '';
$logs_style = '';
if($controller_name == 'usage'){
        $usage_style = "class = 'active'";
}
elseif($controller_name == 'plan'){
	$plan_style = "class = 'active'";
}
elseif($controller_name == 'topup'){
	$topup_style = "class = 'active'";
}
elseif($controller_name == 'credits'){
	$credits_style = "class = 'active'";
}
elseif($controller_name == 'bils'){
	$bils_style = "class = 'active'";
}
elseif($controller_name == 'service'){
	$service_style = "class = 'active'";
}
elseif($controller_name == 'password'){
	$password_style = "class = 'active'";
}
elseif($controller_name == 'logs'){
	$logs_style = "class = 'active'";
}
?>
<div class="navbar navbar-fixed-left">
                           <ul>
                              <li ><a href="<?php echo base_url()?>usage" <?php echo $usage_style?>>My Details & Usage</a></li>
                              <li><a href="<?php echo base_url()?>plan" <?php echo $plan_style?>>My Plan</a></li>
                              <li><a href="<?php echo base_url()?>topup" <?php echo $topup_style?>>Top-Up Plans</a></li>
                              <!--li><a href="<?php echo base_url()?>credits" <?php echo $credits_style?>>My Credits</a></li-->
                              <li><a href="<?php echo base_url()?>bils" <?php echo $bils_style?>>Bills & Payments</a></li>
                              <li><a href="<?php echo base_url()?>service" <?php echo $service_style?>>Service Request</a></li>
			      <li><a href="<?php echo base_url()?>logs" <?php echo $logs_style?>>Logs & Activity</a></li>
                              <li><a href="<?php echo base_url()?>password" <?php echo $password_style?>>Change Password</a></li>
			      <li><a href="<?php echo base_url()?>login/logout">Logout</a></li>
                           </ul>
                        </div>