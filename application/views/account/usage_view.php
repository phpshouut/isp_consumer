<?php $this->load->view("account/include/header");?>
<div id="loading" style="display: none"><img src="<?php echo base_url()?>assets/images/loader.svg"/></div>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
               <section>
                  <div class="container-fluid">
                     <div class="row">
                        <?php $this->load->view("account/include/left_nav");?>
                        <div class="container">
                           <div class="right_side" id="wrapper_right">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                       <?php echo $this->session->flashdata('updated_usage')?>
                                       <table class="table" style="border-top:none">
                                          <tr>
                                             <td><strong class="strong">Name:</strong></td>
                                             <td colspan="2"><?php if(isset($user_info['name']))echo $user_info['name']?></td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">UID:</strong></td>
                                             <td colspan="2"><?php if(isset($user_info['uid']))echo $user_info['uid']?></td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">User Name:</strong></td>
                                             <td colspan="2"><?php if(isset($user_info['username']))echo $user_info['username']?></td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">Email:</strong></td>
                                             <td colspan="2"><?php if(isset($user_info['email']))echo $user_info['email']?>
                                                <a href="#" class="btn btn-raised btn-xs change_btn" onclick="change_email('<?php echo $user_info['email']?>')">CHANGE</a> 
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><strong class="strong">Mobile:</strong></td>
                                             <td colspan="2">+91 <?php if(isset($user_info['mobile']))echo $user_info['mobile']?>
                                             <a href="#" class="btn btn-raised btn-xs change_btn" onclick="change_mobile('<?php echo $user_info['mobile']?>')">CHANGE</a> 
                                             </td>
                                          </tr>
                                      
                                       </table>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                       <table class="table">
                                          <tr>
                                             <td><strong class="strong">Address:</strong></td>
                                             <td colspan="2">
                                                <?php if(isset($user_info['address']))echo $user_info['address']?>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="25%"><strong class="strong">City / Zone:</strong></td>
                                             <td colspan="2">
                                                <?php if(isset($user_info['city']))echo $user_info['city']?> / <?php if(isset($user_info['zone']))echo $user_info['zone']?>
                                             </td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <h4>LIVE USAGE</h4>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <div class="progress">
                                             <div class="progress-bar" style="width: <?php echo $live_usage['uses_percent']?>%">
                                               <?php
                                               echo $live_usage['live_usage'];
                                               ?>
                                             </div>
                                          </div>
                                          <label class="progress_label pull-right"><?php echo $live_usage['uses_limit_msg']?></label>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <strong>Usage Logs:</strong>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                   <select class="form-control" id="users_lons_month">
                                                      <?php
                                                      $m = date('n');
                                                      for($i=0;$i<6;$i++){
                                                          if(strtotime(date('Y-m', mktime(0,0,0,$m-$i,15,date('Y')))) > strtotime("2017-04")){
                                                            echo "<option value ='".date('m Y', mktime(0,0,0,$m-$i,15,date('Y')))."'>".date('M', mktime(0,0,0,$m-$i,15,date('Y')))."</option>";
                                                          }
            
                                                      }
                                                      ?>
                                                   </select>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                   <a href="#" class="btn btn-raised btn-xs change_btn" onclick="usages_log()">VIEW</a> 
                                                   <!--a href="#" class="btn btn-raised btn-xs change_btn">
                                                      <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                   </a--> 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
                                          <div class="table-responsive">
                                             <table class="table table-striped">
                                                <tr class="table_active">
                                                   <th class="table_th">SESSION STARTS</th>
                                                   <th class="table_th">SESSION ENDS</th>
                                                   <th class="table_th">DATA DOWNLOAD (MB)</th>
                                                   <th class="table_th">DATA UPLOAD (MB)</th>
                                                </tr>
                                                <tbody id="usage_data">
                                                   
                                                
                                                  
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      
       <!-- email change Modal -->
       <div class="modal fade" id="email_change" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title strong">Enter New Email ID</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>usage/update_email" id='update_email'>
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="hidden" name="user_email_hidden" id="user_email_hidden">
                     <input type="email" class="form-control form-modal"  placeholder="Email" onblur="this
                     .placeholder='Email'" onfocus="this.placeholder=''" id="user_email" name="user_email" required="required">
                  </div>
               </div>
               <center>
                  <p id="update_email_error" style="color: red"></p>      
               </center>
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="submit" name="update_email" value="UPDATE" class="btn btn-raised btn-danger btn-lg btn-block">
                  </div>
               </div>
            </form>
         </div>
               
              
            </div>
         </div>
       </div>
       
       
       <!-- mobile change Modal -->
       <div class="modal fade" id="mobile_change" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title strong">Enter New Mobile Number</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <!--form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>usage/update_mobile" id='update_mobile'-->
             
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="hidden" name="user_mobile_hidden" id="user_mobile_hidden">
                     <input type="text" class="form-control form-modal"  placeholder="Mobile Number" onblur="this
                     .placeholder='Mobile Number'" onfocus="this.placeholder=''" id="user_mobile" name="user_mobile" required="required">
                  </div>
               </div>
               <center>
                  <p id="update_mobile_error" style="color: red"></p>      
               </center>
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="button" name="update_email" value="UPDATE" class="btn btn-raised btn-danger btn-lg btn-block" onclick="send_otp()">
                  </div>
               </div>
            
         </div>
               
              
            </div>
         </div>
       </div>
  
  
  
  <!-- Verify modal change Modal -->
       <div class="modal fade" id="verify_otp" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title">Verify OTP</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>usage/update_mobile" id='update_mobile'>
             
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
                     <input type="hidden" name="user_old_mobile_hidden_update" id="user_old_mobile_hidden_update">
                     <input type="hidden" name="user_new_mobile_hidden_update" id="user_new_mobile_hidden_update">
                     <input type="text" class="form-control form-modal"  placeholder="Enter OTP" onblur="this
                     .placeholder='Enter OTP'" onfocus="this.placeholder=''" id="user_otp" name="user_otp" required="required">
                  </div>
               </div>
               <center>
                  <p id="verify_otp_error_msg" style="color: red"></p>      
               </center>
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="submit" name="update_email" value="DONE" class="btn btn-raised btn-danger btn-lg btn-block">
                  </div>
               </div>
            
         </div>
               
              
            </div>
         </div>
       </div>
      <!-- SCRIPTS -->
    
      <script type="text/javascript" language="javascript">
         $(function(){
            $('#wrapper').css({'height':($(document).height())+'px'});
            $(window).resize(function(){
                   $('#wrapper').css({'height':($(document).height())+'px'});
            });
             $('#wrapper_right').css({'height':($(document).height())+'px'});
            $(window).resize(function(){
                   $('#wrapper_right').css({'height':($(document).height())+'px'});
            });
            
            //call uses log method
               usages_log('');
             
         });
         
         //ajax to get data of usage logs
            function usages_log() {
               $("#loading").show();
                var months = '';
                months = $("#users_lons_month").val();
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url()?>usage/usage_logs",
                  async: false,
		    cache: false,
		    data: "month="+months,
		    success: function(data){
			$("#usage_data").html(data);
                        $("#loading").hide();
		    }
               });
            }
         
         // change email popup open
         function change_email(email) {
            $("#user_email_hidden").val(email);
            $("#user_email").val(email);
            $('#email_change').modal('show');
         }
         //email update time check email validation
         $('#update_email').submit(function(){
            if ($("#user_email").val() == '') {
              $("#update_email_error").html("Fill email id");
               return false;
            }
            else{
                  return true;
            }
         });
         
         // change mobile popup open
         function change_mobile(mobile) {
            $("#user_mobile_hidden").val(mobile);
            $("#user_mobile").val(mobile);
            $('#mobile_change').modal('show');
         }
         //send otp
         function send_otp() {
            var new_number = $("#user_mobile").val();
            var old_number = $("#user_mobile_hidden").val()
            var valid_mobile = $.isNumeric( $("#user_mobile").val());
            if ($("#user_mobile").val() == '' || valid_mobile == false || $("#user_mobile").val().length!= '10') {
              $("#update_mobile_error").html("Enter 10 digit valid mobile number");
               return false;
            }
            else{
               $.ajax({
                  type: "POST",
                  url: "<?php echo base_url()?>usage/send_otp",
                  async: false,
		    cache: false,
		    data: "old_number="+old_number+"&new_number="+new_number,
		    success: function(data){
			 if(data == '0'){
			      $("#update_mobile_error").html("Enter new number");
			 }else{
                             $("#mobile_change").modal("hide");
			     $("#verify_otp").modal("show");
                             $("#user_old_mobile_hidden_update").val(old_number);
                             $("#user_new_mobile_hidden_update").val(new_number);
			 }
		    }
               });
            }
            
         }
         //mobile update time check mobile validation
         $('#update_mobile').submit(function(){
            var otp = $("#user_otp").val();
            var ajax_response = 0;
            $.ajax({
               type: "POST",
               url: "<?php echo base_url()?>usage/verify_otp",
               async:false,
               cache: false,
               data: "otp="+otp,
               success: function(data){
                  if(data == '0'){
                     $("#verify_otp_error_msg").html("Invalid OTP.");
                      ajax_response = 0;
                  }
                  else{
                      ajax_response = 1;
                  }
      
               }
            });
            if(ajax_response == '0'){
               return false;
            }
            else{
               return true;
            }

            return false;
         });
      </script>
   </body>
</html>