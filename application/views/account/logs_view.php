<?php
$this->load->view("account/include/header");
$user_uid = '';
if(isset($this->session->userdata['isp_consumer_session']['user_uid'])){
            $user_uid = $this->session->userdata['isp_consumer_session']['user_uid'];    
        }
?>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<?php echo base_url()?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/daterangepicker.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/canvasjs.min.js"></script>

<div id="loading" style="display: none"><img src="<?php echo base_url()?>assets/images/loader.svg"/></div>
<section>
     <div class="container-fluid">
	  <div class="row">
               <?php $this->load->view("account/include/left_nav");?>
               <div class="container">
		    <div class="right_side" id="wrapper_right">
			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      
			      
			      
			      <div class="row">
				   <div class="col-sm-3 col-xs-3">
					<div class="row">
					     <div class="garfes_div">
						 <ul>
						   <li>
						     <input type="radio" value="1" id="usage-garfes" name="logs_activity_graph" checked="checked">
						     <label for="usage-garfes">USAGE GRAPH </label>
						     <div class="check"></div>
						   </li>
						 </ul>
					     </div>
					</div>
				   </div>
				   <div class="col-sm-4 col-xs-4">
					<div class="row">
					     <div class="garfes_div">
						  <ul>
						     <li>
						      <input type="radio" value="2" id="speed-garfes" name="logs_activity_graph">
						      <label for="speed-garfes">SPEED GRAPH</label>
						      <div class="check"><div class="inside"></div></div>
						    </li>
						  </ul>
					     </div>
					</div>
				   </div>
                              </div>
			      
			      
			      
			      <div id="usages_graph_div">
				   <div class="row" >
					<div class="col-sm-2 col-xs-2">
					     <h2>Usage</h2>
					</div>
					<div class="col-sm-10 col-xs-10">
					     <div class="col-md-4 col-sm-4 col-xs-4 demo">
						  <i class="glyphicon glyphicon-calendar fa fa-calendar datefinder_icons"></i>
						  <input name="datefilter" value="<?php echo $this->logs_model->usage_graph_billing_date($user_uid)?>" class="datefinder" type="text" placeholder="DD-MM-YYYY" onblur="this.placeholder='DD-MM-YYYY'" onfocus="this.placeholder=''" >
					     </div>
					     <div class="col-md-8 col-sm-8 col-xs-8">
						  <div class="form-inline">
						      <label class="form-inline_lable" style="font-size:12px; margin-right:20px"><span>Total used: </span><strong id="total_used" style="color:#36465f; font-weight:600">0 GB</strong> </label>
						      <label class="form-inline_lable" style="font-size:12px; margin-right:20px"><span>Total download: </span><strong id="total_download" style=" color:#36465f; font-weight:600">0 GB</strong> </label>
						      <label class="form-inline_lable" style="font-size:12px; margin-right:20px"><span>Total upload: </span><strong id="total_upload"  style="color:#36465f; font-weight:600">0 GB</strong> </label>
						  </div>
					     </div>
					</div>
				   </div>
				   <div class="bar_chart" id="chartContainer" style="height: 300px; width: 80%">
					<img src="" class="img-responsive"/>
				   </div>
			      </div>
							       
							       
							       
				   
			      <div id="data_graph_div" style="display:none">
				   <div class="row" >
                                        <div class="col-sm-3 col-xs-3"><h2>Data</h2></div>
					<div class="col-sm-6 col-xs-6">
					     <div class="row">
						  <div class="col-md-12 demo">
						       <div class="row"> 
							    <div class="col-sm-3 col-xs-3">
							       <label style=" font-size: 10px; font-weight: 600; color: rgba(0,0,0,.87)">Select Date Filter</label>
							    </div>
							    <div class="col-sm-4 col-xs-4">
								 <div class="datefilters">
								      <select  name="data_graph_value" id="data_graph_value"  class="select_datefilters" onchange="data_logs()" style="background-position:right center">
									   <option value="today">Today</option>
									   <option value="yesterday">Yesterday</option>
									   <option value="last_week">Last week</option>
									   <option value="last_month"> Last month</option>
		   
									   <option value="last_year">Last year</option>
		   
								      </select>
		   
								 </div>
							    </div>
						       </div>
						  </div>
					     </div>
					</div>
				   </div>
				   <div class="bar_chart" id="DatachartContainer" style="height: 300px; width: 80%">
					<img src="" class="img-responsive"/>
				   </div>
			      </div>
				       
				       
                                      

				       
                                       



			      
			      
			      
			      
			      
			      
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
    
    
    
     
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	 $(function() {
	    $('input[name="datefilter"]').daterangepicker({
	       locale: {
		  format: 'DD.MM.YYYY',
	       },
	       //startDate: moment().startOf('month').format('DD.MM.YYYY'),
	       //endDate: moment(),
	       "maxDate": moment(),
		}).on('change', function(e){
	       
		 usuage_logs();
	    });
	    
	    
	 });
	 
     $(document).ready(function(){
	  $('#loading').show();
	  usuage_logs();
     });
     
     $("input[name='logs_activity_graph']").change(function() {
	 
	 var graph_type = $("input[name='logs_activity_graph']:checked").val();
	 if (graph_type == '1') {
	    usuage_logs();
	    $("#usages_graph_div").show();
	    $("#data_graph_div").hide();
	 }else{
	    data_logs();
	    $("#usages_graph_div").hide();
	    $("#data_graph_div").show();
	 }
     });
	 
     function usuage_logs() {
   
	  $('#loading').show();
	  //var cust_uuid = $('input[name="subscriber_uuid"]').val();
	  var cust_uuid = '<?php echo $user_uid ?>';
	  var date_range = $('input[name="datefilter"]').val();
	  $.ajax({
	       url: '<?php echo base_url()?>'+'logs/usuage_logs',
	       type: 'POST',
	       dataType: 'json',
	       data: 'uuid='+cust_uuid+"&date_range="+date_range,
	       success: function(result){
		    $('#loading').hide();
		    $("#total_used").html(result.total_used);
		    $("#total_download").html(result.total_download);
		    $("#total_upload").html(result.total_upload);
	      
		    var d = new Date();
		    var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		    var currmonth = months[d.getMonth()];
		    var chart = new CanvasJS.Chart("chartContainer", {
		     animationEnabled: true,
		     title: {
			 text: "DATA USUAGE OF "+currmonth.toUpperCase(),
			  fontSize:18,
		     },
		     axisX:{
			 title: "Number of Days",
		     },
		     axisY:{
			 title: "Data Usuage",
			 //interval:1024,
			 suffix: " MB"
		     },
		     dataPointMaxWidth: 40,
		     data: [
			 {
			     type: "stackedColumn",
			     legendText: "Between 00:00 to 11:59",
			     showInLegend: "true",
			     dataPoints: result.firsthalfdata
			 },
			 {
			     type: "stackedColumn",
			     legendText: "Between 12:00 to 23:59",
			     showInLegend: "true",
			     dataPoints: result.secondhalfdata
			 }
		     ]
		 });
		 chart.render();
	   
	       }
	  });	
     }
     
     function data_logs() {
   
	  $('#loading').show();
	  //var cust_uuid = $('input[name="subscriber_uuid"]').val();
	  var cust_uuid = '<?php echo $user_uid ?>';
	  var date_range = $('#data_graph_value').val();
	  $.ajax({
	       url: '<?php echo base_url()?>'+'logs/data_logs',
	       type: 'POST',
	       dataType: 'json',
	       data: 'uuid='+cust_uuid+"&date_range="+date_range,
	       success: function(result){
		    $('#loading').hide();
		    var chart = new CanvasJS.Chart("DatachartContainer",
		    {
			title:{
			    text: "Speed Graph",
			     fontSize:18,
			},  
			animationEnabled: true,  
			axisY: {
				       //interval:100,
				       suffix: " KB"
			    /*title: "Units Sold",
			    valueFormatString: "#0,,.",
			    suffix: " m"*/
			},
			data: [
			{        
			    
			    type: "splineArea",
			    color: "#90b040",
			    markerSize: 1,
			    dataPoints: result.input
			}  ,{        
			    type: "splineArea",
			    color: "#8080c0",
			    markerSize: 1,
			    dataPoints: result.output
			}            
			]
		    });
		    chart.render();
	       }
	  });
     }

	 
	 
	 
      </script>
   </body>
</html>