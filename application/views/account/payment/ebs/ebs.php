<?php
$checkSum = "";
$paramList = array();


// Create an array having all required parameters for creating checksum.
$key = $userdata['key'];
$account_id = $userdata['account_id'];
$ebsuser_name = $userdata['ebsuser_name'];
$ebsuser_address = $userdata['ebsuser_address'];
$ebsuser_zipcode = $userdata['ebsuser_zipcode'];
$ebsuser_city = $userdata['ebsuser_city'];
$ebsuser_state = $userdata['ebsuser_state'];
$ebsuser_country = $userdata['ebsuser_country'];
$ebsuser_phone = $userdata['ebsuser_phone'];
$ebsuser_email = $userdata['ebsuser_email'];
$ebsuser_id = $userdata['ebsuser_id'];
$modelno = $userdata['modelno'];
$finalamount = $userdata['finalamount'];
$order_no = $userdata['order_no'];
$return_url = $userdata['return_url'];
$mode = $userdata['mode'];

$hash = $key."|".$account_id."|".$finalamount."|".$order_no."|".$return_url."|".$mode;
$secure_hash = md5($hash);
?>
<!DOCTYPE html>
<html>
<head>
<title>Merchant Check Out Page</title>
</head>
<body>
	<center><h1>Please do not refresh this page...</h1></center>
		<form method="post" action="https://secure.ebs.in/pg/ma/sale/pay" name="frmPaymentConfirm">
		<table border="1">
			<tbody>
			<input name="account_id" value="<?php echo $account_id;?>" type="hidden">
<input name="return_url" size="60" value="<?php echo $return_url; ?>" type="hidden">
<input name="mode" size="60" value="<?php echo $mode; ?>" type="hidden">
<input name="reference_no" value="<?php echo $order_no; ?>" type="hidden">
<input name="description" value="<?php echo $modelno; ?>" type="hidden">
<input name="name" maxlength="255" value="<?php echo $ebsuser_name; ?>" type="hidden">
<input name="address" maxlength="255" value="<?php echo $ebsuser_address; ?>" type="hidden">
<input name="city" maxlength="255" value="<?php echo $ebsuser_city; ?>" type="hidden">
<input name="state" maxlength="255" value="<?php echo $ebsuser_state; ?>" type="hidden">
<input name="postal_code" maxlength="255" value="<?php echo $ebsuser_zipcode; ?>" type="hidden">
<input name="country" maxlength="255" value="<?php echo $ebsuser_country; ?>" type="hidden">
<input name="phone" maxlength="255" value="<?php echo $ebsuser_phone; ?>" type="hidden">
<input name="email" size="60" value="<?php echo $ebsuser_email; ?>" type="hidden">
<input name="secure_hash" size="60" value="<?php echo $secure_hash; ?>" type="hidden">
<input name="amount" id="amount" readonly="" value="<?php echo $finalamount; ?>" type="hidden">
			</tbody>
		</table>
		<script type="text/javascript">
			document.frmPaymentConfirm.submit();
		</script>
	</form>
</body>
</html>
