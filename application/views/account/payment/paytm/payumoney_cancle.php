<?php $this->load->view("account/include/header");?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<section>
     <div class="container-fluid">
	  <div class="row">
               <?php $this->load->view("account/include/left_nav");?>
               <div class="container">
		    <div class="right_side" id="wrapper_right">
			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="row">
				   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <h5>Sorry! Transaction Failed</h5>
                <h6 style="color:#6D6E71">The transaction failed due to some error. Please check the details<br/>entered and try again. <strong>In case you have been charged already, we will<br/>process a refund within 4 business days</strong></h6>
					<table class="table" style="border-top:none">
					    <tr>
                                                <td style="font-weight:500">Transaction ID</td>
                                                <td><?php echo $transection_detail['transactionId']?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight:500">Error Message</td>
                                                <td><?php echo $transection_detail['error_Message']?></td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight:500">Status</td>
                                                <td>Failed</td>
                                            </tr>
                           
                         
                           
                          
                        </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				   <div class="row">
					
				
					
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
    
    
    
     
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	 
      </script>
   </body>
</html>