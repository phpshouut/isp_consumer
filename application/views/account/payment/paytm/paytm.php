<?php
$checkSum = "";
$paramList = array();


// Create an array having all required parameters for creating checksum.
$paramList["MID"] = $userdata['paytm_merchant_mid'];
$paramList["ORDER_ID"] = $userdata["ORDER_ID"];
$paramList["CUST_ID"] = $userdata["CUST_ID"] ;
$paramList["INDUSTRY_TYPE_ID"] = $userdata["INDUSTRY_TYPE_ID"];
$paramList["CHANNEL_ID"] = $userdata["CHANNEL_ID"];
$paramList["TXN_AMOUNT"] = $userdata["TXN_AMOUNT"];
$paramList["WEBSITE"] = $userdata['paytm_merchant_web'];
$paramList["MOBILE_NO"] = $userdata["MOBILE_NO"];
$paramList['MERC_UNQ_REF']=$userdata["CUST_ID"];
$paramList["CALLBACK_URL"] =  $userdata["call_back_url"];
//Here checksum string will return by getChecksumFromArray() function.
$checkSum = getChecksumFromArray($paramList,$userdata['paytm_merchant_key']);
$PAYTM_DOMAIN = "pguat.paytm.com";
if ($userdata['paytm_environment'] == 'PROD') {
	$PAYTM_DOMAIN = 'securegw.paytm.in';
}
define('PAYTM_REFUND_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/REFUND');
define('PAYTM_STATUS_QUERY_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/TXNSTATUS');
//define('PAYTM_TXN_URL', 'https://'.$PAYTM_DOMAIN.'/oltp-web/processTransaction');
define('PAYTM_TXN_URL', 'https://'.$PAYTM_DOMAIN.'/theia/processTransaction');
?>
<!DOCTYPE html>
<html>
<head>
<title>Merchant Check Out Page</title>
</head>
<body>
	<center><h1>Please do not refresh this page...</h1></center>
		<form method="post" action="<?php echo PAYTM_TXN_URL ?>" name="f1">
		<table border="1">
			<tbody>
			<?php
			foreach($paramList as $name => $value) {
				echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
			}
			?>
			<input type="hidden" name="CHECKSUMHASH" value="<?php echo $checkSum ?>">
			</tbody>
		</table>
		<script type="text/javascript">
			document.f1.submit();
		</script>
	</form>
</body>
</html>
