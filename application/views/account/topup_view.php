<?php $this->load->view("account/include/header");?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<section>
     <div class="container-fluid">
	  <div class="row">
               <?php $this->load->view("account/include/left_nav");?>
               <div class="container">
		    <div class="right_side" id="wrapper_right">
			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="row">
				   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<table class="table" style="border-top:none">
					     <?php
					     echo "<p style='color:red'>".$this->session->flashdata("msgLogin")."</p>";
					     $total_topup_apply = count($active_topup);
					     $i = 1;
					     if($total_topup_apply > 0){
						  foreach($active_topup as $active_topup1){
						       echo "<tr><th style='font-size:12px;font-weight:600'>Active Top Up ".$i."</th></tr>";
						       echo "<tr>";
						       echo "<td><strong class='strong'>TopUp Name:</strong></td>";
						       echo "<td colspan='2'>".$active_topup1['topupname']."</td>";
						       echo "</tr>";
						       echo "<tr>";
						       echo "<td><strong class='strong'>TopUp Type:</strong></td>";
						       echo "<td colspan='2'>".$active_topup1['topuptype']."</td>";
						       echo "</tr>";
						       
							    echo "<tr>";
							    echo "<td><strong class='strong'>TopUp Start Date:</strong></td>";
							    echo "<td colspan='2'>".$active_topup1['topupstartdate']."</td>";
							    echo "</tr>";
							    echo "<tr>";
							    echo "<td><strong class='strong'>TopUp End Date:</strong></td>";
							    echo "<td colspan='2'>".$active_topup1['topupenddate']."</td>";
							    echo "</tr>";
						       
						       
						       $i++;
						  }
					     }
					     else{
						 echo "<tr>";
						  echo "<th style='font-size:12px;font-weight:600'>No TopUp Applied:</th>";
						  echo "</tr>";  
					     }
					     ?>
                           
                         
                           
                          
                        </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				   <div class="row">
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
						  <div class="bs-component">
						       <div class="progress">
							  
							  
						       </div>
						  </div>
						  
					     </div>
					</div>
					
					<div class="row">
					    <!-- data topup section start -->
					     <?php
					     if(count($recommend_topup['data_topup']) >0){
						  ?>
						 
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
						     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							<h4>DATA TOP-UP</h4>
							<p style='color:#6D6E71'>Ran out of data / FUP limit? Just add a top-up and get back your speed!</p>
						     </div>
						  </div>
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							    <div class="row">
								 <?php
								 foreach($recommend_topup['data_topup'] as $data_topup){
								      ?>
								 <div class="col-md-3 col-sm-3 col-xs-4">
								      <div class="thumbnail_red">
									   <div class="caption">
									      <h6><?php echo $data_topup['topup_name']?></h6>
									      <h4><?php echo $data_topup['total_data']?></h4>
									      <h4>&#8377; <?php echo $data_topup['topup_price']?></h4>
									   </div>
									   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									      <div class="row">
										 <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
										    <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick="active_data_toupup('<?php echo $data_topup['topup_id'] ?>', '<?php echo $data_topup['total_data']?>', '<?php echo $data_topup['topup_price']?>')">ACTIVATE</a>
										 </div>
										 
									      </div>
									   </div>
								      </div>
								 </div>     
								      <?php
								 }
								 ?>
							    </div>
						       </div>
						  </div>
					     </div>
					      <?php
					     }
					     ?>
					     <!-- data topup section End -->
					     <!-- speed topup start -->
					      <?php
					     if(count($recommend_topup['speed_topup']) >0){
						?>
						
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
						       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							  <h4>SPEED TOP-UP</h4>
							  <p style='color:#6D6E71'>Get on-demand speed burst to watch that movie online or to attend that super important web conference meeting</p>
						       </div>
						  </div>
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							    <div class="row">
								 <?php
								 foreach($recommend_topup['speed_topup'] as $speed_topup){
								      ?>
								 <div class="col-md-3 col-sm-3 col-xs-4">
								      <div class="thumbnail_blue">
									   <div class="caption" style="margin-bottom:15px">
									      <h6><?php echo $speed_topup['topup_name']?></h6>
									      <h6><small style="color: #fff"><?php echo $speed_topup['topup_days']?></small></h6>
									      <h4><?php echo $speed_topup['download_speed']?></h4>
									      <h4>&#8377; <?php echo $speed_topup['topup_price']?> / <small>week</small></h4>
									   </div>
									   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
										   <select class="form-control form-control_select" name = "<?php echo $speed_topup['topup_id'] ?>" id="<?php echo $speed_topup['topup_id'] ?>">
										      <option value='1'>Top-Up for 1week</option>
										      <option value='2'>Top-Up for 2week</option>
										      <option value='3'>Top-Up for 3week</option>
										      <option value='4'>Top-Up for 4week</option>
										      <option value='5'>Top-Up for 5week</option>
										      <option value='6'>Top-Up for 6week</option>
										   </select>
										</div>
									   </div>
									   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									      <div class="row">
										 <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
										    <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick="active_speed_toupup('<?php echo $speed_topup['topup_id'] ?>', '<?php echo $speed_topup['download_speed']?>')">ACTIVATE</a>
										 </div>
										 
									      </div>
									   </div>
								      </div>
								 </div>
								 <?php
								 }
								 ?>
							    </div>
						       </div>
						  </div>
						  
					     </div>
					     <?php
					     }
					     ?>
					     <!-- speed topup End -->
					     
					     <!-- free data topup start -->
					     <?php
					     if(count($recommend_topup['data_unacc_topup']) >0){
						  ?>
					     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
						     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							<h4>FREE DATA TOP-UPS</h4>
							<p style='color:#6D6E71'>Ran out of data / FUP limit? Just add a top-up and get back your speed!</p>
						     </div>
						  </div>
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-left">
							<div class="row">
							<?php
								 foreach($recommend_topup['data_unacc_topup'] as $data_unacc_topup){
								      ?>
							    <div class="col-md-3 col-sm-3 col-xs-4">
								<div class="thumbnail_pink">
								   <div class="caption">
								      <h6><?php echo $data_unacc_topup['topup_name']?></h6>
								      <h6><small style="color: #fff"><?php echo $data_unacc_topup['topup_days']?></small></h6>
								      <h4><?php echo $data_unacc_topup['topup_percent']?>% <small>free data</small></h4>
								      <h4>&#8377; <?php echo $data_unacc_topup['topup_price']?> / <small>week</small></h4>
								   </div>
								   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
										   <select class="form-control form-control_select" name = "<?php echo $data_unacc_topup['topup_id'] ?>" id="<?php echo $data_unacc_topup['topup_id'] ?>">
										      <option value='1'>Top-Up for 1week</option>
										      <option value='2'>Top-Up for 2week</option>
										      <option value='3'>Top-Up for 3week</option>
										      <option value='4'>Top-Up for 4week</option>
										      <option value='5'>Top-Up for 5week</option>
										      <option value='6'>Top-Up for 6week</option>
										   </select>
										</div>
									   </div>
								   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								      <div class="row">
									 <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5 nopadding-left">
									    <a href="#" class="btn btn-raised btn-xs thumbnail_btn_left" onclick="active_data_unacc_toupup('<?php echo $data_unacc_topup['topup_id'] ?>', '<?php echo $data_unacc_topup['topup_percent']?>')">ACTIVATE</a>
									 </div>
									 
								      </div>
								   </div>
								</div>
							    </div>
							<?php
								 }
							?>
						       </div>
						  </div>
					     </div>
						  <?php
					     }
					     ?>
					     <!-- free data topup End -->  
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
    
    
    
     <!-- Data topup  Modal -->
       <div class="modal fade" id="data_topup_modal" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title" style="font-weight:400">Apply Data TopUp</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>topup/check_payment_gateway" id='update_mobile'>
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <input type="hidden" name = "data_topup_id_hidden" id="data_topup_id_hidden">
		    <h4 style="display: inline-block; font-weight:400">Data: </h4>
		    <h5 style="display: inline-block;margin-left:20px" id="data_topup_data"></h5>
                  </div>
               </div>
	       <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <h4 style="display: inline-block; font-weight:400;">Price: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="data_topup_price"></h5>
                  </div>
               </div>
               
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="submit" name="pay_money" value="NEXT" class="btn btn-raised btn-danger btn-lg btn-block">
                  </div>
               </div>
            </form>
         </div>
               
              
            </div>
         </div>
       </div>
       
       
       
       <!-- Speed topup  Modal -->
       <div class="modal fade" id="speed_topup_modal" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title" style="font-weight:400">Apply Speed TopUp</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>topup/check_payment_gateway" >
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <input type="hidden" name = "speed_topup_id_hidden" id="speed_topup_id_hidden">
			 <input type="hidden" name = "speed_topup_week_hidden" id="speed_topup_week_hidden">
		    <h4 style="display: inline-block;font-weight:400">Speed: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="speed_topup_speed"></h5>
                  </div>
               </div>
	        <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <h4 style="display: inline-block;font-weight:400">Duration: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="speed_topup_duration"></h5>&nbsp; Week
                  </div>
               </div>
	       <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <h4 style="display: inline-block;font-weight:400">Price: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="speed_topup_price"></h5>
                  </div>
               </div>
               
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="submit" name="pay_money" value="NEXT" class="btn btn-raised btn-danger btn-lg btn-block">
                  </div>
               </div>
            </form>
         </div>
               
              
            </div>
         </div>
       </div> 
       
       
       
       
       <!-- Data unaccoundancy topup  Modal -->
       <div class="modal fade" id="data_unacc_topup_modal" role="dialog" data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center>
                     <h4 class="modal-title" style="font-weight:400">Apply Free Data TopUp</h4>
                  </center>
                  
               </div>
               <div class="modal-body" style="margin-top:5px">
               <form class="form-horizontal" autocomplete="off" method="post" action="<?php echo base_url();
            ?>topup/check_payment_gateway" >
               <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <input type="hidden" name = "data_unacc_topup_id_hidden" id="data_unacc_topup_id_hidden">
		    <input type="hidden" name = "data_unacc_topup_week_hidden" id="data_unacc_topup_week_hidden">	 
		    <h4 style="display: inline-block;font-weight:400">Data: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="data_unacc_topup_data"></h5>
                  </div>
               </div>
	       <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <h4 style="display: inline-block;font-weight:400">Duration: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="data_unacc_topup_duration"></h5>&nbsp; Week
                  </div>
               </div>
	       <div class="form-group">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-centered">
		    <h4 style="display: inline-block;font-weight:400">Price: </h4>
		    <h5 style="display: inline-block; margin-left:20px" id="data_unacc_topup_price"></h5>
                  </div>
               </div>
               
            
               <div class="form-group">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-centered">
                     <input type="submit" name="pay_money" value="NEXT" class="btn btn-raised btn-danger btn-lg btn-block">
                  </div>
               </div>
            </form>
         </div>
               
              
            </div>
         </div>
       </div>
       
       
      <script type="text/javascript" language="javascript">
         $(function(){
         $('#wrapper').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper').css({'height':($(document).height())+'px'});
         });
          $('#wrapper_right').css({'height':($(document).height())+'px'});
         $(window).resize(function(){
         	$('#wrapper_right').css({'height':($(document).height())+'px'});
         });
         });
	 
	 function active_data_toupup(topupid, topupdata, topupprice){
	       $("#data_topup_data").html(topupdata);
	       $("#data_topup_price").html("&#8377; "+topupprice);
	       $("#data_topup_id_hidden").val(topupid);
	       $("#data_topup_modal").modal("show");
	 }
	 
	  function active_speed_toupup(topupid,speed) {
	       var week_selected = $("#"+topupid).val();
	       $.ajax({
		    type: "POST",
		    url: "<?php echo base_url()?>topup/check_already_speed_topup_apply",
		    async: false,
		    cache: false,
		    dataType: 'json',
		    data: "topupid="+topupid+"&week="+week_selected,
		    success: function(data){
			 if(data.resultCode == '0'){
			      alert("Speed Topup already applied");
			 }else{
			      $("#speed_topup_speed").html(speed);
			      $("#speed_topup_duration").html(week_selected);
			      $("#speed_topup_price").html("&#8377; "+data.price);
			      $("#speed_topup_id_hidden").val(topupid);
			      $("#speed_topup_week_hidden").val(week_selected);
			      $("#speed_topup_modal").modal("show");
			 }
		    }
	       });
	 }
	 
	 function active_data_unacc_toupup(topupid, freedata) {
	       var week_selected = $("#"+topupid).val();
	       $.ajax({
		    type: "POST",
		    url: "<?php echo base_url()?>topup/check_already_data_unacc_topup_apply",
		    async: false,
		    cache: false,
		    dataType: 'json',
		    data: "topupid="+topupid+"&week="+week_selected,
		    success: function(data){
			 if(data.resultCode == '0'){
			      alert("Free Data Topup already applied");
			 }else{
			      $("#data_unacc_topup_id_hidden").val(topupid);
			      $("#data_unacc_topup_week_hidden").val(week_selected);
			      $("#data_unacc_topup_data").html(freedata+"% free data");
			      $("#data_unacc_topup_modal").modal("show");  
			      $("#data_unacc_topup_price").html("&#8377; "+data.price);
			      $("#data_unacc_topup_duration").html(week_selected);
			 }
		    }
	       });
	       
	 }
	 
	
      </script>
   </body>
</html>